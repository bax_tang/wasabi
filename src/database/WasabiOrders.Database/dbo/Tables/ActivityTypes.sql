﻿
CREATE TABLE [dbo].[ActivityTypes]
(
	[TypeID] BIGINT NOT NULL IDENTITY(1, 1),
	[Name] VARCHAR(64) NOT NULL,
	[Text] VARCHAR(1024) NULL,

	CONSTRAINT [activitytypes_pk_typeid] PRIMARY KEY NONCLUSTERED ([TypeID] ASC)
)
ON [PRIMARY];
GO

CREATE UNIQUE NONCLUSTERED INDEX [activitytypes_idx_name] ON [dbo].[ActivityTypes] ([Name] ASC);
GO