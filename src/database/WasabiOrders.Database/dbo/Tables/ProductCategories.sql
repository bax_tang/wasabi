﻿CREATE TABLE [dbo].[ProductCategories]
(
	[CategoryID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [productcategories_def_categoryid] DEFAULT (NEWSEQUENTIALID()),
	[Name] NVARCHAR(64) NOT NULL,
	[Description] NVARCHAR(MAX) NULL,
	[ExternalFolderID] VARCHAR(16) NULL,
	[CreatedAt] DATETIME NULL CONSTRAINT [productcategories_def_createdat] DEFAULT (GETUTCDATE()),
	[UpdatedAt] DATETIME NULL,

	CONSTRAINT [productcategories_pk_categoryid] PRIMARY KEY NONCLUSTERED ([CategoryID] ASC)
)
ON [PRIMARY];
GO

CREATE UNIQUE NONCLUSTERED INDEX [productcategories_idx_name] ON [dbo].[ProductCategories] ([Name] ASC);
GO