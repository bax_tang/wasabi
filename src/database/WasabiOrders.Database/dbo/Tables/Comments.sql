﻿
CREATE TABLE [dbo].[Comments]
(
	[CommentID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [comments_def_commentid] DEFAULT (NEWSEQUENTIALID()),
	[RepliedTo] UNIQUEIDENTIFIER NULL,
	[ObjectID] UNIQUEIDENTIFIER NOT NULL,
	[AuthorID] UNIQUEIDENTIFIER NOT NULL,
	[Title] NVARCHAR(256) NULL,
	[Text] NVARCHAR(2048) NULL,
	[CreatedAt] DATETIME NULL CONSTRAINT [comments_def_creationdatetime] DEFAULT (GETUTCDATE()),
	[UpdatedAt] DATETIME NULL,

	CONSTRAINT [comments_pk_commentid] PRIMARY KEY NONCLUSTERED ([CommentID] ASC),

	CONSTRAINT [comments_fk_repliedto] FOREIGN KEY ([RepliedTo])
	REFERENCES [dbo].[Comments] ([CommentID]) ON DELETE NO ACTION ON UPDATE NO ACTION,

	CONSTRAINT [comments_fk_authorid] FOREIGN KEY ([AuthorID])
	REFERENCES [dbo].[Users] ([UserID]) ON DELETE CASCADE ON UPDATE NO ACTION
)
ON [PRIMARY];
GO

CREATE NONCLUSTERED INDEX [comments_idx_authorid] ON [dbo].[Comments] ([AuthorID] ASC);
GO

CREATE NONCLUSTERED INDEX [comments_idx_ownerid] ON [dbo].[Comments] ([ObjectID] ASC);
GO