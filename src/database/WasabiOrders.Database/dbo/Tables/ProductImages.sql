﻿
CREATE TABLE [dbo].[ProductImages]
(
	[ImageID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [productimages_def_imageid] DEFAULT (NEWSEQUENTIALID()),
	[ProductID] UNIQUEIDENTIFIER NOT NULL,
	[FileID] UNIQUEIDENTIFIER NOT NULL,
	[Type] INT NOT NULL CONSTRAINT [productimages_def_imagetype] DEFAULT (2),
	[CreatedAt] DATETIME NULL CONSTRAINT [productimages_def_createdat] DEFAULT (GETUTCDATE()),

	CONSTRAINT [productimages_pk_imageid] PRIMARY KEY NONCLUSTERED ([ImageID] ASC),

	CONSTRAINT [productimages_chk_imagetype] CHECK (([Type] = 0) OR ([Type] = 1) OR ([Type] = 2)), -- 0 - thumbnail, 1 - poster, 2 - other

	CONSTRAINT [productimages_fk_productid] FOREIGN KEY ([ProductID])
	REFERENCES [dbo].[Products] ([ProductID]) ON DELETE CASCADE ON UPDATE NO ACTION,

	CONSTRAINT [productimages_fk_binaryid] FOREIGN KEY ([FileID])
	REFERENCES [dbo].[Files] ([FileID]) ON DELETE CASCADE ON UPDATE NO ACTION
)
ON [PRIMARY];
GO
