﻿
CREATE TABLE [dbo].[Orders]
(
	[OrderID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [orders_def_orderid] DEFAULT (NEWSEQUENTIALID()),
	[AuthorID] UNIQUEIDENTIFIER NOT NULL,
	[Name] NVARCHAR(64) NULL,
	[Status] INT NOT NULL CONSTRAINT [orders_def_status] DEFAULT (0), -- 0 - Created; 1 - Registered; 2 - Delivered
	[OrderDateTime] DATETIME NOT NULL,
	[ExpectedDeliveryDateTime] DATETIME NULL,
	[ActualDeliveryDateTime] DATETIME NULL,
	[TotalPrice] MONEY NULL,
	[DiscountedPrice] MONEY NULL,
	[Rating] INT NULL,
	[ExternalFolderID] VARCHAR(16) NULL,
	[CreatedAt] DATETIME NULL CONSTRAINT [orders_def_createdat] DEFAULT (GETUTCDATE()),
	[UpdatedAt] DATETIME NULL,

	CONSTRAINT [orders_pk_orderid] PRIMARY KEY NONCLUSTERED ([OrderID] ASC),

	CONSTRAINT [orders_chk_status] CHECK (([Status] = 0) OR ([Status] = 1) OR ([Status] = 2)),
	CONSTRAINT [orders_chk_rating] CHECK (([Rating] IS NULL) OR (([Rating] >= 1) AND ([Rating] <= 5))),

	CONSTRAINT [orders_fk_authorid] FOREIGN KEY ([AuthorID])
	REFERENCES [dbo].[Users] ([UserID]) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ON [PRIMARY];
GO
