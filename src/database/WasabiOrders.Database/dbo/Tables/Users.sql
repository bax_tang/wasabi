﻿
CREATE TABLE [dbo].[Users]
(
	[UserID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [applicationusers_def_userid] DEFAULT (NEWSEQUENTIALID()),
	[AccountName] NVARCHAR(64) NOT NULL,
	[FirstName] NVARCHAR(64) NULL,
	[LastName] NVARCHAR(64) NULL,
	[Email] NVARCHAR(128) NULL,
	[PasswordHash] NVARCHAR(MAX) NOT NULL,
	[Discount] FLOAT NOT NULL CONSTRAINT [applicationusers_def_discount] DEFAULT (0.0),
	[CreatedAt] DATETIME NULL CONSTRAINT [applicationusers_def_createdat] DEFAULT (GETUTCDATE()),
	[UpdatedAt] DATETIME NULL,

	CONSTRAINT [users_pk_userid] PRIMARY KEY NONCLUSTERED ([UserID] ASC),
	CONSTRAINT [users_uni_accountname] UNIQUE NONCLUSTERED ([AccountName] ASC)
)
ON [PRIMARY];
GO

CREATE UNIQUE NONCLUSTERED INDEX [users_idx_accountname] ON [dbo].[Users] ([AccountName] ASC);
GO