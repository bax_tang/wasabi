﻿
CREATE TABLE [dbo].[GlobalSettings]
(
	[SettingID] BIGINT NOT NULL IDENTITY(1, 1),
	[Name] VARCHAR(64) NOT NULL,
	[Value] VARCHAR(2048) NOT NULL,
	[CreatedAt] DATETIME NULL CONSTRAINT [globalsettings_def_createdat] DEFAULT (GETUTCDATE()),
	[UpdatedAt] DATETIME NULL,

	CONSTRAINT [globalsettings_pk_settingid] PRIMARY KEY NONCLUSTERED ([SettingID] ASC)
)
ON [PRIMARY];
GO

CREATE UNIQUE NONCLUSTERED INDEX [globalsettings_idx_name] ON [dbo].[GlobalSettings] ([Name] ASC);
GO