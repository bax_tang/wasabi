﻿	
CREATE TABLE [dbo].[Products]
(
	[ProductID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [products_def_productid] DEFAULT (NEWSEQUENTIALID()),
	[CategoryID] UNIQUEIDENTIFIER NOT NULL,
	[Name] NVARCHAR(64) NOT NULL,
	[Description] NVARCHAR(1024) NULL,
	[Price] MONEY NOT NULL,
	[Weight] FLOAT NOT NULL,
	[Rating] INT NULL,
	[ExternalFolderID] VARCHAR(16) NULL,
	[CreatedAt] DATETIME NULL CONSTRAINT [products_def_createdat] DEFAULT (GETUTCDATE()),
	[UpdatedAt] DATETIME NULL,

	CONSTRAINT [products_pk_productid] PRIMARY KEY NONCLUSTERED ([ProductID] ASC),

	CONSTRAINT [products_fk_categoryid] FOREIGN KEY ([CategoryID])
	REFERENCES [dbo].[ProductCategories] ([CategoryID]) ON DELETE NO ACTION ON UPDATE NO ACTION,

	CONSTRAINT [products_chk_rating] CHECK (([Rating] IS NULL) OR (([Rating] >= 1) AND ([Rating] <= 5)))
)
ON [PRIMARY];
GO

CREATE NONCLUSTERED INDEX [products_idx_categoryid] ON [dbo].[Products] ([CategoryID] ASC);
GO
