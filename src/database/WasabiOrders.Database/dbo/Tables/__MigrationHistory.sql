﻿
CREATE TABLE [dbo].[__MigrationHistory]
(
    [MigrationId]    NVARCHAR (150)  NOT NULL,
    [ContextKey]     NVARCHAR (300)  NOT NULL,
    [Model]          VARBINARY (MAX) NOT NULL,
    [ProductVersion] NVARCHAR (32)   NOT NULL,

    CONSTRAINT [__MigrationHistory_pk_migrationid_contextkey] PRIMARY KEY CLUSTERED ([MigrationId] ASC, [ContextKey] ASC)
)
ON [PRIMARY];
GO