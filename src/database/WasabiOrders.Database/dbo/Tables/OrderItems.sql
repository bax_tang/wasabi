﻿
CREATE TABLE [dbo].[OrderItems]
(
	[ItemID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [orderitems_def_itemid] DEFAULT (NEWSEQUENTIALID()),
	[OrderID] UNIQUEIDENTIFIER NOT NULL,
	[ProductID] UNIQUEIDENTIFIER NOT NULL,
	[Amount] INT NOT NULL CONSTRAINT [orderitems_def_amount] DEFAULT (1),
	[CreatedAt] DATETIME NULL CONSTRAINT [orderitems_def_createdat] DEFAULT (GETUTCDATE()),

	CONSTRAINT [orderitems_pk_itemid] PRIMARY KEY NONCLUSTERED ([ItemID] ASC),

	CONSTRAINT [orderitems_chk_amount] CHECK ([Amount] >= 1),

	CONSTRAINT [orderitems_fk_orderid] FOREIGN KEY ([OrderID])
	REFERENCES [dbo].[Orders] ([OrderID]) ON DELETE CASCADE ON UPDATE NO ACTION,

	CONSTRAINT [orderitems_fk_productid] FOREIGN KEY ([ProductID])
	REFERENCES [dbo].[Products] ([ProductID]) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ON [PRIMARY];
GO
