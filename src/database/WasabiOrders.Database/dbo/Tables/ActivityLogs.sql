﻿
CREATE TABLE [dbo].[ActivityLogs]
(
	[RecordID] BIGINT NOT NULL IDENTITY(1, 1),
	[TypeID] BIGINT NOT NULL,
	[UserID] UNIQUEIDENTIFIER NOT NULL,
	[ObjectID] UNIQUEIDENTIFIER NULL,
	[ObjectName] NVARCHAR(2048) NULL,
	[ActivityDateTime] DATETIME NOT NULL CONSTRAINT [activitylogs_def_activitydatetime] DEFAULT (GETUTCDATE()),
	
	CONSTRAINT [activitylogs_pk_recordid] PRIMARY KEY NONCLUSTERED ([RecordID] ASC),

	CONSTRAINT [activitylogs_fk_typeid] FOREIGN KEY ([TypeID])
	REFERENCES [dbo].[ActivityTypes] ([TypeID]) ON DELETE NO ACTION ON UPDATE NO ACTION,

	CONSTRAINT [activitylogs_fk_userid] FOREIGN KEY ([UserID])
	REFERENCES [dbo].[Users] ([UserID]) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ON [PRIMARY];
GO
