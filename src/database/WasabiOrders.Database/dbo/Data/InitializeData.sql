﻿
IF OBJECT_ID('[dbo].[ActivityTypes]', 'U') IS NOT NULL
BEGIN
	INSERT INTO [dbo].[ActivityTypes] WITH(ROWLOCK) ([Name], [Text])
	VALUES
		(N'UserLogin', N'logged in'),
		(N'UserLogout', N'logged out'),
		(N'ProductCreated', N'added new product'),
		(N'ProductUpdated', N'updated an existing product'),
		(N'ProductDeleted', N'deleted an existing product'),
		(N'OrderCreated', N'added new order'),
		(N'OrderUpdated', N'updated an existing order'),
		(N'ImageUploaded', N'uploaded new image'),
		(N'CommentAdded', N'added new comment'),
		(N'CommentUpdated', N'updated an existing comment'),
		(N'CommentDeleted', N'deleted an existing comment');
END;
GO

PRINT N'Activity types created successfully.';
GO

IF OBJECT_ID('[dbo].[ProductCategories]', 'U') IS NOT NULL
BEGIN
	INSERT INTO [dbo].[ProductCategories] WITH(ROWLOCK) ([Name], [ExternalFolderID])
	VALUES
		(N'Суши', '37893459765'),
		(N'Роллы', '37893514424'),
		(N'Сеты', '37893532890'),
		(N'Хосомаки', '37893616007'),
		(N'Горячие роллы', '37893627206'),
		(N'Теплые и запеченные роллы', '37895046440'),
		(N'Сашими', '37895063943'),
		(N'Гунканы', '37895079212'),
		(N'Супы', '37895085156'),
		(N'Горячее', '37895103758'),
		(N'Салаты', '37895126883'),
		(N'Кокосовая вода', '37895128501'),
		(N'Роллы 1/2', '37895200027'),
		(N'Десерты', '37895202838'),
		(N'Новинки exotic', '37895216949'),
		(N'Добавка!', '37895251048'),
		(N'Пицца', '37895244980');
END;
GO

PRINT N'Product categories created successfully.';
GO

IF OBJECT_ID('[dbo].[GlobalSettings]', 'U') IS NOT NULL
BEGIN
	-- 1. Create Box Client ID
	INSERT INTO [dbo].[GlobalSettings] WITH(ROWLOCK) ([Name], [Value])
	SELECT
		'BoxClientID', '3mbi3klalacchxgxe0uzlbxo1r7d7imz';

	-- 2. Create Box Client Secret
	INSERT INTO [dbo].[GlobalSettings] WITH(ROWLOCK) ([Name], [Value])
	SELECT
		'BoxClientSecret', 'l9WgY9iuJtAQVPSk5xGozjXkJcisSZep';

	-- 3. Create Box Enterprize ID
	INSERT INTO [dbo].[GlobalSettings] WITH(ROWLOCK) ([Name], [Value])
	SELECT
		'BoxEnterpriseID', '26438637';

	-- 4. Create Box Private Key
	INSERT INTO [dbo].[GlobalSettings] WITH(ROWLOCK) ([Name], [Value])
	SELECT
		'BoxPrivateKey',
		'-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-256-CBC,768340ACD3D98354554C67778978E3C2

UpoaNTuBFxEqp/hnIrQ7n/fJ02g/OyEMz+MtZXVKJFACKgeOlvzg1gcI1hewrOej
hKvdgvvNRqyJoTkB38sktqLzKkPfAV99waqAiirMtx+aZwAycsdIi8M83ZOZ9pX0
1yp8YaEt7cW44IgD/Zp4d0+PL6YASkaGkPhS90SxYwHYt8mmXW0db0iUc0mgxth4
6Zp2MESSipvd72L1S0+KBPQOCZPg6HZuWXHyo0gh9Ze+FfVUdneSEjxfxbDxdIBx
LBMsYNSinbGFfwraqd6RRam9Bs+blzM8Cn85cb0yV+TGewriR+Nx3EnV0lovx8h6
Epb5NJcWyCxCVIJwOj9NSzbbTAzdj4rdzJ0YQWMDKml+FZhZjL0tyZAJDLErFMdP
iTI/bM5mnylKjXvK+Af/p4nUDEuZfN/bHysQ2GoJoA4tJOOeMKUW4EGjEYqMr+MN
KJMq+hlzAFOPintGAlBdXmGtH1q5PDlEURjYXr2ATiSTZEOINmx5XAlmmzvIhiG7
wgSdlkYDALXKGJ2s04tONObyj4siUAante8F7+8532/bZuuZDsKJYHEyAxKjg2Qh
G7R4PeJIxfxYH+U64A6rEQpJpU0THqePmevPCQgZNrkawUBBSAxKeJ4Brwg9VedH
1/WJc79zJYUL8MZzyJ9o7oYzmavLrOlYOE/AIbSOiFT0W1pbl42YLWnbLvhebm2r
bD2Hgm4Emyd2PKuhSE5EbtxMtMNZGwV9efAO1Dpl+y+nzTak8EUaG39iWdOjGKjp
sBcnNJnCS3M14Nwi5iaov26yd8SaOm2TnRIznLqVLvFIlMHLev53bf0TNqzC62Hd
oxUd03gXW6cHYRwp4oPnMUdKi5N4qk6YeMDaNxwWyS8NP31Tmv4N8JRtrU0r4wvP
CY3Z94RtVG0SyHOb/0+OyrH3P1Rz3t/frT8BjJeRQdw6XRDWtb3IluFXBUQqKh4C
WoJwdvsHukt6jN3vPnD8Qsk3409UVL85ZR88U1E3uya+khCgEyePAvoNN0UXwjqy
xJiRIEEf0zjbUe6sWTO6G67zdf59YZnLEJKz/4LHHVjvoKz8XZj4Hd+gJ5s3Jivk
1SBjLmqEiMWcnVJF3UMGwOJLTCnALdFQhqmjfcVoa9hP9Ik6PLT8OZGGi2gMkOCU
V6SecWQJtIvxblABQH7oL1TBzmjGl24h60W4ZWrTYyDfDnx3Yw7UlQLBJ7v04Fu5
Nff7Uczax8iEi1F4kRynekDhrcG+79IO8MXSIEfk8V+DR9n0UH3YVMXaDNoiTG7/
Y4PgVhkjx7sP+qHZGHHQu3ILATZ+P957/mz7P/rETq1KqIs6H6WSbv0K8NiiTj+S
zVGvrsOaANnTyZXZOo5T3nJlm+5COcEng5BrLmErgevE3YXSsN162wCW5spLpjDT
TrckJX0Udwpz7M/AQjqBByhxh0jfnuSGof/FMZYHaBzlQWXw8lZjGZv70concXic
6fFpv9+3keHnnR2C9g8dkNrlJqlddIQpuckxmIpTgXJvgIQ7y8HfScf4nMqEtGBH
9jqTuP4vEYxvcVlkkcvf3cyYjcK6TWluAgy9W6SV5hkWWb/xrlbNmF4JmbC4jToA
-----END RSA PRIVATE KEY-----';

	-- 5. Create Box Private Key Passphrase
	INSERT INTO [dbo].[GlobalSettings] WITH(ROWLOCK) ([Name], [Value])
	SELECT
		'BoxPrivateKeyPassphrase', '*8LnQM.6';

	-- 6. Create Box Public Key ID
	INSERT INTO [dbo].[GlobalSettings] WITH(ROWLOCK) ([Name], [Value])
	SELECT
		'BoxPublicKeyID', 'xm2xdnxm';

	-- 7. Create Box User ID
	INSERT INTO [dbo].[GlobalSettings] WITH(ROWLOCK) ([Name], [Value])
	SELECT
		'BoxUserID', '2355238573';
END;
GO

PRINT N'Global settings created successfully.';
GO

IF OBJECT_ID('[dbo].[Users]', 'U') IS NOT NULL
BEGIN
	INSERT INTO [dbo].[Users] WITH(ROWLOCK) ([UserID], [AccountName], [FirstName], [LastName], [Email], [Discount], [PasswordHash])
	VALUES
	(
		'0FFDE532-AF7D-4E80-8364-8C21C47B8C39',
		N'kalinov_dmitry',
		N'Дмитрий',
		N'Калинов',
		N'kadmvl@yandex.ru',
		0.2,
		N'$2a$12$wfOb0ZOhstZ.z1lIQhxpC./LY/eBM0P29CHdvi1qBY.ifRJPkp9cC' -- yakrevedko11
	),
	(
		'DC8DE4C5-F4EA-4951-8DB5-C1DD5DBA7840',
		N'margo7083',
		N'Маргарита',
		N'Жиляева',
		N'mgvik@yandex.ru',
		0.2,
		N'$2a$12$wfOb0ZOhstZ.z1lIQhxpC./LY/eBM0P29CHdvi1qBY.ifRJPkp9cC' -- yakrevedko11
	);
END;
GO

PRINT N'Users created successfully.';
GO