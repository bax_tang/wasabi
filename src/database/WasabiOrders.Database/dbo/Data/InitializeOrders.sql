﻿
SET NOCOUNT ON;

DELETE FROM [dbo].[Orders] WITH(ROWLOCK);
GO

PRINT N'Existing orders deleted successfully.';
GO

DECLARE @ProductsCount INT = 0;
SELECT @ProductsCount = COUNT([ProductID]) FROM [dbo].[Products];
IF (@ProductsCount = 0)
BEGIN
	RAISERROR (N'Can''t create orders because there is no products in the database.', 16, 1);
END;

DECLARE @AuthorID UNIQUEIDENTIFIER = '0FFDE532-AF7D-4E80-8364-8C21C47B8C39'; -- dmitry

EXEC [dbo].[sp_orders_create] @AuthorID, '2017-07-28 18:06:37.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Конго ролл" amount="1" />
	<product productname="Кунсей Бекон" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-07-20 18:24:13.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Конго ролл" amount="1" />
	<product productname="Кунсей Бекон" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-07-14 19:23:25.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Конго ролл" amount="1" />
	<product productname="Кунсей Бекон" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-07-05 18:30:38.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Конго ролл" amount="1" />
	<product productname="Кунсей Бекон" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-06-23 18:58:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Конго ролл" amount="1" />
	<product productname="Кунсей Бекон" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-05-21 16:05:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Окаяма" amount="1" />
	<product productname="Конго ролл" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-05-12 18:05:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Цунами маки" amount="1" />
	<product productname="Конго ролл" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-04-30 12:17:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Цунами маки" amount="1" />
	<product productname="Конго ролл" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-04-21 17:46:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Ямагата" amount="1" />
	<product productname="Конго ролл" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-04-16 17:46:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Баварский" amount="1" />
	<product productname="Конго ролл" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-03-28 19:45:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Кунсей Бекон" amount="1" />
	<product productname="Тотиги унаги" amount="1" />
	<product productname="Конго ролл" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-03-16 19:02:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Кунсей Бекон" amount="1" />
	<product productname="Тотиги унаги" amount="1" />
	<product productname="Конго ролл" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-03-05 13:58:28.400', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Конго ролл" amount="1" />
	<product productname="Кунсей бекон" amount="1" />
	<product productname="Темура" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-02-25 18:15:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Конго" amount="1" />
	<product productname="Тори маки" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-02-08 19:44:30.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Конго ролл" amount="1" />
	<product productname="Филадельфия с огурцом" amount="1" />
	<product productname="Темура" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2017-01-31 18:30:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Тори маки" amount="1" />
	<product productname="Цунами маки" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2016-08-24 15:51:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Тори темпура" amount="1" />
	<product productname="Кунсей бекон" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2016-04-20 18:11:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Якумо" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Цунами маки" amount="1" />
	<product productname="Конго ролл" amount="1" />
</products>';
EXEC [dbo].[sp_orders_create] @AuthorID, '2016-03-05 15:42:00.000', 2, N'
<products>
	<product productname="Кунсей харумаки" amount="1" />
	<product productname="Сяке татаки" amount="1" />
	<product productname="Темура" amount="1" />
	<product productname="Наоми" amount="1" />
	<product productname="Цунами маки" amount="1" />
</products>';
GO

PRINT N'Already delivered orders created successfully.';
GO