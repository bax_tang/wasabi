﻿CREATE TRIGGER [OrderImagesAfterUpdate]
ON [dbo].[SomeTableOrView]
FOR DELETE, INSERT, UPDATE
AS EXTERNAL NAME SomeAssembly.SomeType.SomeMethod
