﻿
CREATE TRIGGER [trg_comments_after_update] ON [dbo].[Comments]
WITH EXECUTE AS N'dbo'
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[Comments] WITH(ROWLOCK)
	SET [UpdatedAt] = GETUTCDATE()
	FROM Inserted AS UpdatedComments
	INNER JOIN [dbo].[Comments] AS Comments ON ([Comments].[CommentID] = [UpdatedComments].[CommentID]);
END;
GO