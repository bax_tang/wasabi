﻿
CREATE PROCEDURE [dbo].[sp_orders_create]
(
	@AuthorID UNIQUEIDENTIFIER,
	@OrderDate DATETIME,
	@ExpectedDeliveryDate DATETIME = NULL,
	@ActualDeliveryDate DATETIME = NULL,
	@OrderStatus INT = 2,
	@OrderProducts NVARCHAR(MAX) = N'<products></products>'
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OrderName NVARCHAR(64) = N'Заказ от ' + CONVERT(NVARCHAR(24), @OrderDate, 104);
	DECLARE @NewOrders TABLE ([OrderID] UNIQUEIDENTIFIER NOT NULL);
	DECLARE @NewOrderID UNIQUEIDENTIFIER = NULL;

	IF (@OrderStatus = 2)
	BEGIN
		IF (@ExpectedDeliveryDate IS NULL)
		BEGIN
			SET @ExpectedDeliveryDate = DATEADD(MINUTE, 120.0, @OrderDate);
		END;

		IF (@ActualDeliveryDate IS NULL)
		BEGIN
			DECLARE @random FLOAT = RAND();
			SET @ActualDeliveryDate =
				CASE
					WHEN (@random > 0.5) THEN DATEADD(MINUTE, 90.0 + 60.0 * (1 - @random), @OrderDate)
					WHEN (@random < 0.5) THEN DATEADD(MINUTE, 90.0 * (1 + @random), @OrderDate)
					ELSE @ExpectedDeliveryDate
				END;
		END;
	END;
	
	INSERT INTO [dbo].[Orders] WITH(ROWLOCK) ([AuthorID], [Name], [OrderDateTime], [ExpectedDeliveryDateTime], [ActualDeliveryDateTime], [Status])
	OUTPUT [Inserted].[OrderID] INTO @NewOrders ([OrderID])
	VALUES
	(
		@AuthorID,
		@OrderName,
		@OrderDate,
		@ExpectedDeliveryDate,
		@ActualDeliveryDate,
		@OrderStatus
	);
	
	SELECT TOP 1 @NewOrderID = [OrderID] FROM @NewOrders;

	DECLARE @OrderProductsXml XML = CAST(@OrderProducts AS XML);
	DECLARE @OrderProductsTable TABLE ([ProductName] NVARCHAR(64) NOT NULL, [Amount] INT NOT NULL);

	INSERT INTO @OrderProductsTable ([ProductName], [Amount])
	SELECT
		T.c.value(N'./@productname', N'NVARCHAR(64)'),
		T.c.value(N'./@amount', N'INT')
	FROM @OrderProductsXml.nodes('/products/product') T(c);
	
	WITH MatchedProducts AS
	(
		SELECT
			[Products].[ProductID],
			[Products].[Name],
			[OPT].[Amount],
			RANK() OVER (ORDER BY [OPT].[ProductName]) AS 'RowRank',
			ROW_NUMBER() OVER (ORDER BY [OPT].[ProductName]) AS 'RowNumber'
		FROM @OrderProductsTable AS OPT
		INNER JOIN [dbo].[Products] AS Products ON (([Products].[Name] = [OPT].[ProductName]) OR ([Products].[Name] LIKE (N'%' + [OPT].[ProductName] + N'%')))
	)
	INSERT INTO [dbo].[OrderItems] WITH(ROWLOCK) ([OrderID], [ProductID], [Amount])
	SELECT @NewOrderID, [ProductID], [Amount]
	FROM MatchedProducts
	WHERE [RowRank] = [RowNumber];
	
	DECLARE @Discount FLOAT = NULL;
	DECLARE @OrderTotalPrice MONEY = NULL;

	SELECT @Discount = [Discount]
	FROM [dbo].[ApplicationUsers]
	WHERE [UserID] = @AuthorID;

	SELECT @OrderTotalPrice = SUM([Products].[Price] * [Items].[Amount])
	FROM [dbo].[OrderItems] AS Items
	INNER JOIN [dbo].[Products] AS Products ON ([Products].[ProductID] = [Items].[ProductID])
	WHERE [Items].[OrderID] = @NewOrderID;

	UPDATE [dbo].[Orders] WITH(ROWLOCK)
	SET
		[TotalPrice] = @OrderTotalPrice,
		[DiscountedPrice] = (1.0 - @Discount) * @OrderTotalPrice
	WHERE [OrderID] = @NewOrderID;
END;
GO