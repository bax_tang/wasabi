﻿
CREATE PROCEDURE [dbo].[sp_comments_get_with_hierarchy]
(
	@ObjectID UNIQUEIDENTIFIER = NULL,
	@RootCommentID UNIQUEIDENTIFIER = NULL,
	@MaxLevel INT = 0
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF (@ObjectID IS NULL)
	BEGIN
		RAISERROR ('Object identifier can''t be null.', 16, 1);
	END;

	WITH CommentsHierarchy AS
	(
		SELECT
			[Comments].[CommentID],
			[Comments].[AuthorID],
			[Comments].[Title],
			[Comments].[Text],
			[Comments].[CreatedAt],
			[Comments].[UpdatedAt],
			[Comments].[RepliedTo],
			0 AS 'Level'
		FROM [dbo].[Comments] AS Comments
		WHERE
			([Comments].[ObjectID] = @ObjectID) AND
			(
				((@RootCommentID IS NULL) AND ([Comments].[RepliedTo] IS NULL))
				OR
				([Comments].[RepliedTo] = @RootCommentID)
			)

		UNION ALL

		SELECT
			[Comments].[CommentID],
			[Comments].[AuthorID],
			[Comments].[Title],
			[Comments].[Text],
			[Comments].[CreatedAt],
			[Comments].[UpdatedAt],
			[Comments].[RepliedTo],
			1 + [Level] AS 'Level'
		FROM [dbo].[Comments] AS Comments
		INNER JOIN [CommentsHierarchy] AS CH ON ([CH].[CommentID] = [Comments].[RepliedTo])
	)
	SELECT
		[CH].[CommentID] AS 'Id',
		[CH].[AuthorID] AS 'AuthorId',
		[Users].[AccountName] AS 'AuthorName',
		[CH].[RepliedTo] AS 'RepliedTo',
		[CH].[Title] AS 'Title',
		[CH].[Text] AS 'Text',
		[CH].[CreatedAt] AS 'CreatedAt',
		[CH].[UpdatedAt] AS 'UpdatedAt',
		[CH].[Level] AS 'Level'
	FROM [CommentsHierarchy] AS CH
	INNER JOIN [dbo].[ApplicationUsers] AS Users ON ([Users].[UserID] = [CH].[AuthorID])
	WHERE [CH].[Level] <= @MaxLevel
	ORDER BY [CH].[Level];
END;
GO