﻿
CREATE PROCEDURE [dbo].[sp_products_get_all]
(
	@CategoryID UNIQUEIDENTIFIER = NULL,
	@PageNumber INT = 1,
	@PageSize INT = 50
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @MinRowNumber INT = @PageSize * (@PageNumber - 1);

	WITH ProductsWithCategories AS
	(
		SELECT
			[Products].[ProductID] AS 'Id',
			[Products].[Name] AS 'Name',
			[Products].[Description] AS 'Description',
			[Products].[Price] AS 'Price',
			[Products].[Rating] AS 'Rating',
			[Products].[Weight] AS 'Weight',
			[Products].[CategoryID] AS 'CategoryID',
			ROW_NUMBER() OVER (ORDER BY [Products].[Price]) AS 'RowNumber'
		FROM [dbo].[Products] AS Products
		WHERE ((@CategoryID IS NULL) OR ([Products].[CategoryID] = @CategoryID))
	)
	SELECT *
	FROM [ProductsWithCategories] AS PWC
	INNER JOIN [dbo].[ProductCategories] AS Categories ON ([Categories].[CategoryID] = [PWC].[CategoryID])
	ORDER BY [PWC].[RowNumber]
END;
GO