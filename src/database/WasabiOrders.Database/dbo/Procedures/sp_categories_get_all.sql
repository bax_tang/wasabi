﻿
CREATE PROCEDURE [dbo].[sp_categories_get_all]
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	WITH CategoriesWithProductsCount AS
	(
		SELECT
			[Products].[CategoryID],
			COUNT([Products].[ProductID]) AS 'ProductsCount'
		FROM [dbo].[Products] AS Products
		GROUP BY [Products].[CategoryID]
	)
	SELECT
		[Categories].[CategoryID] AS 'Id',
		[Categories].[Name] AS 'Name',
		[Categories].[Description] AS 'Description',
		ISNULL([CategoriesWithPC].[ProductsCount], 0) AS 'ProductsCount'
	FROM [dbo].[ProductCategories] AS Categories
	LEFT JOIN [CategoriesWithProductsCount] AS CategoriesWithPC ON ([CategoriesWithPC].[CategoryID] = [Categories].[CategoryID]);
END;
GO