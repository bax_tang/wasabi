﻿
CREATE PROCEDURE [dbo].[sp_activitylogs_create]
(
	@ActivityType VARCHAR(64) = N'',
	@UserID UNIQUEIDENTIFIER = NULL,
	@ObjectID UNIQUEIDENTIFIER = NULL,
	@ObjectName NVARCHAR(2048) = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TypeID BIGINT = [dbo].[fn_activitytypes_get_by_name] (@ActivityType);

	IF ((@TypeID IS NOT NULL) AND (@UserID IS NOT NULL))
	BEGIN
		INSERT INTO [dbo].[ActivityLogs] WITH(ROWLOCK) ([TypeID], [UserID], [ObjectID], [ObjectName])
		VALUES
		(
			@TypeID,
			@UserID,
			@ObjectID,
			@ObjectName
		);
	END;
END;
GO