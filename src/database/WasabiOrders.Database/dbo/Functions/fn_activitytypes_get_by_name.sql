﻿
CREATE FUNCTION [dbo].[fn_activitytypes_get_by_name]
(
	@Name NVARCHAR(64) = N''
)
RETURNS BIGINT
WITH EXECUTE AS N'dbo'
AS
BEGIN
	DECLARE @TypeID BIGINT = NULL;

	SELECT @TypeID = [TypeID]
	FROM [dbo].[ActivityTypes] WITH(ROWLOCK)
	WHERE [Name] = @Name;

	RETURN @TypeID;
END;
GO