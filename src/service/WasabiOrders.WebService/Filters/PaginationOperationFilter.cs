﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

using Swashbuckle.Swagger;

namespace WasabiOrders.WebService.Filters
{
    public class PaginationOperationFilter : IOperationFilter
    {
        #region Constructors

        public PaginationOperationFilter() { }
        #endregion

        #region IOperationFilter implementation

        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            var attributes = apiDescription.ActionDescriptor.GetCustomAttributes<EnablePaginationAttribute>();
            if (attributes.Count > 0)
            {
                IList<Parameter> parameters = operation.parameters;
                if (parameters == null)
                {
                    operation.parameters = parameters = new List<Parameter>();
                }

                CreateParameters(attributes[0], parameters);
            }
        }
        #endregion

        #region Private class methods

        private static void CreateParameters(EnablePaginationAttribute paginationAttribute, IList<Parameter> parameters)
        {
            parameters.Add(CreateParameter("pageNumber", "query", "integer", true, 1));
            parameters.Add(CreateParameter("pageSize", "query", "integer", true, paginationAttribute.PageSize));
            parameters.Add(CreateParameter("orderBy", "query", "string", true, paginationAttribute.OrderBy));
        }

        private static Parameter CreateParameter(string name, string location, string type, bool required, object defaultValue)
        {
            return new Parameter
            {
                @default = defaultValue,
                @in = location,
                name = name,
                required = required,
                type = type
            };
        }
        #endregion
    }
}