﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Filters;

using Swashbuckle.Swagger;

namespace WasabiOrders.WebService
{
	public class ActionParameterOperationFilter : IOperationFilter
	{
		#region Constructors

		public ActionParameterOperationFilter() { }
		#endregion

		#region IOperationFilter implementation

		public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
		{
			var parameterAttributes = apiDescription.ActionDescriptor.GetCustomAttributes<ActionParameterAttribute>();
			if (parameterAttributes.Count > 0)
			{
				IList<Parameter> parameters = operation.parameters;
				if (parameters == null)
				{
					operation.parameters = parameters = new List<Parameter>();
				}

				foreach (ActionParameterAttribute parameterAttribute in parameterAttributes)
				{
					parameters.Add(new Parameter
					{
						@default = parameterAttribute.DefaultValue,
						@in = parameterAttribute.Location.ToString(),
						name = parameterAttribute.Name,
						description = parameterAttribute.Description,
						format = parameterAttribute.Format,
						required = parameterAttribute.IsRequired,
						type = parameterAttribute.Type.ToString()
					});
				}
			}
		}
		#endregion
	}
}