﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

using Autofac.Integration.WebApi;
using NLog;

namespace WasabiOrders.WebService
{
	using Exceptions;

	public sealed class WasabiOrdersExceptionFilter : ExceptionFilterAttribute, IAutofacExceptionFilter
	{
		#region Fields and properties

		private readonly ILogger _logger;
		#endregion

		#region Constructors

		public WasabiOrdersExceptionFilter(ILogger logger)
		{
			if (logger == null)
			{
				throw new ArgumentNullException(nameof(logger), "Service logger can't be null.");
			}

			_logger = logger;
		}
		#endregion

		#region ExceptionFilterAttribute methods overriding

		public override void OnException(HttpActionExecutedContext actionExecutedContext)
		{
			WriteExceptionToLog(actionExecutedContext.Exception);

			CreateErrorResponse(actionExecutedContext);
		}

		public override Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
		{
			WriteExceptionToLog(actionExecutedContext.Exception);

			CreateErrorResponse(actionExecutedContext);

			return Task.FromResult(0);
		}
		#endregion

		#region Private class methods

		private void CreateErrorResponse(HttpActionExecutedContext actionExecutedContext)
		{
			HttpResponseMessage response = null;
			
			HttpWebException serviceException = actionExecutedContext.Exception as HttpWebException;
			if (serviceException != null)
			{
				response = actionExecutedContext.Request.CreateErrorResponse(serviceException.StatusCode, serviceException);
			}
			else
			{
				response = actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, actionExecutedContext.Exception);
			}

			actionExecutedContext.Response = response;
		}

		private void WriteExceptionToLog(Exception occuredException)
		{
			_logger.Error(occuredException, occuredException.Message);
		}
		#endregion
	}
}