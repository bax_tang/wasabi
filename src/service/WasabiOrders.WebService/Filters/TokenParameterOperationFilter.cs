﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Filters;

using Swashbuckle.Swagger;

namespace WasabiOrders.WebService.Filters
{
    public class TokenParameterOperationFilter : IOperationFilter
    {
        #region Constructors

        public TokenParameterOperationFilter() { }
        #endregion

        #region IOperationFilter implementation

        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
		{
			if (apiDescription.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Count > 0)
			{
				return;
			}

			var filters = apiDescription.ActionDescriptor.GetFilterPipeline();
            if (filters.Count > 0)
            {
                if (operation.parameters == null)
                {
                    operation.parameters = new List<Parameter>();
                }

                Parameter authorizationParameter = GetAuthorizationParameterFromOperationFilters(filters);
                if (authorizationParameter != null)
                {
                    operation.parameters.Add(authorizationParameter);
                }
            }
        }
        #endregion

        #region Private class methods

        private Parameter GetAuthorizationParameterFromOperationFilters(ICollection<FilterInfo> operationFilters)
        {
            Parameter authorizationParameter = null;

            foreach (var currentFilter in operationFilters)
            {
                if (currentFilter.Instance is AuthorizeAttribute)
                {
                    authorizationParameter = new Parameter
                    {
                        name = "Authorization",
                        @in = "header",
                        @default = "bearer token_text",
                        description = "Access token",
                        required = true,
                        type = "string"
                    };
                    break;
                }
            }

            return authorizationParameter;
        }
        #endregion
    }
}