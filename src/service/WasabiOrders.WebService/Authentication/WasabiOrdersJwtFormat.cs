﻿using System;
using System.Collections.Generic;
using System.IdentityModel;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;

using ValidatedToken = Microsoft.IdentityModel.Tokens.SecurityToken;

namespace WasabiOrders.WebService
{
	internal class WasabiOrdersJwtFormat : ISecureDataFormat<AuthenticationTicket>
	{
		#region Fields and properties

		private readonly JwtSecurityTokenHandler _tokenHandler;
		private readonly TokenValidationParameters _validationParameters;

		public bool UseTokenLifetime { get; set; } = true;
		#endregion

		#region Constructors

		public WasabiOrdersJwtFormat(JwtSecurityTokenHandler tokenHandler, TokenValidationParameters validationParameters)
		{
			_tokenHandler = tokenHandler;
			_validationParameters = validationParameters;
		}
		#endregion

		#region ISecureDataFormat implementation

		public string Protect(AuthenticationTicket data)
		{
			throw new NotSupportedException("AuthenticationTicket protection is not supported.");
		}

		public AuthenticationTicket Unprotect(string data)
		{
			ValidatedToken validatedToken;
			ClaimsPrincipal principal = _tokenHandler.ValidateToken(data, _validationParameters, out validatedToken);

			ClaimsIdentity identity = principal.Identity as ClaimsIdentity;

			AuthenticationProperties authProperties = new AuthenticationProperties();
			if (UseTokenLifetime)
			{
				DateTime validFrom = validatedToken.ValidFrom;
				if (validFrom != DateTime.MinValue)
				{
					authProperties.IssuedUtc = new DateTimeOffset(validFrom.ToUniversalTime());
				}

				DateTime validTo = validatedToken.ValidTo;
				if (validTo != DateTime.MinValue)
				{
					authProperties.ExpiresUtc = new DateTimeOffset(validTo.ToUniversalTime());
				}

				authProperties.AllowRefresh = false;
			}

			return new AuthenticationTicket(identity, authProperties);
		}
		#endregion
	}
}