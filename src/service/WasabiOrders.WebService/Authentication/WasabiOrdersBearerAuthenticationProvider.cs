﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace WasabiOrders.WebService
{
	using DataModel.Entities;
	using DataModel.Repositories;

	public sealed class WasabiOrdersBearerAuthenticationProvider : IOAuthBearerAuthenticationProvider
	{
		#region Fields and properties

		private readonly IUserSessionRepository _sessionRepository;
		#endregion

		#region Constructors

		public WasabiOrdersBearerAuthenticationProvider(IUserSessionRepository sessionRepository)
		{
			_sessionRepository = sessionRepository;
		}
		#endregion

		#region IOAuthBearerAuthenticationProvider implementation

		public Task RequestToken(OAuthRequestTokenContext context)
		{
			return Task.FromResult(0);
		}

		public Task ApplyChallenge(OAuthChallengeContext context)
		{
			return Task.FromResult(0);
		}

		public async Task ValidateIdentity(OAuthValidateIdentityContext context)
		{
			if (context.Request.Path.Value.IndexOf("logout", StringComparison.OrdinalIgnoreCase) != -1)
			{
				context.Validated(context.Ticket);
				return;
			}

			ClaimsIdentity userIdentity = context.Ticket?.Identity;
			if (userIdentity != null)
			{
				Guid sessionID = userIdentity.GetSessionID();
				Guid userID = userIdentity.GetUserID();

				bool sessionExists = await _sessionRepository
					.Query()
					.AnyAsync(x => x.Id == sessionID && x.UserID == userID);
				if (sessionExists)
				{
					context.Validated(context.Ticket);
				}
				else
				{
					context.SetError($"Session with ID '{sessionID}' does not exist.");
				}
			}
			else
			{
				context.SetError("Invalid token format.");
			}
		}
		#endregion
	}
}