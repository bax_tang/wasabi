﻿using System;
using System.IO;
using System.Web.Http;

using Swashbuckle.Application;
using Swashbuckle.Swagger;
using Swashbuckle.SwaggerUi;

namespace WasabiOrders.WebService
{
    using Filters;

	public static class SwaggerConfig
	{
		#region Public class methods

		public static void Configure(HttpConfiguration configuration)
		{
			configuration
				.EnableSwagger(ConfigureSwaggerDocs)
				.EnableSwaggerUi(ConfigureSwaggerUi);
		}
		#endregion

		#region Private class methods

		private static void ConfigureSwaggerDocs(SwaggerDocsConfig docsConfig)
		{
			docsConfig.PrettyPrint();
			docsConfig.SingleApiVersion("v1", "WasabiOrders Web API");

			docsConfig.DescribeAllEnumsAsStrings();
			docsConfig.IgnoreObsoleteActions();

            docsConfig.OperationFilter<PaginationOperationFilter>();
            docsConfig.OperationFilter<ActionParameterOperationFilter>();
			docsConfig.OperationFilter<TokenParameterOperationFilter>();

			string appDirectory = Directory.GetCurrentDirectory();
			var commentFiles = Directory.EnumerateFiles(appDirectory, "WasabiOrders.*.xml");
			foreach (string commentFile in commentFiles)
			{
				docsConfig.IncludeXmlComments(commentFile);
			}
		}

		private static void ConfigureSwaggerUi(SwaggerUiConfig uiConfig)
		{
			uiConfig.DisableValidator();
			uiConfig.DocExpansion(DocExpansion.List);
			uiConfig.SupportedSubmitMethods("GET", "POST", "PUT", "DELETE", "OPTIONS");
		}
		#endregion
	}
}