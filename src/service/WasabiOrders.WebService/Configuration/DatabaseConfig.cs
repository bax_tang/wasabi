﻿using System;
using System.Data.Entity;

using NLog;

namespace WasabiOrders.WebService
{
	using DataModel;
	using DataModel.Migrations;

	public static class DatabaseConfig
	{
		#region Public class methods

		/// <summary>
		/// Force create or update database
		/// </summary>
		/// <param name="serviceConfiguration">Service configuration</param>
		public static void InitDatabase(WebServiceConfiguration serviceConfiguration, ILogger logger)
		{
			if (serviceConfiguration.UseDatabaseMigrations)
			{
				Database.SetInitializer(new MigrateDatabaseToLatestVersion<WasabiDbContext, WasabiMigrationConfiguration>(true));
			}
			else
			{
				Database.SetInitializer(new NullDatabaseInitializer<WasabiDbContext>());
			}

			using (WasabiDbContext context = new WasabiDbContext(serviceConfiguration.ConnectionString))
			{
#if DEBUG
				context.Database.Log = new Action<string>(logger.Debug);
#endif

				context.SaveChanges();
			}
		}
		#endregion
	}
}