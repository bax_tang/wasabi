﻿using System;
using System.Web.Http;

using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;

using Autofac;
using Autofac.Integration.WebApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Owin;

namespace WasabiOrders.WebService
{
	public static class WebApiConfig
	{
		#region Public class methods

		public static void Configure(IAppBuilder appBuilder, ILifetimeScope lifetimeScope, HttpConfiguration configuration)
		{
			ConfigureAuth(appBuilder, lifetimeScope);
			ConfigureCors(appBuilder);
			ConfigureHandlers(configuration);
			ConfigureWebApi(configuration);

			configuration.DependencyResolver = new AutofacWebApiDependencyResolver(lifetimeScope);

			appBuilder.UseAutofacMiddleware(lifetimeScope);
			appBuilder.UseAutofacWebApi(configuration);
			appBuilder.UseWebApi(configuration);
		}
		#endregion

		#region Private class methods

		private static void ConfigureAuth(IAppBuilder appBuilder, ILifetimeScope lifetimeScope)
		{
			var authOptions = lifetimeScope.Resolve<OAuthBearerAuthenticationOptions>();

			appBuilder.UseOAuthBearerAuthentication(authOptions);
		}

		private static void ConfigureCors(IAppBuilder appBuilder)
		{
			appBuilder.UseCors(CorsOptions.AllowAll);
		}

		private static void ConfigureHandlers(HttpConfiguration configuration)
		{
			configuration.MessageHandlers.Add(new CacheControlMessageHandler());
		}

		private static void ConfigureWebApi(HttpConfiguration configuration)
		{
			configuration.MapHttpAttributeRoutes();

			configuration.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{action}/{id}",
				defaults: new { id = RouteParameter.Optional }
				);

			JsonSerializerSettings settings = configuration.Formatters.JsonFormatter.SerializerSettings;
			settings.Formatting = Formatting.None;
			settings.ContractResolver = new DefaultContractResolver();
			settings.Converters.Add(new GuidConverter());
			settings.Converters.Add(new StringEnumConverter());
		}
		#endregion
	}
}