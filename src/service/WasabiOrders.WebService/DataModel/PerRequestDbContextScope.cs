﻿using System;
using System.Data.Entity;

using Autofac;

namespace WasabiOrders.WebService.DataModel
{
    internal class PerRequestDbContextScope : IDbContextScope, IDisposable
    {
        #region Fields and properties

        private readonly ILifetimeScope _lifetimeScope;
        #endregion

        #region Constructors

        public PerRequestDbContextScope(ILifetimeScope lifetimeScope)
        {
            if (lifetimeScope == null)
            {
                throw new ArgumentNullException(nameof(lifetimeScope), "Lifetime scope can't be null.");
            }

            _lifetimeScope = lifetimeScope;
        }
        #endregion

        #region Public class methods

        public DbContext GetDbContext()
        {
            return _lifetimeScope.Resolve<DbContext>();
        }

        public void Dispose()
        {
            _lifetimeScope?.Dispose();
        }
        #endregion
    }
}