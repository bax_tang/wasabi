﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;

using Autofac;

namespace WasabiOrders.WebService.Modules
{
	public sealed class AuthenticationModule : Autofac.Module
	{
		#region Constructors

		public AuthenticationModule() : base() { }
		#endregion

		#region Module methods overriding

		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			builder.RegisterType<JwtSecurityTokenHandler>()
				.AsSelf()
				.SingleInstance();

			builder.Register(CreateValidationParameters)
				.AsSelf()
				.SingleInstance();

			builder.RegisterType<WasabiOrdersJwtFormat>().SingleInstance();
			builder.RegisterType<WasabiOrdersBearerAuthenticationProvider>()
				.As<IOAuthBearerAuthenticationProvider>()
				.AsSelf()
				.SingleInstance();

			builder.Register(CreateAuthenticationOptions)
				.AsSelf()
				.SingleInstance();
		}

		private static OAuthBearerAuthenticationOptions CreateAuthenticationOptions(IComponentContext context)
		{
			var provider = context.Resolve<IOAuthBearerAuthenticationProvider>();
			var tokenFormat = context.Resolve<WasabiOrdersJwtFormat>();

			var options = new OAuthBearerAuthenticationOptions
			{
				AccessTokenFormat = tokenFormat,
				AuthenticationMode = AuthenticationMode.Active,
				AuthenticationType = "Bearer",
				Provider = provider
			};
			return options;
		}

		private static TokenValidationParameters CreateValidationParameters(IComponentContext context)
		{
			var configuration = context.Resolve<WebServiceConfiguration>();
			var signingKey = new SymmetricSecurityKey(Convert.FromBase64String(configuration.SecurityKey));

			var parameters = new TokenValidationParameters
			{
				AuthenticationType = AuthConstants.DefaultAuthenticationType,
				IssuerSigningKey = signingKey,
				NameClaimType = ClaimTypes.Name,
				RoleClaimType = ClaimTypes.Role,
				RequireExpirationTime = true,
				RequireSignedTokens = true,
				ValidAudience = AuthConstants.Audience,
				ValidIssuer = AuthConstants.Issuer,
				ValidateIssuer = true,
				ValidateLifetime = true,
				ValidateIssuerSigningKey = true
			};
			return parameters;
		}
		#endregion
	}
}