﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Autofac;
using Autofac.Core;
using AutoMapper;
using AutoMapper.Configuration;

namespace WasabiOrders.WebService.Modules
{
	using DataModel;
	using DomainModel.AutoMappings;
	using DomainModel.Providers;
	using DomainModel.Providers.Impl;
	using DomainModel.Services;
	using DomainModel.Services.Impl;

	public sealed class DomainModelModule : Autofac.Module
	{
		#region Constructors

		public DomainModelModule() : base() { }
		#endregion

		#region Module methods overriding

		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

            RegisterAutoMappings(builder);
            RegisterDomainModelProviders(builder);
			RegisterDomainModelServices(builder);
		}
		#endregion

		#region Private class methods

		private static void RegisterAutoMappings(ContainerBuilder builder)
		{
			Assembly mappingsAssembly = typeof(IAutoMap).Assembly;

			builder.RegisterAssemblyTypes(mappingsAssembly)
				.Where(typeof(IAutoMap).IsAssignableFrom)
				.AsImplementedInterfaces()
				.AsSelf()
				.SingleInstance();

			builder.Register(CreateMapper)
				.As<IMapper>()
				.AsSelf()
				.SingleInstance();

			builder.RegisterType<AutoMapperMappingProvider>()
				.As<IMappingProvider>()
				.AsSelf()
				.SingleInstance();
		}

        private static void RegisterDomainModelProviders(ContainerBuilder builder)
        {
			builder.RegisterType<ActivityLogProvider>()
				.As<IActivityLogProvider>()
				.AsSelf()
				.SingleInstance();

			builder.RegisterType<GlobalSettingProvider>()
				.As<IGlobalSettingProvider>()
				.AsSelf()
				.SingleInstance();

			builder.RegisterType<IdentityTokenProvider>()
				.As<IIdentityTokenProvider>()
				.AsSelf()
				.SingleInstance();

			builder.RegisterType<BoxFileUploadProvider>()
				.As<IFileUploadProvider>()
				.AsSelf()
				.SingleInstance();

            builder.RegisterType<SqlProvider>()
                .As<ISqlProvider>()
                .AsSelf()
                .SingleInstance();
        }

		private static void RegisterDomainModelServices(ContainerBuilder builder)
        {
            Assembly servicesAssembly = typeof(DomainModelServiceBase).Assembly;

			builder.RegisterAssemblyTypes(servicesAssembly)
				.Where(typeof(DomainModelServiceBase).IsAssignableFrom)
				.AsImplementedInterfaces()
				.AsSelf()
				.SingleInstance();
		}

		private static Mapper CreateMapper(IComponentContext context)
		{
			var configExpression = new MapperConfigurationExpression();

			var mappings = context.Resolve<IEnumerable<IAutoMap>>();
			foreach (IAutoMap autoMap in mappings)
			{
				autoMap.Apply(configExpression);
			}

			var mapperConfiguration = new MapperConfiguration(configExpression);

#if DEBUG
			mapperConfiguration.AssertConfigurationIsValid();
#endif

			return new Mapper(mapperConfiguration);
		}
		#endregion
	}
}