﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.Http;

using Autofac;
using Autofac.Integration.Owin;
using Autofac.Integration.WebApi;
using NLog;

namespace WasabiOrders.WebService.Modules
{
	using Controllers;

    public sealed class ApplicationModule : Autofac.Module
    {
        #region Constructors

        public ApplicationModule() : base() { }
        #endregion

        #region Module methods overriding

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            string appDirectory = Directory.GetCurrentDirectory();
			builder.RegisterModule(new AuthenticationModule());
            builder.RegisterModule(new ConfigurationModule(appDirectory));
            builder.RegisterModule(new DataModelModule());
			builder.RegisterModule(new DomainModelModule());

			builder.Register(CreateServiceLogger)
				.Named<ILogger>("Service")
				.As<ILogger>()
				.InstancePerDependency();
			builder.Register(CreateSqlLogger)
				.Named<ILogger>("Sql")
				.As<ILogger>()
				.InstancePerDependency();

			HttpConfiguration configuration = new HttpConfiguration();
            builder.RegisterWebApiFilterProvider(configuration);
            builder.RegisterWebApiModelBinderProvider();
            builder.RegisterInstance(configuration);

			builder.Register(CreateExceptionFilter)
				.AsWebApiExceptionFilterFor<ApiController>()
				.SingleInstance();

			Assembly apiAssembly = typeof(ApiControllerBase).Assembly;
			builder.RegisterApiControllers(apiAssembly).InstancePerRequest();
        }
		#endregion

		#region Private class methods

		private static Logger CreateServiceLogger(IComponentContext context)
		{
			return LogManager.GetLogger("Service");
		}

		private static Logger CreateSqlLogger(IComponentContext context)
		{
			return LogManager.GetLogger("Sql");
		}

		private static WasabiOrdersExceptionFilter CreateExceptionFilter(IComponentContext context)
		{
			var logger = context.ResolveNamed<ILogger>("Service");

			return new WasabiOrdersExceptionFilter(logger);
		}
        #endregion
    }
}