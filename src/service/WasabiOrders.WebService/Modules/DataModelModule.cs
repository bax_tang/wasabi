﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Reflection;

using Autofac;
using Autofac.Core.Lifetime;
using NLog;

namespace WasabiOrders.WebService.Modules
{
    using DataModel;
	using DataModel.Mappings;
	using DataModel.Repositories;

	public sealed class DataModelModule : Autofac.Module
    {
		#region Constructors

		public DataModelModule() : base() { }
		#endregion

		#region Module methods overriding

		protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
			
            builder.Register(CreateContext)
                .As<IDisposable, DbContext>()
                .AsSelf()
                .InstancePerRequest();

            builder.Register(CreateContextScope)
                .As<IDisposable, IDbContextScope>()
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .AsSelf()
                .SingleInstance();

			Assembly dataAssembly = typeof(WasabiDbContext).Assembly;
			
			builder.RegisterAssemblyTypes(dataAssembly)
				.Where(typeof(BaseRepository).IsAssignableFrom)
				.AsImplementedInterfaces()
				.AsSelf()
				.SingleInstance();
        }
        #endregion

        #region Private class methods

        private static WasabiDbContext CreateContext(IComponentContext context)
        {
            var configuration = context.Resolve<WebServiceConfiguration>();
			
            WasabiDbContext dbContext = new WasabiDbContext(configuration.ConnectionString);
			
#if DEBUG
			ILogger logger = context.ResolveNamed<ILogger>("Sql");
			dbContext.Database.Log = new Action<string>(logger.Debug);
#endif

			return dbContext;
        }

        private static PerRequestDbContextScope CreateContextScope(IComponentContext context)
        {
            ILifetimeScope rootScope = context.Resolve<ILifetimeScope>();
            ILifetimeScope nestedScope = rootScope.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);

            return new PerRequestDbContextScope(nestedScope);
        }
        #endregion
    }
}