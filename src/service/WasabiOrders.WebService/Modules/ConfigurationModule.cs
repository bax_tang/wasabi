﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

using Autofac;
using NLog;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace WasabiOrders.WebService.Modules
{
    public sealed class ConfigurationModule : Module
    {
        #region Constants and fields

        private const string ConfigDirectory = "Cfg";
        private const string ConfigFileName = "WebServiceConfiguration.json";
        private const string ConfigDirectoryNotFound = "Directory '\\Cfg' doesn't exist.";
        private const string ConfigFileNotFound = "File '\\Cfg\\WebServiceConfiguration.json' doesn't exist.";
        private const string InitializationSucceeded = "Service configuration initialized successfully.";
        private const string UsingDefaultValues = "Using default values.";

        private readonly string _applicationDirectory;

        private JsonSerializerSettings _serializationSettings;
        #endregion

        #region Constructors

        public ConfigurationModule(string applicationDirectory) : base()
        {
            if (!Directory.Exists(applicationDirectory))
            {
                throw new DirectoryNotFoundException($"Application directory '{applicationDirectory}' doesn't exist.");
            }

            _applicationDirectory = applicationDirectory;

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Culture = CultureInfo.InvariantCulture,
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                DefaultValueHandling = DefaultValueHandling.Ignore,
                Formatting = Formatting.Indented,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            settings.Converters.Add(new StringEnumConverter());
            _serializationSettings = settings;
        }
        #endregion

        #region Module methods overriding

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(CreateConfiguration).SingleInstance();
        }
        #endregion

        #region Private class methods

        private WebServiceConfiguration CreateConfiguration(IComponentContext context)
        {
            ILogger logger = context.ResolveNamed<ILogger>("Service");

            WebServiceConfiguration configuration = null;

            string filePath = null; string errorMessage = null;
            bool fileExists = TryGetConfigFilePath(out filePath, out errorMessage);
            if (fileExists)
            {
                JsonSerializer serializer = JsonSerializer.Create(_serializationSettings);
                using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8, false, 8192))
                {
                    object result = serializer.Deserialize(reader, typeof(WebServiceConfiguration));

                    configuration = result as WebServiceConfiguration;
                }
            }
            else
            {
                configuration = WebServiceConfiguration.Instance;
                logger.Warn(string.Concat(errorMessage, " ", UsingDefaultValues));
            }

            if (configuration != null)
            {
                WebServiceConfiguration.SetInstance(configuration);
                logger.Info(InitializationSucceeded);
            }

            return configuration;
        }

        private bool TryGetConfigFilePath(out string filePath, out string errorMessage)
        {
            bool result = false; filePath = null; errorMessage = null;

            string configDirectory = Path.Combine(_applicationDirectory, ConfigDirectory);
            if (Directory.Exists(configDirectory))
            {
                string configFilePath = Path.Combine(configDirectory, ConfigFileName);
                if (File.Exists(configFilePath))
                {
                    filePath = configFilePath;
                    result = true;
                }
                else
                {
                    errorMessage = ConfigFileNotFound;
                }
            }
            else
            {
                errorMessage = ConfigDirectoryNotFound;
            }

            return result;
        }
        #endregion
    }
}