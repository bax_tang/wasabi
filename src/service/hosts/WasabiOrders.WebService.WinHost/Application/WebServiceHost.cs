﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Web;
using System.Web.Http;

using Microsoft.Owin.Hosting;
using Owin;

using Autofac;
using NLog;

namespace WasabiOrders.WebService
{
	using DataModel;
	using DataModel.Migrations;

    internal class WebServiceHost
    {
        #region Fields and properties

        private readonly ILifetimeScope _lifetimeScope;

        private readonly ILogger _logger;

        private IDisposable _serviceInstance;
        #endregion

        #region Constructors

        public WebServiceHost(ILifetimeScope lifetimeScope, ILogger logger)
        {
            if (lifetimeScope == null)
            {
                throw new ArgumentNullException(nameof(lifetimeScope), "Lifetime scope can't be null.");
            }
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger), "Logger can't be null.");
            }

            _lifetimeScope = lifetimeScope;
            _logger = logger;
        }
        #endregion

        #region Public class methods

        public bool Start()
        {
            bool result = false;

            try
            {
                var configuration = _lifetimeScope.Resolve<WebServiceConfiguration>();

				StartOptions startOptions = CreateStartOptions(configuration);
				
                _serviceInstance = WebApp.Start(startOptions, BuildApplication);
                _logger.Debug("Web service started successfully.");
                result = true;
            }
            catch (Exception exc)
            {
                _logger.Error(exc, exc.Message);
            }

            return result;
        }

        public bool Stop()
        {
            _serviceInstance?.Dispose();
            _lifetimeScope.Dispose();
            _logger.Debug("Web service stopped successfully.");
            return true;
        }
        #endregion

        #region Private class methods

		private StartOptions CreateStartOptions(WebServiceConfiguration configuration)
		{
			StartOptions startOptions = new StartOptions()
			{
				Port = configuration.ServicePort
			};

			IPHostEntry hostEntry = Dns.GetHostEntry(configuration.HostName);
			IPAddress baseAddress = Array.Find(hostEntry.AddressList, IsInterNetworkAddress);
			
			startOptions.Urls.Add(string.Format("http://{0}:{1}/", baseAddress, configuration.ServicePort));
			startOptions.Urls.Add(string.Format("http://{0}:{1}/", configuration.HostName, configuration.ServicePort));
			startOptions.Urls.Add(string.Format("http://localhost:{0}/", configuration.ServicePort));
			startOptions.Urls.Add(string.Format("http://127.0.0.1:{0}/", configuration.ServicePort));

			return startOptions;
		}

        private void BuildApplication(IAppBuilder appBuilder)
        {
            HttpConfiguration appConfiguration = _lifetimeScope.Resolve<HttpConfiguration>();
			WebServiceConfiguration serviceConfiguration = _lifetimeScope.Resolve<WebServiceConfiguration>();

			ILogger sqlLogger = _lifetimeScope.ResolveNamed<ILogger>("Sql");

			DatabaseConfig.InitDatabase(serviceConfiguration, sqlLogger);
			SwaggerConfig.Configure(appConfiguration);
			WebApiConfig.Configure(appBuilder, _lifetimeScope, appConfiguration);
        }

		private static bool IsInterNetworkAddress(IPAddress ipAddress)
		{
			return (ipAddress != null) && (ipAddress.AddressFamily == AddressFamily.InterNetwork);
		}
        #endregion
    }
}