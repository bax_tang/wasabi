﻿using System;
using System.Collections.Generic;

using Autofac;
using NLog;
using Topshelf;
using Topshelf.Autofac;
using Topshelf.HostConfigurators;
using Topshelf.ServiceConfigurators;

namespace WasabiOrders.WebService
{
    using Modules;

    internal class EntryPoint
    {
        #region The entry point

        internal static int Main(string[] args)
        {
			AppDomain.CurrentDomain.UnhandledException += ProcessUnhandledException;

			TopshelfExitCode exitCode = HostFactory.Run(ConfigureHost);

			return (int)exitCode;
        }
        #endregion

        #region Private class methods

		private static void ProcessUnhandledException(object sender, UnhandledExceptionEventArgs args)
		{
			ILogger logger = LogManager.GetCurrentClassLogger();

			Exception exc = args.ExceptionObject as Exception;
			if (args.IsTerminating)
			{
				Environment.ExitCode = exc.HResult;
				logger.Fatal(exc, exc.Message);
			}
			else
			{
				logger.Error(exc, exc.Message);
			}
		}

        private static IContainer CreateContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterModule(new ApplicationModule());

            builder.Register(CreateHost).SingleInstance();

            return builder.Build();
        }

        private static void ConfigureHost(HostConfigurator configurator)
        {
            IContainer container = CreateContainer();

            var serviceConfig = container.Resolve<WebServiceConfiguration>();

            configurator.SetServiceName(serviceConfig.ServiceName);
            configurator.SetDescription(serviceConfig.ServiceDescription);

            configurator.UseNLog();
            configurator.UseAutofacContainer(container);

            configurator.ApplyCommandLine();

            configurator.Service<WebServiceHost>(ConfigureServiceHost);

            configurator.RunAsLocalSystem();
        }

        private static void ConfigureServiceHost(ServiceConfigurator<WebServiceHost> configurator)
        {
            configurator.ConstructUsingAutofacContainer();

            configurator.WhenStarted(StartServiceHost);
            configurator.WhenStopped(StopServiceHost);
        }

        private static WebServiceHost CreateHost(IComponentContext context)
        {
            ILifetimeScope currentScope = context.Resolve<ILifetimeScope>();

            ILifetimeScope nestedScope = currentScope.BeginLifetimeScope();
            ILogger logger = nestedScope.ResolveNamed<ILogger>("Service");

            return new WebServiceHost(nestedScope, logger);
        }

        private static bool StartServiceHost(WebServiceHost host, HostControl control)
        {
            return host.Start();
        }

        private static bool StopServiceHost(WebServiceHost host, HostControl control)
        {
            return host.Stop();
        }
        #endregion
    }
}