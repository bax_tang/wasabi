﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Общие сведения об этой сборке предоставляются следующим набором 
// атрибутов. Отредактируйте значения этих атрибутов, чтобы изменить
// общие сведения об этой сборке.
[assembly: AssemblyTitle("WasabiOrders.WebService")]
[assembly: AssemblyDescription("WasabiOrders web service host")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Kalinov Inc")]
[assembly: AssemblyProduct("WasabiOrders web service")]
[assembly: AssemblyCopyright("Copyright © Kalinov Inc 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Установка значения False в параметре ComVisible делает типы в этой сборке невидимыми 
// для COM-компонентов.  Если необходим доступ к типу в этой сборке из 
// COM, следует установить атрибут ComVisible в TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("ecd09df6-50b1-4a31-9953-c02bec5ff6b4")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//   Номер сборки
//      Редакция
//
[assembly: AssemblyVersion("1.0.0.*")]
[assembly: AssemblyFileVersion("1.0.0.1")]
