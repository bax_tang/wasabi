﻿
SET NOCOUNT ON;

DECLARE @Users TABLE
(
	[UserID] UNIQUEIDENTIFIER NOT NULL,
	[AccountName] NVARCHAR(64),
	[FirstName] NVARCHAR(64),
	[LastName] NVARCHAR(64),
	[Email] NVARCHAR(128),
	[PasswordHash] NVARCHAR(MAX),
	[Discount] FLOAT,
	[CreatedAt] DATETIME
);

DECLARE @actionDateTime DATETIME = GETUTCDATE();

INSERT INTO @Users ([UserID], [AccountName], [FirstName], [LastName], [Email], [PasswordHash], [Discount], [CreatedAt])
VALUES
	(
		'0FFDE532-AF7D-4E80-8364-8C21C47B8C39',
		N'kalinov_dmitry',
		N'Дмитрий',
		N'Калинов',
		N'kadmvl@yandex.ru',
		N'$2a$12$wfOb0ZOhstZ.z1lIQhxpC./LY/eBM0P29CHdvi1qBY.ifRJPkp9cC', -- yakrevedko11
		0.2,
		@actionDateTime
	),
	(
		'DC8DE4C5-F4EA-4951-8DB5-C1DD5DBA7840',
		N'margo7083',
		N'Маргарита',
		N'Жиляева',
		N'mgvik@yandex.ru',
		N'$2a$12$wfOb0ZOhstZ.z1lIQhxpC./LY/eBM0P29CHdvi1qBY.ifRJPkp9cC', -- yakrevedko11
		0.2,
		@actionDateTime
	);

MERGE [dbo].[Users] AS [U]
USING @Users AS [US] ON ([U].[UserID] = [US].[UserID])
WHEN MATCHED THEN
	UPDATE
	SET
		[U].[AccountName] = [US].[AccountName],
		[U].[FirstName] = [US].[FirstName],
		[U].[LastName] = [US].[LastName],
		[U].[Email] = [US].[Email],
		[U].[PasswordHash] = [US].[PasswordHash],
		[U].[Discount] = [US].[Discount]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([UserID], [AccountName], [FirstName], [LastName], [Email], [PasswordHash], [Discount], [CreatedAt])
	VALUES
	(
		[US].[UserID],
		[US].[AccountName],
		[US].[FirstName],
		[US].[LastName],
		[US].[Email],
		[US].[PasswordHash],
		[US].[Discount],
		[US].[CreatedAt]
	);