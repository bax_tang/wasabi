﻿
SET NOCOUNT ON;

DECLARE @ProductCategories TABLE
(
	[Name] NVARCHAR(64),
	[ExternalFolderID] VARCHAR(16),
	[CreatedAt] DATETIME
);

DECLARE @actionDateTime DATETIME = GETUTCDATE();

INSERT INTO @ProductCategories ([Name], [ExternalFolderID], [CreatedAt])
VALUES
	(N'Суши', '37893459765', @actionDateTime),
	(N'Роллы', '37893514424', @actionDateTime),
	(N'Сеты', '37893532890', @actionDateTime),
	(N'Хосомаки', '37893616007', @actionDateTime),
	(N'Горячие роллы', '37893627206', @actionDateTime),
	(N'Теплые и запеченные роллы', '37895046440', @actionDateTime),
	(N'Сашими', '37895063943', @actionDateTime),
	(N'Гунканы', '37895079212', @actionDateTime),
	(N'Супы', '37895085156', @actionDateTime),
	(N'Горячее', '37895103758', @actionDateTime),
	(N'Салаты', '37895126883', @actionDateTime),
	(N'Кокосовая вода', '37895128501', @actionDateTime),
	(N'Роллы 1/2', '37895200027', @actionDateTime),
	(N'Десерты', '37895202838', @actionDateTime),
	(N'Новинки exotic', '37895216949', @actionDateTime),
	(N'Добавка!', '37895251048', @actionDateTime),
	(N'Пицца', '37895244980', @actionDateTime);

MERGE [dbo].[ProductCategories] AS [PC]
USING @ProductCategories AS [PCS] ON ([PC].[Name] = [PCS].[Name])
WHEN MATCHED THEN
	UPDATE
	SET [PC].[ExternalFolderID] = [PCS].[ExternalFolderID]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Name], [ExternalFolderID], [CreatedAt])
	VALUES
	(
		[PCS].[Name],
		[PCS].[ExternalFolderID],
		[PCS].[CreatedAt]
	);