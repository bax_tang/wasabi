﻿
SET NOCOUNT ON;

DECLARE @ActivityTypes TABLE ([Name] VARCHAR(64), [Text] VARCHAR(1024));

INSERT INTO @ActivityTypes ([Name], [Text])
VALUES
	('UserLogin', 'logged in'),
	('UserLogout', 'logged out'),
	('ProductCreated', 'added new product'),
	('ProductUpdated', 'updated an existing product'),
	('ProductDeleted', 'deleted an existing product'),
	('OrderCreated', 'added new order'),
	('OrderUpdated', 'updated an existing order'),
	('ImageUploaded', 'uploaded new image'),
	('CommentAdded', 'added new comment'),
	('CommentUpdated', 'updated an existing comment'),
	('CommentDeleted', 'deleted an existing comment');

MERGE [dbo].[ActivityTypes] AS [AT]
USING @ActivityTypes AS [ATS] ON ([AT].[Name] = [ATS].[Name])
WHEN MATCHED THEN
	UPDATE
	SET [AT].[Text] = [ATS].[Text]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Name], [Text])
	VALUES ([ATS].[Name], [ATS].[Text]);