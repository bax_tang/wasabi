﻿
SET NOCOUNT ON;

DECLARE @GlobalSettings TABLE
(
	[Name] VARCHAR(64),
	[Value] VARCHAR(2048),
	[CreatedAt] DATETIME
);

DECLARE @actionDateTime DATETIME = GETUTCDATE();

INSERT INTO @GlobalSettings ([Name], [Value], [CreatedAt])
VALUES
	('BoxClientID', '3mbi3klalacchxgxe0uzlbxo1r7d7imz', @actionDateTime),
	('BoxClientSecret', 'l9WgY9iuJtAQVPSk5xGozjXkJcisSZep', @actionDateTime),
	('BoxEnterpriseID', '26438637', @actionDateTime),
	('BoxPrivateKey',
		'-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-256-CBC,768340ACD3D98354554C67778978E3C2

UpoaNTuBFxEqp/hnIrQ7n/fJ02g/OyEMz+MtZXVKJFACKgeOlvzg1gcI1hewrOej
hKvdgvvNRqyJoTkB38sktqLzKkPfAV99waqAiirMtx+aZwAycsdIi8M83ZOZ9pX0
1yp8YaEt7cW44IgD/Zp4d0+PL6YASkaGkPhS90SxYwHYt8mmXW0db0iUc0mgxth4
6Zp2MESSipvd72L1S0+KBPQOCZPg6HZuWXHyo0gh9Ze+FfVUdneSEjxfxbDxdIBx
LBMsYNSinbGFfwraqd6RRam9Bs+blzM8Cn85cb0yV+TGewriR+Nx3EnV0lovx8h6
Epb5NJcWyCxCVIJwOj9NSzbbTAzdj4rdzJ0YQWMDKml+FZhZjL0tyZAJDLErFMdP
iTI/bM5mnylKjXvK+Af/p4nUDEuZfN/bHysQ2GoJoA4tJOOeMKUW4EGjEYqMr+MN
KJMq+hlzAFOPintGAlBdXmGtH1q5PDlEURjYXr2ATiSTZEOINmx5XAlmmzvIhiG7
wgSdlkYDALXKGJ2s04tONObyj4siUAante8F7+8532/bZuuZDsKJYHEyAxKjg2Qh
G7R4PeJIxfxYH+U64A6rEQpJpU0THqePmevPCQgZNrkawUBBSAxKeJ4Brwg9VedH
1/WJc79zJYUL8MZzyJ9o7oYzmavLrOlYOE/AIbSOiFT0W1pbl42YLWnbLvhebm2r
bD2Hgm4Emyd2PKuhSE5EbtxMtMNZGwV9efAO1Dpl+y+nzTak8EUaG39iWdOjGKjp
sBcnNJnCS3M14Nwi5iaov26yd8SaOm2TnRIznLqVLvFIlMHLev53bf0TNqzC62Hd
oxUd03gXW6cHYRwp4oPnMUdKi5N4qk6YeMDaNxwWyS8NP31Tmv4N8JRtrU0r4wvP
CY3Z94RtVG0SyHOb/0+OyrH3P1Rz3t/frT8BjJeRQdw6XRDWtb3IluFXBUQqKh4C
WoJwdvsHukt6jN3vPnD8Qsk3409UVL85ZR88U1E3uya+khCgEyePAvoNN0UXwjqy
xJiRIEEf0zjbUe6sWTO6G67zdf59YZnLEJKz/4LHHVjvoKz8XZj4Hd+gJ5s3Jivk
1SBjLmqEiMWcnVJF3UMGwOJLTCnALdFQhqmjfcVoa9hP9Ik6PLT8OZGGi2gMkOCU
V6SecWQJtIvxblABQH7oL1TBzmjGl24h60W4ZWrTYyDfDnx3Yw7UlQLBJ7v04Fu5
Nff7Uczax8iEi1F4kRynekDhrcG+79IO8MXSIEfk8V+DR9n0UH3YVMXaDNoiTG7/
Y4PgVhkjx7sP+qHZGHHQu3ILATZ+P957/mz7P/rETq1KqIs6H6WSbv0K8NiiTj+S
zVGvrsOaANnTyZXZOo5T3nJlm+5COcEng5BrLmErgevE3YXSsN162wCW5spLpjDT
TrckJX0Udwpz7M/AQjqBByhxh0jfnuSGof/FMZYHaBzlQWXw8lZjGZv70concXic
6fFpv9+3keHnnR2C9g8dkNrlJqlddIQpuckxmIpTgXJvgIQ7y8HfScf4nMqEtGBH
9jqTuP4vEYxvcVlkkcvf3cyYjcK6TWluAgy9W6SV5hkWWb/xrlbNmF4JmbC4jToA
-----END RSA PRIVATE KEY-----', @actionDateTime),
	('BoxPrivateKeyPassphrase', '*8LnQM.6', @actionDateTime),
	('BoxPublicKeyID', 'xm2xdnxm', @actionDateTime),
	('BoxUserID', '2355238573', @actionDateTime),
	('BoxProductsFolder', '37891861345', @actionDateTime),
	('BoxOrdersFolder', '37891857907', @actionDateTime);

MERGE [dbo].[GlobalSettings] AS [GS]
USING @GlobalSettings AS [GSS] ON ([GS].[Name] = [GSS].[Name])
WHEN MATCHED THEN
	UPDATE
	SET [GS].[Value] = [GSS].[Value]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Name], [Value], [CreatedAt])
	VALUES
	(
		[GSS].[Name],
		[GSS].[Value],
		[GSS].[CreatedAt]
	);