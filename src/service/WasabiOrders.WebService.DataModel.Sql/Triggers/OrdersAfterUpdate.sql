﻿
CREATE TRIGGER [trg_orders_after_update] ON [dbo].[Orders]
WITH EXECUTE AS N'dbo'
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[Orders] WITH(ROWLOCK)
	SET [UpdatedAt] = GETUTCDATE()
	FROM Inserted AS UpdatedOrders
	INNER JOIN [dbo].[Orders] AS Orders ON ([Orders].[OrderID] = [UpdatedOrders].[OrderID]);
END;