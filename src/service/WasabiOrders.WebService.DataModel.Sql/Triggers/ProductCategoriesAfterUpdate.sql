﻿
CREATE TRIGGER [trg_productcategories_after_update] ON [dbo].[ProductCategories]
WITH EXECUTE AS N'dbo'
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[ProductCategories] WITH(ROWLOCK)
	SET [UpdatedAt] = GETUTCDATE()
	FROM Inserted AS UpdatedCategories
	INNER JOIN [dbo].[ProductCategories] AS Categories ON ([Categories].[CategoryID] = [UpdatedCategories].[CategoryID]);
END;