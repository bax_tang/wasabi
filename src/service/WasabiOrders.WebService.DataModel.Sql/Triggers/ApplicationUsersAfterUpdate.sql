﻿
CREATE TRIGGER [trg_applicationusers_after_update] ON [dbo].[Users]
WITH EXECUTE AS N'dbo'
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[Users] WITH(ROWLOCK)
	SET [UpdatedAt] = GETUTCDATE()
	FROM [Inserted] AS [UpdatedUsers]
	INNER JOIN [dbo].[Users] AS [U] ON ([U].[UserID] = [UpdatedUsers].[UserID]);
END;