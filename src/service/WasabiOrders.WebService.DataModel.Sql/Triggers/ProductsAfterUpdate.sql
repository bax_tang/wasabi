﻿
CREATE TRIGGER [trg_products_after_update] ON [dbo].[Products]
WITH EXECUTE AS N'dbo'
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[Products] WITH(ROWLOCK)
	SET [UpdatedAt] = GETUTCDATE()
	FROM Inserted AS UpdatedProducts
	INNER JOIN [dbo].[Products] AS Products ON ([Products].[ProductID] = [UpdatedProducts].[ProductID]);
END;