﻿
CREATE TRIGGER [trg_globalsettings_after_update] ON [dbo].[GlobalSettings]
WITH EXECUTE AS N'dbo'
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[GlobalSettings] WITH(ROWLOCK)
	SET [UpdatedAt] = GETUTCDATE()
	FROM [Inserted] AS [UpdatedSettings]
	INNER JOIN [dbo].[GlobalSettings] AS [Settings] ON ([Settings].[SettingID] = [UpdatedSettings].[SettingID]);
END;