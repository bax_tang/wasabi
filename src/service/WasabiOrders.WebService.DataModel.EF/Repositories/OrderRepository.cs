﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IOrderRepository : IObjectEntityRepository<Guid, Order> { }

	public class OrderRepository : ObjectEntityRepository<Guid, Order>, IOrderRepository
	{
		#region Constructors

		public OrderRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}