﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IProductCategoryRepository : IObjectEntityRepository<Guid, ProductCategory> { }

	public class ProductCategoryRepository : ObjectEntityRepository<Guid, ProductCategory>, IProductCategoryRepository
	{
		#region Constructors

		public ProductCategoryRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion

		#region Public class methods

		#endregion
	}
}