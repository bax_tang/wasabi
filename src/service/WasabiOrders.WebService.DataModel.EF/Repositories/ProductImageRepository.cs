﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IProductImageRepository : IObjectEntityRepository<Guid, ProductImage> { }

	public class ProductImageRepository : ObjectEntityRepository<Guid, ProductImage>, IProductImageRepository
	{
		#region Constructors

		public ProductImageRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}