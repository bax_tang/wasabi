﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IActivityLogRepository : IKeyedEntityRepository<long, ActivityLogRecord> { }

	public class ActivityLogRepository : KeyedEntityRepository<long, ActivityLogRecord>, IActivityLogRepository
	{
		#region Constructors

		public ActivityLogRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}