﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IUserRepository : IObjectEntityRepository<Guid, User> { }

	public class UserRepository : ObjectEntityRepository<Guid, User>, IUserRepository
	{
		#region Constructors

		public UserRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}