﻿using System;
using System.Data;
using System.Data.Entity;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	public interface ISqlRepository
	{
		SqlQueryable StoredProcedure(string procedureName);

		SqlQueryable<TResult> StoredProcedure<TResult>(string procedureName);
	}

	public class SqlRepository : BaseRepository, ISqlRepository
	{
		#region Constructors

		public SqlRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion

		#region ISqlRepository implementation

		public SqlQueryable StoredProcedure(string procedureName)
		{
			return new SqlQueryable(Context, procedureName, CommandType.StoredProcedure);
		}

		public SqlQueryable<TResult> StoredProcedure<TResult>(string procedureName)
		{
			return new SqlQueryable<TResult>(Context, procedureName, CommandType.StoredProcedure);
		}
		#endregion
	}
}