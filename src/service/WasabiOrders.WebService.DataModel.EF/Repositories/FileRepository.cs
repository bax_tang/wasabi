﻿using System;
using System.Data;
using System.Data.Entity;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IFileRepository : IKeyedEntityRepository<Guid, File> { }

	public class FileRepository : KeyedEntityRepository<Guid, File>, IFileRepository
	{
		#region Constructors

		public FileRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}