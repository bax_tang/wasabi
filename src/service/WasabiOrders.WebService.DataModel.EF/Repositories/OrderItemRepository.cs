﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IOrderItemRepository : IObjectEntityRepository<Guid, OrderItem> { }

	public class OrderItemRepository : ObjectEntityRepository<Guid, OrderItem>, IOrderItemRepository
	{
		#region Constructors

		public OrderItemRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}