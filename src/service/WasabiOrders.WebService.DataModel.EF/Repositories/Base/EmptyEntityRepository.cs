﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IEmptyEntityRepository<TEntity> where TEntity : EmptyEntity
	{
		TEntity Add(TEntity entity);

		IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities);

		void Remove(TEntity entity);

		void RemoveRange(IEnumerable<TEntity> entities);

		int Count();

		int Count(Expression<Func<TEntity, bool>> predicate);

		Task<int> CountAsync();

		Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate);

		IQueryable<TEntity> Query();
	}

	public abstract class EmptyEntityRepository<TEntity> : BaseRepository, IEmptyEntityRepository<TEntity> where TEntity : EmptyEntity
	{
        #region Fields and properties

        protected DbSet<TEntity> EntitySet => Context.Set<TEntity>();
        #endregion

        #region Constructors

        public EmptyEntityRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion

		#region Public class methods

		public TEntity Add(TEntity entity)
		{
			return EntitySet.Add(entity);
		}

		public IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities)
		{
			return EntitySet.AddRange(entities);
		}

		public void Remove(TEntity entity)
		{
			var entry = Context.Entry(entity);
			if (entry.State == EntityState.Detached)
			{
				EntitySet.Attach(entity);
			}
			entry.State = EntityState.Deleted;
		}

		public void RemoveRange(IEnumerable<TEntity> entities)
		{
			EntitySet.RemoveRange(entities);
		}

		public int Count()
		{
			return EntitySet.Count();
		}

		public int Count(Expression<Func<TEntity, bool>> predicate)
		{
			return EntitySet.Count(predicate);
		}

		public Task<int> CountAsync()
		{
			return EntitySet.CountAsync();
		}

		public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
		{
			return EntitySet.CountAsync(predicate);
		}

		public virtual IQueryable<TEntity> Query()
		{
			if (Context.Configuration.AutoDetectChangesEnabled)
			{
				return EntitySet;
			}

			return EntitySet.AsNoTracking();
		}
		#endregion
	}
}