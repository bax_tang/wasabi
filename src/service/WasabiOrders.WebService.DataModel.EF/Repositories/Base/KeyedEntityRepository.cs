﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IKeyedEntityRepository<TKey, TEntity> : IEmptyEntityRepository<TEntity>
		where TEntity : KeyedEntity<TKey>, new()
		where TKey : struct
	{
		TEntity Find(TKey entityID);

		Task<TEntity> FindAsync(TKey entityID);

		bool Exists(TKey entityID);

		Task<bool> ExistsAsync(TKey entityID);

		void Remove(TKey entityID);
	}

	public abstract class KeyedEntityRepository<TKey, TEntity> :
		EmptyEntityRepository<TEntity>,
		IKeyedEntityRepository<TKey, TEntity>
		where TEntity : KeyedEntity<TKey>, new() where TKey : struct
	{
		#region Constructors

		public KeyedEntityRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion

		#region Public class methods

		public TEntity Find(TKey entityID)
		{
			return EntitySet.Find(entityID);
		}

		public Task<TEntity> FindAsync(TKey entityID)
		{
			return EntitySet.FindAsync(entityID);
		}

		public bool Exists(TKey entityID)
		{
			return
				Query()
				.Any(x => x.Id.Equals(entityID));
		}

		public Task<bool> ExistsAsync(TKey entityID)
		{
			return
				Query()
				.AnyAsync(x => x.Id.Equals(entityID));
		}

		public void Remove(TKey entityID)
		{
			TEntity entityToRemove = new TEntity
			{
				Id = entityID
			};
			Context.Entry(entityToRemove).State = EntityState.Deleted;
		}
		#endregion
	}
}