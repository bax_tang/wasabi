﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IObjectEntityRepository<TKey, TEntity> : IKeyedEntityRepository<TKey, TEntity>
		where TEntity : ObjectEntity<TKey>, new()
		where TKey : struct
	{
	}

	public abstract class ObjectEntityRepository<TKey, TEntity> :
		KeyedEntityRepository<TKey, TEntity>,
		IObjectEntityRepository<TKey, TEntity>
		where TEntity : ObjectEntity<TKey>, new() where TKey : struct
	{
		#region Constructors

		public ObjectEntityRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}