﻿using System;
using System.Data;
using System.Data.Entity;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	public abstract class BaseRepository : IDisposable
	{
        #region Fields and properties

        private readonly IDbContextScope _contextScope;

        protected DbContext Context => _contextScope.GetDbContext();
		#endregion

		#region Constructors

		protected internal BaseRepository(IDbContextScope contextScope)
		{
			if (contextScope == null)
			{
				throw new ArgumentNullException(nameof(contextScope), "Database context scope can't be null.");
			}

            _contextScope = contextScope;
		}
		#endregion

		#region Class methods

		public virtual void Dispose()
		{
            _contextScope?.Dispose();
		}
		#endregion
	}
}