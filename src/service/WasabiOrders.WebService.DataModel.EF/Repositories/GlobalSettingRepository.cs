﻿using System;
using System.Data;
using System.Data.Entity;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IGlobalSettingRepository : IObjectEntityRepository<long, GlobalSetting> { }

	public class GlobalSettingRepository : ObjectEntityRepository<long, GlobalSetting>, IGlobalSettingRepository
	{
		#region Constructors

		public GlobalSettingRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}