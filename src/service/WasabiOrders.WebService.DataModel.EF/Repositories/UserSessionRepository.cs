﻿using System;
using System.Data.Entity;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IUserSessionRepository : IKeyedEntityRepository<Guid, UserSession> { }

	public class UserSessionRepository : KeyedEntityRepository<Guid, UserSession>, IUserSessionRepository
	{
		#region Constructors

		public UserSessionRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}