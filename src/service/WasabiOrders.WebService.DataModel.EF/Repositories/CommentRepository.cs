﻿using System;
using System.Data;
using System.Data.Entity;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface ICommentRepository : IObjectEntityRepository<Guid, Comment> { }

	public class CommentRepository : ObjectEntityRepository<Guid, Comment>, ICommentRepository
	{
		#region Constructors

		public CommentRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}