﻿using System;
using System.Data.Entity;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IOrderImageRepository : IObjectEntityRepository<Guid, OrderImage> { }

	public class OrderImageRepository : ObjectEntityRepository<Guid, OrderImage>, IOrderImageRepository
	{
		#region Constructors

		public OrderImageRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}