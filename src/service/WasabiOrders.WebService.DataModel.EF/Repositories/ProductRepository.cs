﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace WasabiOrders.WebService.DataModel.Repositories
{
	using Entities;

	public interface IProductRepository : IObjectEntityRepository<Guid, Product> { }

	public class ProductRepository : ObjectEntityRepository<Guid, Product>, IProductRepository
	{
		#region Constructors

		public ProductRepository(IDbContextScope contextScope) : base(contextScope) { }
		#endregion
	}
}