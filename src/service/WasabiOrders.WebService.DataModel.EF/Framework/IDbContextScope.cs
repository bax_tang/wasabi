﻿using System;
using System.Data.Entity;

namespace WasabiOrders.WebService.DataModel
{
    public interface IDbContextScope : IDisposable
    {

        DbContext GetDbContext();
    }
}