﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;

namespace WasabiOrders.WebService.DataModel
{
	using Mappings;

	public static class WasabiDbSchema
	{
		#region Public class methods

		public static IEnumerable<IEntityMapper> GetEntityMappers()
		{
			return new IEntityMapper[]
			{
				new ActivityTypeMapper(),
				new ActivityLogRecordMapper(),
				new CommentMapper(),
				new FileMapper(),
				new GlobalSettingMapper(),
				new ProductCategoryMapper(),
				new ProductMapper(),
				new ProductImageMapper(),
				new OrderMapper(),
                new OrderImageMapper(),
				new OrderItemMapper(),
				new UserMapper(),
				new UserSessionMapper()
			};
		}
		#endregion
	}
}