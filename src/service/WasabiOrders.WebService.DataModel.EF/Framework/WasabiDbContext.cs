﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DataModel
{
	using Entities;
	using Mappings;

	public class WasabiDbContext : DbContext
    {
		#region Constructors

		public WasabiDbContext() : base() { }

		public WasabiDbContext(string connectionString) : base(connectionString) { }
		#endregion

		#region DbContext methods overriding

		public override int SaveChanges()
		{
			SetDateTimeForAllTrackedEntities();

			return base.SaveChanges();
		}

		public override Task<int> SaveChangesAsync()
		{
			SetDateTimeForAllTrackedEntities();

			return base.SaveChangesAsync();
		}

		public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
		{
			SetDateTimeForAllTrackedEntities();

			return base.SaveChangesAsync(cancellationToken);
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
			ApplyAllMappings(modelBuilder);
        }
		#endregion

		#region Private class methods

		private static void ApplyAllMappings(DbModelBuilder modelBuilder)
		{
			var mappers = WasabiDbSchema.GetEntityMappers();
			foreach (var mapper in mappers)
			{
				mapper.MapEntity(modelBuilder);
			}
		}

		private static bool CheckTypeIsMapper(Type type)
		{
			return !type.IsAbstract && typeof(IEntityMapper).IsAssignableFrom(type);
		}

		private void SetDateTimeForAllTrackedEntities()
		{
			DateTime currentDateTime = DateTime.UtcNow;

			var objectEntityEntries = ChangeTracker.Entries<IObjectEntity>();
			foreach (var objectEntry in objectEntityEntries)
			{
				switch (objectEntry.State)
				{
					case EntityState.Added:
						objectEntry.Entity.CreatedAt = currentDateTime;
						break;
					default: continue;
				}
			}
		}
		#endregion
	}
}