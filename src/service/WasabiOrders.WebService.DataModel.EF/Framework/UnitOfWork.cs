﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DataModel
{
    using Exceptions;

    public class UnitOfWork : IUnitOfWork
    {
        #region Fields and properties

        private readonly IDbContextScope _contextScope;

        protected DbContext Context => _contextScope.GetDbContext();
        #endregion

        #region Constructors

        public UnitOfWork(IDbContextScope contextScope)
        {
            _contextScope = contextScope;
        }
        #endregion

        #region IUnitOfWork implementation

        public Task<TResult> ExecuteAsync<TParameter, TResult>(Func<TParameter, TResult> commands, TParameter parameter,
            IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return InternalExecuteAsync(commands, parameter, isolationLevel, true);
        }

        public Task<TResult> ExecuteAsync<TResult>(Func<Task<TResult>> commands, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return InternalExecuteAsync(commands, isolationLevel, true);
        }

        public Task<TResult> ExecuteAsync<TParameter, TResult>(Func<TParameter, Task<TResult>> commands, TParameter parameter,
            IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return InternalExecuteAsync(commands, parameter, isolationLevel, true);
        }

        public Task<TResult> ReturnAsync<TResult>(Func<Task<TResult>> commands, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return InternalExecuteAsync(commands, isolationLevel, false);
        }

        public Task<TResult> ReturnAsync<TParameter, TResult>(Func<TParameter, Task<TResult>> commands, TParameter parameter,
            IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return InternalExecuteAsync(commands, parameter, isolationLevel, false);
        }
        #endregion

        #region Private class methods

        private async Task<TResult> InternalExecuteAsync<TResult>(Func<Task<TResult>> commands, IsolationLevel isolationLevel, bool withChanges)
        {
            TResult result = default(TResult);

            DbContext context = _contextScope.GetDbContext();

			DbContextTransaction transaction = null;
			try
			{
				using (transaction = CreateTransaction(context, isolationLevel, withChanges))
				{
					result = await commands.Invoke();
					if (withChanges)
					{
						await context.SaveChangesAsync();
					}
					transaction.Commit();
				}
			}
			catch
			{
				transaction?.Rollback();
				throw;
			}

			return result;
        }

        private async Task<TResult> InternalExecuteAsync<TParameter, TResult>(Func<TParameter, TResult> commands, TParameter parameter,
            IsolationLevel isolationLevel, bool withChanges)
        {
            TResult result = default(TResult);

            DbContext context = _contextScope.GetDbContext();

			DbContextTransaction transaction = null;
			try
			{
				using (transaction = CreateTransaction(context, isolationLevel, withChanges))
				{
					result = commands.Invoke(parameter);
					if (withChanges)
					{
						await context.SaveChangesAsync();
					}
					transaction.Commit();
				}
			}
			catch
			{
				transaction?.Rollback();
				throw;
			}

			return result;
        }

        private async Task InternalExecuteAsync<TParameter, TResult>(Func<TParameter, Task> commands, TParameter parameter,
            IsolationLevel isolationLevel, bool withChanges)
        {
            DbContext context = _contextScope.GetDbContext();

			DbContextTransaction transaction = null;
			try
			{
				using (transaction = CreateTransaction(context, isolationLevel, withChanges))
				{
					await commands.Invoke(parameter);
					if (withChanges)
					{
						await context.SaveChangesAsync();
					}
					transaction.Commit();
				}
			}
			catch
			{
				transaction?.Rollback();
				throw;
			}
		}

        private async Task<TResult> InternalExecuteAsync<TParameter, TResult>(Func<TParameter, Task<TResult>> commands, TParameter parameter,
            IsolationLevel isolationLevel, bool withChanges)
        {
            TResult result = default(TResult);

			DbContext context = _contextScope.GetDbContext();

			DbContextTransaction transaction = null;
			try
			{
				using (transaction = CreateTransaction(context, isolationLevel, withChanges))
				{
					result = await commands.Invoke(parameter);
					if (withChanges)
					{
						await context.SaveChangesAsync();
					}
					transaction.Commit();
				}
			}
			catch
			{
				transaction?.Rollback();
				throw;
			}

			return result;
        }

		private DbContextTransaction CreateTransaction(DbContext context, IsolationLevel isolationLevel, bool withChanges)
		{
			if (context.Database.CurrentTransaction != null)
			{
				throw new InternalErrorException("Database context has already been initialized.");
			}

			context.Configuration.AutoDetectChangesEnabled = withChanges;

			return context.Database.BeginTransaction(isolationLevel);
		}
        #endregion
    }
}