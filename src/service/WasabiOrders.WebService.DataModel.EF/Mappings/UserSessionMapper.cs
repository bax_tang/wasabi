﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class UserSessionMapper : KeyedEntityMapper<Guid, UserSession>
	{
		#region Constructors

		public UserSessionMapper() : base("UserSessions", "SessionID", DatabaseGeneratedOption.None) { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<UserSession> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.UserID)
                .HasIndex(clustered: false, unique: false, name: "usersessions_idx_userid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.LoginDateTime)
                .HasColumnOrder(columnOrder++)
                .IsRequired();

			entityConfig.HasRequired(x => x.User).WithMany().HasForeignKey(x => x.UserID).WillCascadeOnDelete(false);
		}
		#endregion
	}
}