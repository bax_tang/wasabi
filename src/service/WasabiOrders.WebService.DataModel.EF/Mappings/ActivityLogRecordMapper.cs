﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class ActivityLogRecordMapper : KeyedEntityMapper<long, ActivityLogRecord>
	{
		#region Constructors

		public ActivityLogRecordMapper() : base("ActivityLogs", "RecordID") { }
		#endregion

		#region EntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<ActivityLogRecord> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

			entityConfig.Property(x => x.TypeID)
                .HasIndex(clustered: false, unique: false, name: "activitylogs_idx_typeid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.UserID)
                .HasIndex(clustered: false, unique: false, name: "activitylogs_idx_userid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.ObjectID)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
            entityConfig.Property(x => x.ObjectName).HasMaxLength(2048)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.ActivityDateTime)
                .HasColumnOrder(columnOrder++)
                .IsRequired();

			entityConfig.HasRequired(x => x.Type).WithMany().HasForeignKey(x => x.TypeID).WillCascadeOnDelete(false);
			entityConfig.HasRequired(x => x.User).WithMany().HasForeignKey(x => x.UserID).WillCascadeOnDelete(false);
		}
		#endregion
	}
}