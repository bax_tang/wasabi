﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
    using Entities;

    public class OrderImageMapper : ObjectEntityMapper<Guid, OrderImage>
    {
        #region Constructors

        public OrderImageMapper() : base("OrderImages", "ImageID") { }
        #endregion

        #region EmptyEntityMapper methods overriding

        protected override void MapEntity(EntityTypeConfiguration<OrderImage> entityConfig)
        {
            base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.FileID)
                .HasIndex(clustered: false, unique: false, name: "orderimages_idx_fileid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
            entityConfig.Property(x => x.OrderID)
                .HasIndex(clustered: false, unique: false, name: "orderimages_idx_orderid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();

            entityConfig.HasRequired(x => x.ImageFile).WithMany().HasForeignKey(x => x.FileID).WillCascadeOnDelete(true);
            entityConfig.HasRequired(x => x.ParentOrder).WithMany().HasForeignKey(x => x.OrderID).WillCascadeOnDelete(true);
        }
        #endregion
    }
}