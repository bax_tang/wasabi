﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class GlobalSettingMapper : UpdatableEntityMapper<long, GlobalSetting>
	{
		#region Constructors

		public GlobalSettingMapper() : base("GlobalSettings", "SettingID") { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<GlobalSetting> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.Name).HasMaxLength(64)
                .HasIndex(clustered: false, unique: true, name: "globalsettings_idx_name")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Value).HasMaxLength(2048)
                .HasColumnOrder(columnOrder++)
                .IsRequired();
		}
		#endregion
	}
}