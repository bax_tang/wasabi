﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class ProductImageMapper : ObjectEntityMapper<Guid, ProductImage>
	{
		#region Constructors

		public ProductImageMapper() : base("ProductImages", "ImageID") { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<ProductImage> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.FileID)
                .HasIndex(clustered: false, unique: false, name: "productimages_idx_fileid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
            entityConfig.Property(x => x.ProductID)
                .HasIndex(clustered: false, unique: false, name: "productimages_idx_productid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Type)
                .HasColumnOrder(columnOrder++)
                .IsRequired();

            entityConfig.HasRequired(x => x.ImageFile).WithMany().HasForeignKey(x => x.FileID).WillCascadeOnDelete(true);
            entityConfig.HasRequired(x => x.ParentProduct).WithMany().HasForeignKey(x => x.ProductID).WillCascadeOnDelete(true);
		}
		#endregion
	}
}