﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class OrderItemMapper : ObjectEntityMapper<Guid, OrderItem>
	{
		#region Constructors

		public OrderItemMapper() : base("OrderItems", "ItemID") { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<OrderItem> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.OrderID)
                .HasIndex(clustered: false, unique: false, name: "orderitems_idx_orderid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.ProductID)
                .HasIndex(clustered: false, unique: false, name: "orderitems_idx_productid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
            entityConfig.Property(x => x.Amount).IsRequired();

            entityConfig.HasRequired(x => x.ParentOrder).WithMany().HasForeignKey(x => x.OrderID).WillCascadeOnDelete(true);
			entityConfig.HasRequired(x => x.RelatedProduct).WithMany().HasForeignKey(x => x.ProductID).WillCascadeOnDelete(false);
		}
		#endregion
	}
}