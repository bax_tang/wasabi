﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq.Expressions;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class ProductMapper : UpdatableEntityMapper<Guid, Product>
	{
		#region Constructors

		public ProductMapper() : base("Products", "ProductID") { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<Product> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.CategoryID)
                .HasIndex(clustered: false, unique: false, name: "products_idx_categoryid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Name).IsUnicode(true).HasMaxLength(64)
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Description).IsUnicode(true).HasMaxLength(1024)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.Price).HasColumnType("money")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Weight).HasColumnType("float")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Rating)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.ExternalFolderID).HasMaxLength(16)
                .HasColumnOrder(columnOrder++)
                .IsOptional();

			entityConfig.HasRequired(x => x.Category).WithMany().HasForeignKey(x => x.CategoryID).WillCascadeOnDelete(false);

			entityConfig.HasMany(x => x.Images).WithRequired().HasForeignKey(x => x.ProductID).WillCascadeOnDelete(true);
		}
		#endregion
	}
}