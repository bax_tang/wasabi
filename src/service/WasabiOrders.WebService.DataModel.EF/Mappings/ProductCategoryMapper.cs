﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Linq.Expressions;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class ProductCategoryMapper : UpdatableEntityMapper<Guid, ProductCategory>
	{
		#region Constructors

		public ProductCategoryMapper() : base("ProductCategories", "CategoryID") { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<ProductCategory> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.Name).IsUnicode(true)
                .HasMaxLength(64)
                .HasIndex(clustered: false, unique: true, name: "productcategories_idx_name")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Description).IsUnicode(true).HasMaxLength(int.MaxValue)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.ExternalFolderID).HasMaxLength(16)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
		}
		#endregion
	}
}