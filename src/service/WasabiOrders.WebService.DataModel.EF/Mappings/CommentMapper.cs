﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class CommentMapper : UpdatableEntityMapper<Guid, Comment>
	{
		#region Constructors

		public CommentMapper() : base("Comments", "CommentID") { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<Comment> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.RepliedTo)
                .HasIndex(clustered: false, unique: false, name: "comments_idx_repliedto")
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.ObjectID)
                .HasIndex(clustered: false, unique: false, name: "comments_idx_objectid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.AuthorID)
                .HasIndex(clustered: false, unique: false, name: "comments_idx_authorid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Title).IsUnicode(true).HasMaxLength(256)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.Text).IsUnicode(true).HasMaxLength(2048)
                .HasColumnOrder(columnOrder++)
                .IsOptional();

			entityConfig.HasRequired(x => x.Author).WithMany().HasForeignKey(x => x.AuthorID).WillCascadeOnDelete(false);
			entityConfig.HasOptional(x => x.ParentComment).WithMany().HasForeignKey(x => x.RepliedTo).WillCascadeOnDelete(false);
		}
		#endregion
	}
}