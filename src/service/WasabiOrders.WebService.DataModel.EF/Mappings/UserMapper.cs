﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class UserMapper : UpdatableEntityMapper<Guid, User>
	{
		#region Constructors

		public UserMapper() : base("Users", "UserID") { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<User> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.AccountName).IsUnicode(true).HasMaxLength(64)
                .HasIndex(clustered: false, unique: true, name: "users_idx_accountname")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.FirstName).IsUnicode(true).HasMaxLength(64)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.LastName).IsUnicode(true).HasMaxLength(64)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.Email).IsUnicode(true).HasMaxLength(128)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.PasswordHash).IsUnicode(true).HasMaxLength(2048)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.Discount)
                .HasColumnOrder(columnOrder++)
                .IsRequired();
		}
		#endregion
	}
}