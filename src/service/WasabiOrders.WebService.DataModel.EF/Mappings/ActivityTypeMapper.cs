﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class ActivityTypeMapper : KeyedEntityMapper<long, ActivityType>
	{
		#region Constructors

		public ActivityTypeMapper() : base("ActivityTypes", "TypeID") { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<ActivityType> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

			entityConfig.Property(x => x.Name)
                .HasMaxLength(64)
                .HasIndex(clustered: false, unique: true, name: "activitytypes_idx_name")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Text).HasMaxLength(1024)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
		}
		#endregion
	}
}