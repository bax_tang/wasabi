﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public abstract class ObjectEntityMapper<TKey, TEntity> : KeyedEntityMapper<TKey, TEntity> where TEntity : ObjectEntity<TKey> where TKey : struct
	{
		#region Constructors

		protected internal ObjectEntityMapper(string tableName, string keyColumnName,
			DatabaseGeneratedOption keyGeneratedOption = DatabaseGeneratedOption.Identity) : base(tableName, keyColumnName, keyGeneratedOption) { }
		#endregion

		#region EntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<TEntity> entityConfig)
		{
			base.MapEntity(entityConfig);

			entityConfig.Property(x => x.CreatedAt)
                .HasColumnOrder(Constants.CreatedAtColumnOrder)
                .IsOptional();
		}
		#endregion
	}
}