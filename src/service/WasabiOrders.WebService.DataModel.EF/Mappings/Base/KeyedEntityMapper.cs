﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public abstract class KeyedEntityMapper<TKey, TEntity> : EmptyEntityMapper<TEntity> where TEntity : KeyedEntity<TKey> where TKey : struct
	{
		#region Fields and properties

		protected readonly string _keyColumnName;

		private readonly DatabaseGeneratedOption _keyGeneratedOption;
		#endregion

		#region Constructors

		protected internal KeyedEntityMapper(string tableName, string keyColumnName,
			DatabaseGeneratedOption keyGeneratedOption = DatabaseGeneratedOption.Identity) : base(tableName)
		{
			if (string.IsNullOrEmpty(keyColumnName))
			{
				throw new ArgumentNullException(nameof(keyColumnName), "Key column name can't be null or empty.");
			}

			_keyColumnName = keyColumnName;
			_keyGeneratedOption = keyGeneratedOption;
		}
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<TEntity> entityConfig)
		{
			base.MapEntity(entityConfig);
			
			entityConfig.HasKey(x => x.Id, ConfigureEntityKey)
				.Property(x => x.Id)
				.HasColumnName(_keyColumnName)
				.HasColumnOrder(Constants.IdColumnOrder)
				.HasDatabaseGeneratedOption(_keyGeneratedOption)
				.IsRequired();
		}
		#endregion

		#region Private class methods

		private void ConfigureEntityKey(PrimaryKeyIndexConfiguration primaryKeyConfig)
		{
			string primaryKeyName = string.Format(
                "{0}_pk_{1}",
				_tableName.ToLowerInvariant(),
				_keyColumnName.ToLowerInvariant());

			primaryKeyConfig.IsClustered(false);
			primaryKeyConfig.HasName(primaryKeyName);
		}
		#endregion
	}
}