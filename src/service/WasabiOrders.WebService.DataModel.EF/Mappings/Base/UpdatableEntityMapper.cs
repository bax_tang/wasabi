﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public abstract class UpdatableEntityMapper<TKey, TEntity> : ObjectEntityMapper<TKey, TEntity> where TEntity : UpdatableEntity<TKey> where TKey : struct
	{
		#region Constructors

		protected internal UpdatableEntityMapper(string tableName, string keyColumnName) : base(tableName, keyColumnName) { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<TEntity> entityConfig)
		{
			base.MapEntity(entityConfig);

			entityConfig.Property(x => x.UpdatedAt)
				.HasColumnOrder(Constants.UpdatedAtColumnOrder)
				.IsOptional();
		}
		#endregion
	}
}