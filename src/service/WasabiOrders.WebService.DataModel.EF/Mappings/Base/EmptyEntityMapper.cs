﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public interface IEntityMapper
	{
		void MapEntity(DbModelBuilder modelBuilder);
	}

	public abstract class EmptyEntityMapper<TEntity> : IEntityMapper where TEntity : EmptyEntity
	{
		#region Constants and fields

		private const string SchemaName = "dbo";

		protected readonly string _tableName;
		#endregion

		#region Constructors

		protected internal EmptyEntityMapper(string tableName)
		{
			if (string.IsNullOrEmpty(tableName))
			{
				throw new ArgumentNullException(nameof(tableName), "Table name can't be null or empty.");
			}

			_tableName = tableName;
		}
		#endregion

		#region Class methods

		public void MapEntity(DbModelBuilder modelBuilder)
		{
			EntityTypeConfiguration<TEntity> entityConfig = modelBuilder.Entity<TEntity>();

			MapEntity(entityConfig);
		}

		protected virtual void MapEntity(EntityTypeConfiguration<TEntity> entityConfig)
		{
			entityConfig.ToTable(_tableName, SchemaName);
		}
		#endregion
	}
}