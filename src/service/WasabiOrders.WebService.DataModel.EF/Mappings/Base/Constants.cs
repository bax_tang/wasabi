﻿using System;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	internal static class Constants
	{
		public const int IdColumnOrder = 0;
        public const int ColumnsStartOrder = 100;
        public const int CreatedAtColumnOrder = 1000;
		public const int UpdatedAtColumnOrder = 1001;
	}
}