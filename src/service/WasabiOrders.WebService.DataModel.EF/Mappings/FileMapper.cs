﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class FileMapper : ObjectEntityMapper<Guid, File>
	{
		#region Constructors

		public FileMapper() : base("Files", "FileID", DatabaseGeneratedOption.None) { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<File> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.Name).IsUnicode(true).HasMaxLength(64)
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Type).HasMaxLength(24)
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Size)
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.ExternalFileID)
                .HasMaxLength(16)
                .HasIndex(clustered: false, unique: true, name: "files_idx_externalfileid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.ExternalFileUrl).IsUnicode(true).HasMaxLength(1024)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
		}
		#endregion
	}
}