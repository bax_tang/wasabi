﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace WasabiOrders.WebService.DataModel.Mappings
{
	using Entities;

	public class OrderMapper : UpdatableEntityMapper<Guid, Order>
	{
		#region Constructors

		public OrderMapper() : base("Orders", "OrderID") { }
		#endregion

		#region EmptyEntityMapper methods overriding

		protected override void MapEntity(EntityTypeConfiguration<Order> entityConfig)
		{
			base.MapEntity(entityConfig);

            int columnOrder = Constants.ColumnsStartOrder;

            entityConfig.Property(x => x.AuthorID)
                .HasIndex(clustered: false, unique: false, name: "orders_idx_authorid")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.Name).IsUnicode(true).HasMaxLength(64)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.Status).HasColumnType("int")
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.OrderDateTime)
                .HasColumnOrder(columnOrder++)
                .IsRequired();
			entityConfig.Property(x => x.ExpectedDeliveryDateTime)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.ActualDeliveryDateTime)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.TotalPrice)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.DiscountedPrice)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.Rating)
                .HasColumnOrder(columnOrder++)
                .IsOptional();
			entityConfig.Property(x => x.ExternalFolderID).HasMaxLength(16)
                .HasColumnOrder(columnOrder++)
                .IsOptional();

			entityConfig.HasRequired(x => x.Author).WithMany().HasForeignKey(x => x.AuthorID).WillCascadeOnDelete(false);

			entityConfig.HasMany(x => x.Images).WithRequired().HasForeignKey(x => x.OrderID).WillCascadeOnDelete(true);
			entityConfig.HasMany(x => x.Items).WithRequired().HasForeignKey(x => x.OrderID).WillCascadeOnDelete(true);
		}
		#endregion
	}
}