﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Design;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Migrations.Utilities;

namespace WasabiOrders.WebService.DataModel.Migrations
{
    public class WasabiMigrationCodeGenerator : CSharpMigrationCodeGenerator
    {
        #region Constructors

        public WasabiMigrationCodeGenerator() : base() { }
        #endregion

        #region CSharpMigrationCodeGenerator methods overriding
        
        protected override void Generate(AddForeignKeyOperation addForeignKeyOperation, IndentedTextWriter writer)
        {
            if (addForeignKeyOperation == null)
            {
                throw new ArgumentNullException(nameof(addForeignKeyOperation));
            }
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            BuildForeignKeyName(addForeignKeyOperation);
            
            base.Generate(addForeignKeyOperation, writer);
        }

        protected override void GenerateInline(AddForeignKeyOperation addForeignKeyOperation, IndentedTextWriter writer)
        {
            if (addForeignKeyOperation == null)
            {
                throw new ArgumentNullException(nameof(addForeignKeyOperation));
            }
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            BuildForeignKeyName(addForeignKeyOperation);

            writer.WriteLine();
            writer.Write(".ForeignKey(" + Quote(addForeignKeyOperation.PrincipalTable) + ", ");
            Generate(addForeignKeyOperation.DependentColumns, writer);
            if (addForeignKeyOperation.CascadeDelete)
            {
                writer.Write(", cascadeDelete: true");
            }
            writer.Write(string.Format(", name: {0}", Quote(addForeignKeyOperation.Name)));
            writer.Write(")");
        }

		protected override void Generate(ColumnModel column, IndentedTextWriter writer, bool emitName = false)
		{
			if (((column.ClrType == typeof(DateTime)) || (column.ClrType == typeof(DateTime?))) &&
				string.Equals(column.Name, "CreatedAt", StringComparison.OrdinalIgnoreCase))
			{
				column.DefaultValueSql = "GETUTCDATE()";
			}

			base.Generate(column, writer, emitName);
		}
		#endregion

		#region Private class methods

		private void BuildForeignKeyName(AddForeignKeyOperation operation)
        {
            string dependentTable = operation.DependentTable;

            int dotIndex = dependentTable.IndexOf('.');
            if (dotIndex != -1)
            {
                dependentTable = dependentTable.Substring(dotIndex + 1);
            }

            string joinedColumns = string.Join("_", operation.DependentColumns);

            string keyName = string.Format("{0}_fk_{1}", dependentTable, joinedColumns);
            operation.Name = keyName.ToLowerInvariant();
        }
        #endregion
    }
}