﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace WasabiOrders.WebService.DataModel.Migrations
{
	public class WasabiMigrationConfiguration : DbMigrationsConfiguration<WasabiDbContext>
	{
		#region Constructors

		public WasabiMigrationConfiguration()
		{
			AutomaticMigrationsEnabled = false;
            CodeGenerator = new WasabiMigrationCodeGenerator();
			CommandTimeout = 3600;
		}
		#endregion

		#region DbMigrationsConfiguration methods overriding

		protected override void Seed(WasabiDbContext context)
		{
			Database currentDatabase = context.Database;

			DropProgrammability(currentDatabase);
            ApplyProgrammability(currentDatabase);
		}
		#endregion

		#region Private class methods
		
		private void DropProgrammability(Database database)
		{
			DropProgrammability(database, "DROP PROCEDURE [dbo].[{0}]", "'P'");
			DropProgrammability(database, "DROP FUNCTION [dbo].[{0}]", "'FN'", "'IF'", "'TF'");
            DropProgrammability(database, "DROP TRIGGER [dbo].[{0}]", "'TR'");
		}

		private void DropProgrammability(Database database, string dropObjectFormat, params string[] objectTypes)
		{
			DbContextTransaction transaction = null;
			try
			{
				using (transaction = database.BeginTransaction())
				{
					string objectTypesJoined = string.Join(", ", objectTypes);
					string objectNamesQueryText = string.Format("SELECT [name] FROM sys.objects WHERE [type] IN ({0})", objectTypesJoined);
					var objectNamesQuery = database.SqlQuery<string>(objectNamesQueryText);
					var objectNames = objectNamesQuery.ToList();
					
					foreach (string objectName in objectNames)
					{
						string command = string.Format(dropObjectFormat, objectName);
						database.ExecuteSqlCommand(command);
					}

					transaction.Commit();
				}
			}
			catch
			{
				transaction?.Rollback();
				throw;
			}
		}

        private void ApplyProgrammability(Database database)
        {
            Assembly resourceAssembly = typeof(SqlResources).Assembly;
            string[] resourceNames = resourceAssembly.GetManifestResourceNames();

            var groupedResourceNames = resourceNames.ToLookup(ResourceNameKeySelector);
            ApplyProgrammability(database, resourceAssembly, groupedResourceNames["Triggers"]);
            ApplyProgrammability(database, resourceAssembly, groupedResourceNames["Functions"]);
            ApplyProgrammability(database, resourceAssembly, groupedResourceNames["Procedures"]);
        }

        private void ApplyProgrammability(Database database, Assembly resourceAssembly, IEnumerable<string> resourceNames)
        {
            foreach (string resourceName in resourceNames)
            {
                Stream resourceStream = resourceAssembly.GetManifestResourceStream(resourceName);
                using (StreamReader reader = new StreamReader(resourceStream, Encoding.UTF8))
                {
                    string resourceText = reader.ReadToEnd();
                    database.ExecuteSqlCommand(resourceText);
                }
            }
        }

        private static string ResourceNameKeySelector(string resourceName)
        {
            if (resourceName.IndexOf("Functions") != -1)
            {
                return "Functions";
            }
            else if (resourceName.IndexOf("Procedures") != -1)
            {
                return "Procedures";
            }
            else if (resourceName.IndexOf("Triggers") != -1)
            {
                return "Triggers";
            }
            else return "Unknown";
        }
		#endregion
	}
}