namespace WasabiOrders.WebService.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityTypes",
                c => new
                    {
                        TypeID = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Text = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.TypeID, name: "activitytypes_pk_typeid", clustered: false)
                .Index(t => t.Name, unique: true, name: "activitytypes_idx_name");
            
            CreateTable(
                "dbo.ActivityLogs",
                c => new
                    {
                        RecordID = c.Long(nullable: false, identity: true),
                        TypeID = c.Long(nullable: false),
                        UserID = c.Guid(nullable: false),
                        ObjectID = c.Guid(),
                        ObjectName = c.String(maxLength: 2048),
                        ActivityDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.RecordID, name: "activitylogs_pk_recordid", clustered: false)
                .ForeignKey("dbo.ActivityTypes", t => t.TypeID, name: "activitylogs_fk_typeid")
                .ForeignKey("dbo.Users", t => t.UserID, name: "activitylogs_fk_userid")
                .Index(t => t.TypeID, name: "activitylogs_idx_typeid")
                .Index(t => t.UserID, name: "activitylogs_idx_userid");
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Guid(nullable: false, identity: true),
                        AccountName = c.String(nullable: false, maxLength: 64),
                        FirstName = c.String(maxLength: 64),
                        LastName = c.String(maxLength: 64),
                        Email = c.String(maxLength: 128),
                        PasswordHash = c.String(maxLength: 2048),
                        Discount = c.Double(nullable: false),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserID, name: "users_pk_userid", clustered: false)
                .Index(t => t.AccountName, unique: true, name: "users_idx_accountname");
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentID = c.Guid(nullable: false, identity: true),
                        RepliedTo = c.Guid(),
                        ObjectID = c.Guid(nullable: false),
                        AuthorID = c.Guid(nullable: false),
                        Title = c.String(maxLength: 256),
                        Text = c.String(maxLength: 2048),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.CommentID, name: "comments_pk_commentid", clustered: false)
                .ForeignKey("dbo.Users", t => t.AuthorID, name: "comments_fk_authorid")
                .ForeignKey("dbo.Comments", t => t.RepliedTo, name: "comments_fk_repliedto")
                .Index(t => t.RepliedTo, name: "comments_idx_repliedto")
                .Index(t => t.ObjectID, name: "comments_idx_objectid")
                .Index(t => t.AuthorID, name: "comments_idx_authorid");
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        FileID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                        Type = c.String(nullable: false, maxLength: 24),
                        Size = c.Long(nullable: false),
                        ExternalFileID = c.String(nullable: false, maxLength: 16),
                        ExternalFileUrl = c.String(maxLength: 1024),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.FileID, name: "files_pk_fileid", clustered: false)
                .Index(t => t.ExternalFileID, unique: true, name: "files_idx_externalfileid");
            
            CreateTable(
                "dbo.GlobalSettings",
                c => new
                    {
                        SettingID = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Value = c.String(nullable: false, maxLength: 2048),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.SettingID, name: "globalsettings_pk_settingid", clustered: false)
                .Index(t => t.Name, unique: true, name: "globalsettings_idx_name");
            
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        CategoryID = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(),
                        ExternalFolderID = c.String(maxLength: 16),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.CategoryID, name: "productcategories_pk_categoryid", clustered: false)
                .Index(t => t.Name, unique: true, name: "productcategories_idx_name");
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Guid(nullable: false, identity: true),
                        CategoryID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 1024),
                        Price = c.Decimal(nullable: false, storeType: "money"),
                        Weight = c.Double(nullable: false),
                        Rating = c.Int(),
                        ExternalFolderID = c.String(maxLength: 16),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProductID, name: "products_pk_productid", clustered: false)
                .ForeignKey("dbo.ProductCategories", t => t.CategoryID, name: "products_fk_categoryid")
                .Index(t => t.CategoryID, name: "products_idx_categoryid");
            
            CreateTable(
                "dbo.ProductImages",
                c => new
                    {
                        ImageID = c.Guid(nullable: false, identity: true),
                        FileID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.ImageID, name: "productimages_pk_imageid", clustered: false)
                .ForeignKey("dbo.Files", t => t.FileID, cascadeDelete: true, name: "productimages_fk_fileid")
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true, name: "productimages_fk_productid")
                .Index(t => t.FileID, name: "productimages_idx_fileid")
                .Index(t => t.ProductID, name: "productimages_idx_productid");
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Guid(nullable: false, identity: true),
                        AuthorID = c.Guid(nullable: false),
                        Name = c.String(maxLength: 64),
                        Status = c.Int(nullable: false),
                        OrderDateTime = c.DateTime(nullable: false),
                        ExpectedDeliveryDateTime = c.DateTime(),
                        ActualDeliveryDateTime = c.DateTime(),
                        TotalPrice = c.Decimal(precision: 18, scale: 2),
                        DiscountedPrice = c.Decimal(precision: 18, scale: 2),
                        Rating = c.Int(),
                        ExternalFolderID = c.String(maxLength: 16),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.OrderID, name: "orders_pk_orderid", clustered: false)
                .ForeignKey("dbo.Users", t => t.AuthorID, name: "orders_fk_authorid")
                .Index(t => t.AuthorID, name: "orders_idx_authorid");
            
            CreateTable(
                "dbo.OrderImages",
                c => new
                    {
                        ImageID = c.Guid(nullable: false, identity: true),
                        FileID = c.Guid(nullable: false),
                        OrderID = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.ImageID, name: "orderimages_pk_imageid", clustered: false)
                .ForeignKey("dbo.Files", t => t.FileID, cascadeDelete: true, name: "orderimages_fk_fileid")
                .ForeignKey("dbo.Orders", t => t.OrderID, cascadeDelete: true, name: "orderimages_fk_orderid")
                .Index(t => t.FileID, name: "orderimages_idx_fileid")
                .Index(t => t.OrderID, name: "orderimages_idx_orderid");
            
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        ItemID = c.Guid(nullable: false, identity: true),
                        OrderID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ItemID, name: "orderitems_pk_itemid", clustered: false)
                .ForeignKey("dbo.Orders", t => t.OrderID, cascadeDelete: true, name: "orderitems_fk_orderid")
                .ForeignKey("dbo.Products", t => t.ProductID, name: "orderitems_fk_productid")
                .Index(t => t.OrderID, name: "orderitems_idx_orderid")
                .Index(t => t.ProductID, name: "orderitems_idx_productid");
            
            CreateTable(
                "dbo.UserSessions",
                c => new
                    {
                        SessionID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                        LoginDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SessionID, name: "usersessions_pk_sessionid", clustered: false)
                .ForeignKey("dbo.Users", t => t.UserID, name: "usersessions_fk_userid")
                .Index(t => t.UserID, name: "usersessions_idx_userid");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserSessions", "usersessions_fk_userid");
            DropForeignKey("dbo.OrderItems", "orderitems_fk_productid");
            DropForeignKey("dbo.OrderItems", "orderitems_fk_orderid");
            DropForeignKey("dbo.OrderImages", "orderimages_fk_orderid");
            DropForeignKey("dbo.OrderImages", "orderimages_fk_fileid");
            DropForeignKey("dbo.Orders", "orders_fk_authorid");
            DropForeignKey("dbo.ProductImages", "productimages_fk_productid");
            DropForeignKey("dbo.ProductImages", "productimages_fk_fileid");
            DropForeignKey("dbo.Products", "products_fk_categoryid");
            DropForeignKey("dbo.Comments", "comments_fk_repliedto");
            DropForeignKey("dbo.Comments", "comments_fk_authorid");
            DropForeignKey("dbo.ActivityLogs", "activitylogs_fk_userid");
            DropForeignKey("dbo.ActivityLogs", "activitylogs_fk_typeid");
            DropIndex("dbo.UserSessions", "usersessions_idx_userid");
            DropIndex("dbo.OrderItems", "orderitems_idx_productid");
            DropIndex("dbo.OrderItems", "orderitems_idx_orderid");
            DropIndex("dbo.OrderImages", "orderimages_idx_orderid");
            DropIndex("dbo.OrderImages", "orderimages_idx_fileid");
            DropIndex("dbo.Orders", "orders_idx_authorid");
            DropIndex("dbo.ProductImages", "productimages_idx_productid");
            DropIndex("dbo.ProductImages", "productimages_idx_fileid");
            DropIndex("dbo.Products", "products_idx_categoryid");
            DropIndex("dbo.ProductCategories", "productcategories_idx_name");
            DropIndex("dbo.GlobalSettings", "globalsettings_idx_name");
            DropIndex("dbo.Files", "files_idx_externalfileid");
            DropIndex("dbo.Comments", "comments_idx_authorid");
            DropIndex("dbo.Comments", "comments_idx_objectid");
            DropIndex("dbo.Comments", "comments_idx_repliedto");
            DropIndex("dbo.Users", "users_idx_accountname");
            DropIndex("dbo.ActivityLogs", "activitylogs_idx_userid");
            DropIndex("dbo.ActivityLogs", "activitylogs_idx_typeid");
            DropIndex("dbo.ActivityTypes", "activitytypes_idx_name");
            DropTable("dbo.UserSessions");
            DropTable("dbo.OrderItems");
            DropTable("dbo.OrderImages");
            DropTable("dbo.Orders");
            DropTable("dbo.ProductImages");
            DropTable("dbo.Products");
            DropTable("dbo.ProductCategories");
            DropTable("dbo.GlobalSettings");
            DropTable("dbo.Files");
            DropTable("dbo.Comments");
            DropTable("dbo.Users");
            DropTable("dbo.ActivityLogs");
            DropTable("dbo.ActivityTypes");
        }
    }
}
