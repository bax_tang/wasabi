using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Reflection;

namespace WasabiOrders.WebService.DataModel.Migrations
{
    public partial class InitEntities : DbMigration
	{
		private const string DataResourceName = "WasabiOrders.WebService.DataModel.Sql.Data";

		public override void Up()
		{
			Assembly resourceAssembly = typeof(SqlResources).Assembly;

			foreach (string resourceName in resourceAssembly.GetManifestResourceNames().Where(IsDataResource))
			{
				SqlResource(resourceName, resourceAssembly);
			}
		}

		public override void Down()
		{
			Sql("DELETE FROM [dbo].[Users] WITH(ROWLOCK)");
			Sql("DELETE FROM [dbo].[ProductCategories] WITH(ROWLOCK)");
			Sql("DELETE FROM [dbo].[GlobalSettings] WITH(ROWLOCK)");
			Sql("DELETE FROM [dbo].[ActivityTypes] WITH(ROWLOCK)");
		}

		private static bool IsDataResource(string resourceName)
		{
			return resourceName.StartsWith(DataResourceName);
		}
	}
}
