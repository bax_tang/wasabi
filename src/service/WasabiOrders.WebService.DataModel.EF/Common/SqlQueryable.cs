﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DataModel
{
    public abstract class SqlQueryableBase
    {
        #region Fields and properties

        protected readonly DbContext Context;

        private readonly string CommandText;

        private readonly CommandType CommandType;

        protected readonly List<SqlParameter> Parameters;
        #endregion

        #region Constructors

        protected internal SqlQueryableBase(DbContext context, string commandText, CommandType commandType)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context), "Database context can't be null.");
            }
            if (string.IsNullOrEmpty(commandText))
            {
                throw new ArgumentNullException(nameof(commandText), "Command text can't be null or empty.");
            }

            Context = context;
            CommandText = commandText;
            CommandType = commandType;
            Parameters = new List<SqlParameter>(4);
        }
        #endregion

        #region Class methods

        protected string BuildFullCommandText()
        {
            StringBuilder builder = new StringBuilder(64);

            switch (CommandType)
            {
                case CommandType.StoredProcedure:
                    builder.Append("EXEC ");

                    builder.Append(CommandText);

                    var parameters = Parameters;
                    if (parameters.Count > 0)
                    {
                        builder.Append(' ');

                        var parametersNames = parameters.Select(SqlParameterToName).ToArray();
                        builder.Append(string.Join(", ", parametersNames));
                    }
                    break;
                case CommandType.Text:
                    builder.Append(CommandText);
                    break;
                default: break;
            }

            return builder.ToString();
        }

        protected static string FixParameterName(string parameterName)
        {
            if (string.IsNullOrEmpty(parameterName))
            {
                throw new ArgumentNullException(nameof(parameterName), "Parameter name can't be null or empty.");
            }

            if (parameterName[0] != '@')
            {
                return string.Concat("@", parameterName);
            }

            return parameterName;
        }

        private static string SqlParameterToName(SqlParameter parameter)
        {
            return (parameter != null) ? parameter.ParameterName : string.Empty;
        }
        #endregion
    }

    public class SqlQueryable : SqlQueryableBase
    {
        #region Constructors

        internal SqlQueryable(DbContext context, string commandText, CommandType commandType) : base(context, commandText, commandType)
        {

        }
        #endregion

        #region Public class methods

        public SqlQueryable AddParameter<T>(string parameterName, T? value) where T : struct
        {
            parameterName = FixParameterName(parameterName);

            if (value.HasValue)
            {
                Parameters.Add(new SqlParameter(parameterName, value));
            }
            else
            {
                Parameters.Add(new SqlParameter(parameterName, DBNull.Value));
            }

            return this;
        }

        public SqlQueryable AddParameter<T>(string parameterName, T value)
        {
            parameterName = FixParameterName(parameterName);

            if (value != null)
            {
                Parameters.Add(new SqlParameter(parameterName, value));
            }
            else
            {
                Parameters.Add(new SqlParameter(parameterName, DBNull.Value));
            }

            return this;
        }

        public int Execute()
        {
            string commandText = BuildFullCommandText();
            object[] parameters = Parameters.ToArray();

            return Context.Database.ExecuteSqlCommand(commandText, parameters);
        }

        public Task<int> ExecuteAsync()
        {
            string commandText = BuildFullCommandText();
            object[] parameters = Parameters.ToArray();

            return Context.Database.ExecuteSqlCommandAsync(commandText, parameters);
        }
        #endregion
    }

    public class SqlQueryable<TResult> : SqlQueryableBase
    {
        #region Constructors

        internal SqlQueryable(DbContext context, string commandText, CommandType commandType) : base(context, commandText, commandType)
        {

        }
        #endregion

        #region Public class methods

        public SqlQueryable<TResult> AddParameter<T>(string parameterName, T? value) where T : struct
        {
            parameterName = FixParameterName(parameterName);

            if (value.HasValue)
            {
                Parameters.Add(new SqlParameter(parameterName, value));
            }
            else
            {
                Parameters.Add(new SqlParameter(parameterName, DBNull.Value));
            }

            return this;
        }

        public SqlQueryable<TResult> AddParameter<T>(string parameterName, T value)
        {
            parameterName = FixParameterName(parameterName);

            if (value != null)
            {
                Parameters.Add(new SqlParameter(parameterName, value));
            }
            else
            {
                Parameters.Add(new SqlParameter(parameterName, DBNull.Value));
            }

            return this;
        }

        public DbRawSqlQuery<TResult> Execute()
        {
            string commandText = BuildFullCommandText();
            object[] parameters = Parameters.ToArray();

            return Context.Database.SqlQuery<TResult>(commandText, parameters);
        }
        #endregion
    }
}