﻿using System;

namespace WasabiOrders.WebService.DataModel
{
	public static class SqlProcedureNames
	{
		#region Constants

		private const string CreateActivityLogRecordProcedureName = "[dbo].[sp_activitylogs_create]";

		private const string GetAllCategoriesProcedureName = "[dbo].[sp_categories_get_all]";

		private const string GetCommentsProcedureName = "[dbo].[sp_comments_get_with_hierarchy]";
		#endregion

		#region Properties

		public static string CreateActivityLogRecord => CreateActivityLogRecordProcedureName;

		public static string GetAllCategories => GetAllCategoriesProcedureName;

		public static string GetComments => GetCommentsProcedureName;
		#endregion
	}
}