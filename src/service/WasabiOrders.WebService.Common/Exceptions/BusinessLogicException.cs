﻿using System;
using System.Net;

namespace WasabiOrders.WebService.Exceptions
{
	public class BusinessLogicException : HttpWebException
	{
		#region Properties

		public override HttpStatusCode StatusCode => HttpStatusCode.MethodNotAllowed;
		#endregion

		#region Constructors

		public BusinessLogicException(string message) : base(message) { }

		public BusinessLogicException(string message, Exception innerException) : base(message, innerException) { }
		#endregion
	}
}