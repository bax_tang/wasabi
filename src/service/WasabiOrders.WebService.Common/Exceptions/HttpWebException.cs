﻿using System;
using System.Net;

namespace WasabiOrders.WebService.Exceptions
{
	public abstract class HttpWebException : WebException
	{
		#region Properties

		public abstract HttpStatusCode StatusCode { get; }
		#endregion

		#region Constructors

		public HttpWebException(string message) : base(message) { }

		public HttpWebException(string message, Exception innerException) : base(message, innerException) { }
		#endregion
	}
}