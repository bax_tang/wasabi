﻿using System;
using System.Net;

namespace WasabiOrders.WebService.Exceptions
{
	public class BadRequestException : HttpWebException
	{
		#region Properties

		public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
		#endregion

		#region Constructors

		public BadRequestException(string message) : base(message) { }

		public BadRequestException(string message, Exception innerException) : base(message, innerException) { }
		#endregion
	}
}