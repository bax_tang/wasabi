﻿using System;
using System.Net;

namespace WasabiOrders.WebService.Exceptions
{
    public class InternalErrorException : HttpWebException
    {
        #region Properties

        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;
        #endregion

        #region Constructors

        public InternalErrorException(string message) : base(message) { }

        public InternalErrorException(string message, Exception innerException) : base(message, innerException) { }
        #endregion
    }
}