﻿using System;
using System.Net;

namespace WasabiOrders.WebService.Exceptions
{
	public class NotFoundException : HttpWebException
	{
		#region Properties

		public override HttpStatusCode StatusCode => HttpStatusCode.NotFound;
		#endregion

		#region Constructors

		public NotFoundException(string message) : base(message) { }

		public NotFoundException(string message, Exception innerException) : base(message, innerException) { }
		#endregion
	}
}