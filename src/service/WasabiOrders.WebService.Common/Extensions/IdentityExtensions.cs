﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;

namespace WasabiOrders.WebService
{
	public static class IdentityExtensions
	{
		#region Public class methods

		public static Guid GetSessionID(this IIdentity identity)
		{
			ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
			if (claimsIdentity == null)
			{
				throw new ArgumentOutOfRangeException(nameof(identity), $"Identity must be an instance of class {nameof(ClaimsIdentity)}.");
			}

			Guid sessionID = Guid.Empty;

			Claim identifierClaim = claimsIdentity.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sessionidentifier");
			if (identifierClaim != null)
			{
				Guid.TryParse(identifierClaim.Value, out sessionID);
			}

			return sessionID;
		}

		public static Guid GetUserID(this IIdentity identity)
		{
			ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
			if (claimsIdentity == null)
			{
				throw new ArgumentOutOfRangeException(nameof(identity), $"Identity must be an instance of class {nameof(ClaimsIdentity)}.");
			}

			Guid userID = Guid.Empty;

			Claim identifierClaim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);
			if (identifierClaim != null)
			{
				Guid.TryParse(identifierClaim.Value, out userID);
			}

			return userID;
		}
		#endregion
	}
}