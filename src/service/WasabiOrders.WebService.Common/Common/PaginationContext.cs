﻿using System;
using System.Collections.Specialized;

namespace WasabiOrders.WebService
{
	public class PaginationContext
	{
		#region Fields and properties

		private readonly char[] Separators;

		private readonly NameValueCollection Values;

		public int PageNumber => GetInt32OrDefault(nameof(PageNumber), 1);

		public int PageSize => GetInt32OrDefault(nameof(PageSize), 50);

		public int Offset => PageSize * (PageNumber - 1);

		public string OrderBy => Values.Get(nameof(OrderBy));
		#endregion

		#region Constructors

		public PaginationContext(NameValueCollection values)
		{
			if (values == null)
			{
				throw new ArgumentNullException(nameof(values), "Values collection can't be null.");
			}

			Separators = new char[] { ',', ';' };
			Values = values;
		}
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			return Values.ToString();
		}
        #endregion

        #region Public class methods

        public string GetOrderingExpression(string prefix = null, string defaultExpression = null)
        {
			if (string.IsNullOrEmpty(OrderBy))
			{
				return string.Concat(prefix, defaultExpression);
			}

			string[] expressions = OrderBy.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
			for (int index = 0; index < expressions.Length; ++index)
			{
				string current = expressions[index];
				if (string.IsNullOrEmpty(current))
				{
					continue;
				}
				expressions[index] = string.Concat(prefix, current.Trim());
			}
			return string.Join(",", expressions);
        }
        #endregion

        #region Private class methods

        private int GetInt32OrDefault(string name, int defaultValue)
		{
			string value = Values.Get(name);

			int parsedValue;
			if (!int.TryParse(value, out parsedValue))
			{
				parsedValue = defaultValue;
			}
			return parsedValue;
		}
		#endregion
	}
}