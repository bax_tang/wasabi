﻿using System;

using Newtonsoft.Json;

namespace WasabiOrders.WebService
{
	public sealed class GuidConverter : JsonConverter
	{
		#region Fields and properties

		private readonly Type GuidType;
		#endregion

		#region Constructors

		public GuidConverter()
		{
			GuidType = typeof(Guid);
		}
		#endregion

		#region JsonConvert methods overriding

		public override bool CanConvert(Type objectType)
		{
			if (objectType.IsGenericType)
			{
				return objectType.GenericTypeArguments[0] == GuidType;
			}

			return objectType == GuidType;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			string value = null;
			Guid result = Guid.Empty;

			switch (reader.TokenType)
			{
				case JsonToken.String:
					value = Convert.ToString(reader.Value);
					break;
				case JsonToken.PropertyName:
					value = reader.ReadAsString();
					break;
				default:
					break;
			}

			Guid.TryParse(value, out result);

			return result;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			Guid? id = value as Guid?;
			if (id.HasValue)
			{
				writer.WriteValue(id.Value.ToString().ToUpperInvariant());
				return;
			}

			writer.WriteNull();
		}
		#endregion
	}
}