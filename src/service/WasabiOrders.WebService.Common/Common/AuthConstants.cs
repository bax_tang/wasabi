﻿using System;

namespace WasabiOrders.WebService
{
	public static class AuthConstants
	{
		public const string DefaultAuthenticationType = "ApplicationCookie";

		public const string DefaultIdentityProviderClaimValue = "ASP.NET Identity";

		public const string Audience = "WasabiOrders";

		public const string Issuer = "WasabiOrders Web Service";
	}
}