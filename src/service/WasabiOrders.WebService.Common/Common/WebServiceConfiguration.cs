﻿using System;

namespace WasabiOrders.WebService
{
    public class WebServiceConfiguration
    {
        #region Static members

        private static WebServiceConfiguration ConfigInstance;

        public static WebServiceConfiguration Instance => ConfigInstance;

        public static void SetInstance(WebServiceConfiguration newInstance)
        {
            if (newInstance == null)
            {
                throw new ArgumentNullException(nameof(newInstance), "Configuration instance can't be null.");
            }

            ConfigInstance = newInstance;
        }
        #endregion

        #region Properties

        public string ServiceName { get; set; } = "WasabiOrders WebAPI";

        public string ServiceDescription { get; set; } = "WasabiOrders Web Service";

		public string SecurityKey { get; set; } = "/VOujctsW9lSVSO6Cx4lLH9C34KMgTdyk/8cbtNcBJyq7e7Rt4ts1A22b1ngb+o0KEuIrZSIJZ3GoHHXj8R78w==";

		/// <summary>
		/// Gets or sets value that determines whether automatic database migrations are used
		/// </summary>
		public bool UseDatabaseMigrations { get; set; } = true;

        public string ConnectionString { get; set; } = "Data Source=(local);Initial Catalog=WasabiOrders;User ID=sa;Password=saionara;Connect Timeout=30;";

        public string HostName { get; } = Environment.MachineName;

        public int ServicePort { get; set; } = 5000;
        #endregion

        #region Constructors

        static WebServiceConfiguration()
        {
            ConfigInstance = new WebServiceConfiguration();
        }

        internal WebServiceConfiguration() { }
        #endregion
    }
}