﻿using System;

namespace WasabiOrders.WebService
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ActionParameterAttribute : Attribute
	{
		#region Properties

		public object DefaultValue { get; set; }

		public ActionParameterLocation Location { get; private set; }

		public string Name { get; private set; }

		public string Description { get; set; }

		public string Format { get; set; }

		public bool IsRequired { get; set; }

		public ActionParameterType Type { get; private set; }
		#endregion

		#region Constructors

		public ActionParameterAttribute(ActionParameterLocation location, ActionParameterType type, string name)
		{
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name), "Parameter name can't be null or empty.");
            }

            Location = location;
            Type = type;
            Name = name;
		}
		#endregion
	}
}