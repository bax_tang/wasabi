﻿using System;

namespace WasabiOrders.WebService
{
    public enum ActionParameterType
	{
		boolean,
		integer,
		file,
		@string,
		uuid
	}
}