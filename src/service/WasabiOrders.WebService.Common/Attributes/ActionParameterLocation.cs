﻿using System;

namespace WasabiOrders.WebService
{
    /// <summary>
    /// Provides numeric constants that defines location of action parameter
    /// </summary>
    public enum ActionParameterLocation
    {
        /// <summary>
        /// Parameter located in header
        /// </summary>
        header,
        /// <summary>
        /// Parameter located in query
        /// </summary>
        query,
        /// <summary>
        /// Parameter located in body
        /// </summary>
        body
    }
}