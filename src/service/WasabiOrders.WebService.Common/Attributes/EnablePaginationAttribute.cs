﻿using System;

namespace WasabiOrders.WebService
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class EnablePaginationAttribute : Attribute
    {
        #region Fields and properties
        
        public int PageSize { get; private set; }

        public string OrderBy { get; private set; }
        #endregion

        #region Constructors

        public EnablePaginationAttribute(int pageSize, string orderBy)
        {
            if (pageSize <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(pageSize), "Page size can't be less than or equal zero.");
            }
            if (string.IsNullOrEmpty(orderBy))
            {
                throw new ArgumentOutOfRangeException(nameof(orderBy), "Ordering property name can't be null or empty.");
            }

            PageSize = pageSize;
            OrderBy = orderBy;
        }
        #endregion
    }
}