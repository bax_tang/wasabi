﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class Comment : UpdatableEntity<Guid>
	{
		#region Properties

		public Guid? RepliedTo { get; set; }

		public Guid ObjectID { get; set; }

		public Guid AuthorID { get; set; }

		public string Title { get; set; }

		public string Text { get; set; }

		public virtual User Author { get; set; }

		public virtual Comment ParentComment { get; set; }
		#endregion

		#region Constructors

		public Comment() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			if ((Author != null) && (Title != null))
			{
				return string.Format("{0}: {1}", Author.AccountName, Title);
			}
			if (Title != null)
			{
				return Title;
			}
			if (Text != null)
			{
				return Text;
			}
			return base.ToString();
		}
		#endregion
	}
}