﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class OrderItem : ObjectEntity<Guid>
	{
		#region Fields and properties

		private Order ItemOrder;

		private Product ItemProduct;

		public Guid OrderID { get; set; }

		public Guid ProductID { get; set; }

		public int Amount { get; set; }

		public virtual Order ParentOrder
		{
			get { return ItemOrder; }
			set
			{
				ItemOrder = value;

				if (value != null)
				{
					OrderID = value.Id;
				}
			}
		}

		public virtual Product RelatedProduct
		{
			get { return ItemProduct; }
			set
			{
				ItemProduct = value;

				if (value != null)
				{
					ProductID = value.Id;
				}
			}
		}
		#endregion

		#region Constructors

		public OrderItem() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			if (RelatedProduct != null)
			{
				return string.Format("{0} {1}", Amount, RelatedProduct.Name);
			}

			return base.ToString();
		}
		#endregion
	}
}