﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class ActivityLogRecord : KeyedEntity<long>
	{
		#region Fields and properties

		private User _recordUser;

		public long TypeID { get; set; }

		public Guid UserID { get; set; }

		public Guid? ObjectID { get; set; }

        public string ObjectName { get; set; }

		public DateTime ActivityDateTime { get; set; }

		public virtual ActivityType Type { get; set; }

		public virtual User User
		{
			get { return _recordUser; }
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException(nameof(value), "Activity log record user can't be null.");
				}

				_recordUser = value;
				UserID = value.Id;
			}
		}
		#endregion

		#region Constructors

		public ActivityLogRecord() : base() { }
		#endregion
	}
}