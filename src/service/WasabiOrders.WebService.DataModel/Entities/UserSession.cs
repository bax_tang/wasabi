﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class UserSession : KeyedEntity<Guid>
	{
		#region Fields and properties

		public Guid UserID { get; set; }

		public DateTime LoginDateTime { get; set; }

		public virtual User User { get; set; }
		#endregion

		#region Constructors

		public UserSession() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			return string.Format("Session '{0}' for user {1}", Id, User);
		}
		#endregion
	}
}