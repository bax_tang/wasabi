﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class User : UpdatableEntity<Guid>
	{
		#region Properties

		public string AccountName { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }

		public string PasswordHash { get; set; }

		public double Discount { get; set; }
		#endregion

		#region Constructors

		public User() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			return string.Format("{0}: {1} {2}", AccountName, FirstName, LastName);
		}
		#endregion
	}
}