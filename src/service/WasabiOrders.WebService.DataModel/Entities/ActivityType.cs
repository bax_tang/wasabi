﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class ActivityType : KeyedEntity<long>
	{
		#region Properties

		public string Name { get; set; }

		public string Text { get; set; }
		#endregion

		#region Constructors

		public ActivityType() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			return string.Concat(Name, ": ", Text);
		}
		#endregion
	}
}