﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class GlobalSetting : UpdatableEntity<long>
	{
		#region Properties

		public string Name { get; set; }

		public string Value { get; set; }
		#endregion

		#region Constructors

		public GlobalSetting() { }
		#endregion
	}
}