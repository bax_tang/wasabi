﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public abstract class KeyedEntity<TKey> : EmptyEntity where TKey : struct
	{
		#region Properties

		public TKey Id { get; set; }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			return string.Format("{0} '{1}'", GetType().Name, Id);
		}
		#endregion
	}
}