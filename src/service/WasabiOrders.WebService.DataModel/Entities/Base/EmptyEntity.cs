﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public abstract class EmptyEntity
	{
		#region Constructors

		protected internal EmptyEntity() { }
		#endregion
	}
}