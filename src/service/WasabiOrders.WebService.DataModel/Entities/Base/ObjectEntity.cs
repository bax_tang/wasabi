﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public interface IObjectEntity
	{
		DateTime? CreatedAt { get; set; }
	}

	public abstract class ObjectEntity<TKey> : KeyedEntity<TKey>, IObjectEntity where TKey : struct
	{
		#region Constants and fields

		protected const string DateTimeFormat = "dd.MM.yyyy HH:mm:ss";
		#endregion

		#region Properties

		public DateTime? CreatedAt { get; set; }
		#endregion

		#region Constructors

		protected internal ObjectEntity() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			DateTime? createdAt = CreatedAt;
			if (createdAt.HasValue)
			{
				string dateText = createdAt.Value.ToString(DateTimeFormat);

				return string.Format("{0} ({2})", GetType().Name, dateText);
			}

			return base.ToString();
		}
		#endregion
	}
}