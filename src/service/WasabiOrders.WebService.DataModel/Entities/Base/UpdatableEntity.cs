﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public abstract class UpdatableEntity<TKey> : ObjectEntity<TKey> where TKey : struct
	{
		#region Properties

		public DateTime? UpdatedAt { get; set; }
		#endregion

		#region Constructors

		protected internal UpdatableEntity() : base() { }
		#endregion
	}
}