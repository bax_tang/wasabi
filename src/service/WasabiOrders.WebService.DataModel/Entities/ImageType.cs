﻿using System;

namespace WasabiOrders.WebService.DataModel
{
	public enum ImageType : int
	{
		Thumbnail = 0,

		Poster = 1,

		Other = 2
	}
}