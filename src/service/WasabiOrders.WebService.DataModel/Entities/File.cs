﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class File : ObjectEntity<Guid>
	{
		#region Properties

		public string Name { get; set; }

		public string Type { get; set; }

		public long Size { get; set; }

		public string ExternalFileID { get; set; }

		public string ExternalFileUrl { get; set; }
		#endregion

		#region Constructors

		public File() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			return Name ?? base.ToString();
		}
		#endregion
	}
}