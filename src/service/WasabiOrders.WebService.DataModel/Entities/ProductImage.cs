﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class ProductImage : ObjectEntity<Guid>
	{
		#region Fields and properties

		private Product Product;

		private File _file;

		public Guid ProductID { get; set; }
		
		public Guid FileID { get; set; }

		public ImageType Type { get; set; }

		public virtual Product ParentProduct
		{
			get { return Product; }
			set
			{
				Product = value;

				if (value != null)
				{
					ProductID = value.Id;
				}
			}
		}

		public virtual File ImageFile
		{
			get { return _file; }
			set
			{
				_file = value;

				if (value != null)
				{
					FileID = value.Id;
				}
			}
		}
		#endregion

		#region Constructors

		public ProductImage() : base() { }
		#endregion
	}
}