﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class Product : UpdatableEntity<Guid>
	{
		#region Fields and properties

		public Guid CategoryID { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public decimal Price { get; set; }

		public double Weight { get; set; }

		public int? Rating { get; set; }

		public string ExternalFolderID { get; set; }

		public virtual ProductCategory Category { get; set; }

		public virtual ICollection<ProductImage> Images { get; set; }
		#endregion

		#region Constructors

		public Product() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			if (Category != null)
			{
				return string.Format("{0}: {1}", Category.Name, Name);
			}

			return Name;
		}
		#endregion
	}
}