﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class Order : UpdatableEntity<Guid>
	{
		#region Fields and properties

		public Guid AuthorID { get; set; }

		public string Name { get; set; }

		public OrderStatus Status { get; set; }

		public DateTime OrderDateTime { get; set; }

		public DateTime? ExpectedDeliveryDateTime { get; set; }

		public DateTime? ActualDeliveryDateTime { get; set; }

		public decimal? TotalPrice { get; set; }

		public decimal? DiscountedPrice { get; set; }

		public int? Rating { get; set; }

		public string ExternalFolderID { get; set; }

		public virtual User Author { get; set; }

		public virtual ICollection<OrderImage> Images { get; set; }

		public virtual ICollection<OrderItem> Items { get; set; }
		#endregion

		#region Constructors

		public Order() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			return string.Format("{0} ({1}): {2} руб.", Name, Status, TotalPrice.GetValueOrDefault(0M));
		}
		#endregion
	}
}