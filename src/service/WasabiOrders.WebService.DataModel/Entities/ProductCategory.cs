﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public class ProductCategory : UpdatableEntity<Guid>
	{
		#region Properties

		public string Name { get; set; }

		public string Description { get; set; }

		public string ExternalFolderID { get; set; }
		#endregion

		#region Constructors

		public ProductCategory() : base() { }
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			if (string.IsNullOrEmpty(Description))
			{
				return Name;
			}

			return string.Concat(Name, ": ", Description);
		}
		#endregion
	}
}