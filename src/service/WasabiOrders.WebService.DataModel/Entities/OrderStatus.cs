﻿using System;

namespace WasabiOrders.WebService.DataModel
{
	public enum OrderStatus : int
	{
		Created = 0,
		Registered = 1,
		Delivered = 2
	}
}