﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
    public class OrderImage : ObjectEntity<Guid>
    {
        #region Fields and properties

        private Order Order;

		private File _file;

        public Guid OrderID { get; set; }

        public Guid FileID { get; set; }

        public virtual Order ParentOrder
        {
            get { return Order; }
            set
            {
                Order = value;

				if (value != null)
				{
					OrderID = value.Id;
				}
            }
        }

        public virtual File ImageFile
        {
            get { return _file; }
            set
            {
				_file = value;

				if (value != null)
				{
					FileID = value.Id;
				}
            }
        }
        #endregion

        #region Constructors

        public OrderImage() : base() { }
        #endregion
    }
}