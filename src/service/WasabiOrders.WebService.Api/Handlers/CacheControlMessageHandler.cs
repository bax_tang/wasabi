﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace WasabiOrders.WebService
{
	public class CacheControlMessageHandler : DelegatingHandler
	{
		#region Constructors

		public CacheControlMessageHandler() { }

		public CacheControlMessageHandler(HttpMessageHandler innerHandler) : base(innerHandler) { }
		#endregion

		#region DelegatingHandler methods overriding

		protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			var response = await base.SendAsync(request, cancellationToken);
			
			CacheControlHeaderValue cacheControlHeader = response.Headers.CacheControl;
			if (cacheControlHeader != null)
			{
				cacheControlHeader.NoCache = true;
			}
			else
			{
				cacheControlHeader = new CacheControlHeaderValue
				{
					NoCache = true
				};
				response.Headers.CacheControl = cacheControlHeader;
			}

			return response;
		}
		#endregion
	}
}