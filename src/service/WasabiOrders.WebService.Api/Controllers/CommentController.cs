﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace WasabiOrders.WebService.Controllers
{
	using DomainModel;
	using DomainModel.Dtos;
	using DomainModel.Services;

	/// <summary>
	/// Provide endpoints to work with comments
	/// </summary>
	public class CommentController : ApiControllerBase
	{
		#region Fields and properties

		private ICommentService _commentService;
		#endregion

		#region Constructors

		public CommentController(ICommentService commentService) : base()
		{
			_commentService = commentService;
		}
        #endregion

        #region Action methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectID"></param>
        /// <param name="rootCommentID"></param>
        /// <param name="maxLevel"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/comments")]
        [ResponseType(typeof(CollectionResult<CommentResponseDto>))]
		public async Task<IHttpActionResult> GetComments([FromUri] Guid objectID, [FromUri] Guid? rootCommentID = null, [FromUri] int maxLevel = 10)
		{
			var getResult = await _commentService.GetCommentsAsync(objectID, rootCommentID, maxLevel);
			if (getResult != null)
			{
				return GetJsonResult(getResult);
			}

			return InternalServerError();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commentDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/comments/new")]
        [ResponseType(typeof(IdentityResponseDto))]
		public async Task<IHttpActionResult> Create([FromBody] CommentDto commentDto)
		{
			Guid userID = User.Identity.GetUserID();
			var createResult = await _commentService.CreateCommentAsync(userID, commentDto);
			if (createResult.Succeeded)
			{
				return GetJsonResult(createResult.Content);
			}

			return GetErrorResultFromOperationResult(createResult);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commentDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/comments")]
		public async Task<IHttpActionResult> Update([FromBody] CommentDto commentDto)
		{
			Guid userID = User.Identity.GetUserID();
			return BadRequest("Action does not implemented yet.");
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/comments/{id:Guid}")]
		public async Task<IHttpActionResult> Delete([FromUri] Guid id)
		{
			return BadRequest("Action does not implemented yet.");
		}
		#endregion
	}
}