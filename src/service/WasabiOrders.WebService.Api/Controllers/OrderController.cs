﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace WasabiOrders.WebService.Controllers
{
	using DomainModel;
	using DomainModel.Dtos;
	using DomainModel.Services;

    /// <summary>
    /// Provide endpoints to work with orders
    /// </summary>
    public class OrderController : ApiControllerBase
	{
		#region Fields and properties

		private IOrderService _orderService;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of <see cref="OrderController"/>
		/// </summary>
		public OrderController(IOrderService orderService) : base()
		{
			_orderService = orderService;
		}
		#endregion

		#region Action methods

		/// <summary>
		/// Get all orders
		/// </summary>
		/// <remarks>
		/// Gets a list of all existing orders with pagination
		/// </remarks>
		/// <response code="200">OK</response>
		/// <response code="500">Internal server error</response>
		/// <returns></returns>
		[HttpGet]
		[Route("api/orders")]
		[ResponseType(typeof(CollectionResult<OrderResponseDto>))]
        [EnablePagination(10, "OrderDateTime DESC")]
		public async Task<IHttpActionResult> GetAll([FromUri] Guid? productId = null, [FromUri] Guid? categoryId = null)
		{
            PaginationContext paginationContext = GetPaginationContext();

			var result = await _orderService.GetAllAsync(paginationContext);
			if (result != null)
			{
				return GetJsonResult(result);
			}

			return InternalServerError();
		}

		/// <summary>
		/// Get order
		/// </summary>
		/// <remarks>
		/// Gets detailed information about an existing order
		/// </remarks>
		/// <param name="id">Unique order identifier (Guid)</param>
		/// <response code="200">OK</response>
		/// <response code="401">
		/// Possible reasons:
		///     - The request's Authorization header was not specified
		///     - The request's Authorization header has invalid format
		/// </response>
		/// <response code="404">Order with specified identifier does not exist</response>
		/// <response code="500">Internal server error</response>
		/// <returns></returns>
		[HttpGet]
		[Route("api/orders/{id:Guid}")]
		[ResponseType(typeof(OrderDetailedResponseDto))]
		public async Task<IHttpActionResult> Get([FromUri] Guid id)
		{
			var getResult = await _orderService.GetAsync(id);
			if (getResult.Succeeded)
			{
				return GetJsonResult(getResult.Content);
			}

			return GetErrorResultFromOperationResult(getResult);
		}

		/// <summary>
		/// Create new order
		/// </summary>
		/// <remarks>
		/// Creates a new order
		/// </remarks>
		/// <response code="200">OK</response>
		/// <response code="401">
		/// Possible reasons:
		///     - The request's Authorization header was not specified
		///     - The request's Authorization header has invalid format
		/// </response>
		/// <response code="500">Internal server error</response>
		/// <param name="orderDto">Order DTO</param>
		/// <returns></returns>
		[HttpPost]
		[Route("api/orders/new")]
		[ResponseType(typeof(IdentityResponseDto))]
		public async Task<IHttpActionResult> Create([FromBody] OrderDto orderDto)
		{
			var createResult = await _orderService.CreateAsync(User.Identity, orderDto);
			if (createResult.Succeeded)
			{
				return GetJsonResult(createResult.Content);
			}

			return GetErrorResultFromOperationResult(createResult);
		}

		/// <summary>
		/// Update order
		/// </summary>
		/// <remarks>
		/// Updates an existing order
		/// </remarks>
		/// <response code="200">OK</response>
		/// <response code="401">
		/// Possible reasons:
		///     - The request's Authorization header was not specified
		///     - The request's Authorization header has invalid format
		/// </response>
		/// <response code="500">Internal server error</response>
		/// <param name="orderDto">Order DTO</param>
		/// <returns></returns>
		[HttpPost]
		[Route("api/orders")]
		public async Task<IHttpActionResult> Update([FromBody] OrderDto orderDto)
		{
			var updateResult = await _orderService.UpdateAsync(User.Identity, orderDto);
			if (updateResult.Succeeded)
			{
				return Ok();
			}

			return GetErrorResultFromOperationResult(updateResult);
		}

		/// <summary>
		/// Upload order images
		/// </summary>
		/// <remarks>
		/// Uploads a list of order images
		/// </remarks>
		/// <param name="id">Unique order identifier (Guid)</param>
		/// <response code="200">OK</response>
		/// <response code="500">Internal server error</response>
		/// <returns></returns>
		[HttpPost]
		[Route("api/orders/{id}/images")]
		[ResponseType(typeof(ImagesResponseDto))]
		[ActionParameter(ActionParameterLocation.body, ActionParameterType.file, "images", IsRequired = true, Format = "image")]
		public async Task<IHttpActionResult> UploadImages([FromUri] Guid id)
        {
            if (Request.Content.IsMimeMultipartContent())
            {
				List<FileDto> images = await ReadImagesFromRequestContent(Request.Content);

				var result = await _orderService.UploadImagesAsync(User.Identity, id, images);
				if (result.Succeeded)
				{
					return GetJsonResult(result.Content);
				}

				return GetErrorResultFromOperationResult(result);
			}

            return BadRequest("Invalid content type.");
        }
		#endregion

		#region Private class methods

		private async Task<List<FileDto>> ReadImagesFromRequestContent(HttpContent content)
		{
			MultipartMemoryStreamProvider provider = await content.ReadAsMultipartAsync();

			var contents = provider.Contents;

			List<FileDto> images = new List<FileDto>(contents.Count);

			foreach (HttpContent imageContent in contents)
			{
				Stream imageData = await imageContent.ReadAsStreamAsync();
				string imageName = imageContent.Headers.ContentDisposition.FileName;
				string imageType = imageContent.Headers.ContentType.MediaType;

				images.Add(new FileDto
				{
					Data = imageData,
					Name = imageName,
					Type = imageType
				});
			}

			return images;
		}
		#endregion
	}
}