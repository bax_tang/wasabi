﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace WasabiOrders.WebService.Controllers
{
	using DomainModel;
	using DomainModel.Dtos;
	using DomainModel.Services;

	/// <summary>
	/// Provide endpoints to work with categories
	/// </summary>
	public class CategoryController : ApiControllerBase
	{
		#region Fields and properties

		private ICategoryService _categoryService;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="CategoryController"/>
		/// </summary>
		/// <param name="categoryService">Instance of <see cref="ICategoryService"/></param>
		public CategoryController(ICategoryService categoryService)
		{
			_categoryService = categoryService;
		}
        #endregion

        #region Action methods

        /// <summary>
        /// Get categories
        /// </summary>
        /// <remarks>
        /// Gets a list of existing product categories
        /// </remarks>
        /// <response code="200">OK</response>
        /// <response code="500">Internal server error</response>
        /// <returns>List of existing product categories</returns>
        [HttpGet]
        [Route("api/categories")]
        [ResponseType(typeof(CollectionResult<CategoryResponseDto>))]
		public async Task<IHttpActionResult> GetAll()
		{
			var getResult = await _categoryService.GetAllAsync();
			if (getResult != null)
			{
				return GetJsonResult(getResult);
			}

			return InternalServerError();
		}

        /// <summary>
        /// Get category
        /// </summary>
        /// <remarks>
        /// Gets an existing category 
        /// </remarks>
        /// <param name="id">Unique category identifier (Guid)</param>
        /// <response code="200">OK</response>
        /// <response code="401">Authorization failed</response>
        /// <response code="500">Internal server error</response>
        /// <returns></returns>
        [HttpGet]
        [Route("api/categories/{id:Guid}")]
        [ResponseType(typeof(CategoryResponseDto))]
        public async Task<IHttpActionResult> Get([FromUri] Guid id)
        {
            var getResult = await _categoryService.GetAsync(id);
            if (getResult.Succeeded)
            {
                return GetJsonResult(getResult.Content);
            }

            return GetErrorResultFromOperationResult(getResult);
		}

        /// <summary>
        /// Create new category
        /// </summary>
        /// <remarks>
        /// Creates a new category
        /// </remarks>
        /// <param name="categoryDto">Category DTO</param>
        /// <response code="200">OK</response>
        /// <response code="500">Internal server error</response>
        /// <returns></returns>
        [HttpPost]
        [Route("api/categories/new")]
        [ResponseType(typeof(IdentityResponseDto))]
		public async Task<IHttpActionResult> Create([FromBody] CategoryDto categoryDto)
		{
			var createResult = await _categoryService.CreateAsync(categoryDto);
			if (createResult.Succeeded)
			{
				return GetJsonResult(createResult.Content);
			}

			return GetErrorResultFromOperationResult(createResult);
		}

        /// <summary>
        /// Delete an existing category
        /// </summary>
        /// <remarks>
        /// Deletes an existing category with specified identifier
        /// </remarks>
        /// <param name="id">Unique category identifier (Guid)</param>
		/// <response code="200">OK</response>
		/// <response code="500">Internal server error</response>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/categories/{id:Guid}")]
        public async Task<IHttpActionResult> Delete([FromUri] Guid id)
        {
            var deleteResult = await _categoryService.DeleteAsync(id);
            if (deleteResult.Succeeded)
            {
                return Ok();
            }

            return GetErrorResultFromOperationResult(deleteResult);
        }
		#endregion
	}
}