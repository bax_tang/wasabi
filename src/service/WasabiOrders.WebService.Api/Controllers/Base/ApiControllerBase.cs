﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

using Newtonsoft.Json;

namespace WasabiOrders.WebService.Controllers
{
	using DomainModel;

	/// <summary>
	/// Provide base methods for all service controllers
	/// </summary>
	[Authorize]
	public class ApiControllerBase : ApiController
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of <see cref="ApiControllerBase"/>
		/// </summary>
		public ApiControllerBase() { }
		#endregion

		#region Class methods

		/// <summary>
		/// Creates <see cref="PaginationContext"/> from query string of current request
		/// </summary>
		/// <returns>Instance of <see cref="PaginationContext"/></returns>
		protected PaginationContext GetPaginationContext()
		{
			NameValueCollection queryParts = HttpUtility.ParseQueryString(Request.RequestUri.Query);

			PaginationContext paginationContext = new PaginationContext(queryParts);
			return paginationContext;
		}

		/// <summary>
		/// Creates a <see cref="JsonResult{T}" /> (200 OK) with specified content using default serialization settings
		/// </summary>
		/// <typeparam name="TResult">Content object type</typeparam>
		/// <param name="content">Result content</param>
		/// <returns>Instance of <see cref="JsonResult{T}"/></returns>
		protected JsonResult<TResult> GetJsonResult<TResult>(TResult content)
		{
			JsonSerializerSettings settings = Configuration.Formatters.JsonFormatter.SerializerSettings;

			JsonResult<TResult> result = Json(content, settings);

			return result;
		}

		/// <summary>
		/// Creates a <see cref="IHttpActionResult" /> from specified <see cref="OperationResult" />.
		/// </summary>
		/// <param name="operationResult">Service operation result</param>
		/// <returns>Instance of the <see cref="IHttpActionResult"/></returns>
		protected IHttpActionResult GetErrorResultFromOperationResult(OperationResult operationResult)
		{
			if (operationResult != null)
			{
				if (operationResult.Succeeded)
				{
					return Ok();
				}

				if (ModelState.IsValid)
				{
					return ResponseMessage(CreateResponseFromOperationResult(operationResult));
				}
				else
				{
					return BadRequest(ModelState);
				}
			}

			return InternalServerError();
		}

		private HttpResponseMessage CreateResponseFromOperationResult(OperationResult operationResult)
		{
			var content = new
			{
				Message = string.Join("\r\n", operationResult.Errors)
			};
			string contentText = JsonConvert.SerializeObject(content);

			HttpContent responseContent = new StringContent(contentText);
			responseContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

			HttpResponseMessage response = new HttpResponseMessage()
			{
				Content = responseContent,
				RequestMessage = Request,
				StatusCode = operationResult.StatusCode
			};
			return response;
		}
		#endregion
	}
}