﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace WasabiOrders.WebService.Controllers
{
	using DomainModel.Dtos;
	using DomainModel.Services;

    /// <summary>
    /// Provide endpoints to gain access to service
    /// </summary>
    public class AccountController : ApiControllerBase
    {
		#region Fields and properties

		private IAccountService _accountService;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="AccountController"/>
		/// </summary>
		public AccountController(IAccountService accountService) : base()
		{
			_accountService = accountService;
		}
        #endregion

        #region Action methods
        /// <summary>
        /// Perform user login
        /// </summary>
        /// <remarks>
        /// Authenticate user with specified account name and password
        /// </remarks>
		/// <param name="loginDto">Login information</param>
        /// <response code="200">OK</response>
        /// <response code="400">
        /// Possible reasons:
        ///     - The user with specified account name and password does not exist
        ///     - Unsupported grant type specified
        ///     - Invalid model data
        /// </response>
        /// <response code="500">Internal server error</response>
        /// <returns></returns>
        [AllowAnonymous]
		[HttpPost]
		[Route("api/account/login")]
		[ResponseType(typeof(AccountInfoResponseDto))]
        public async Task<IHttpActionResult> Login([FromBody] AccountInfoDto accountInfoDto)
        {
			var loginResult = await _accountService.LoginAsync(accountInfoDto);
			if (loginResult.Succeeded)
			{
				return GetJsonResult(loginResult.Content);
			}

			return GetErrorResultFromOperationResult(loginResult);
        }
		/// <summary>
		/// Perform user logout
		/// </summary>
		/// <remarks>
		/// Deauthenticate current user
		/// </remarks>
		/// <response code="200">OK</response>
		/// <response code="401">
		/// Possible reasons:
		///     - The request's Authorization header was not specified
		///     - The request's Authorization header has invalid format
		/// </response>
		/// <response code="500">Internal server error</response>
		/// <returns></returns>
		[HttpPost]
		[Route("api/account/logout")]
        public async Task<IHttpActionResult> Logout()
        {
			var logoutResult = await _accountService.LogoutAsync(User.Identity);
			if (logoutResult.Succeeded)
			{
				return Ok();
			}

			return GetErrorResultFromOperationResult(logoutResult);
        }
        #endregion
    }
}