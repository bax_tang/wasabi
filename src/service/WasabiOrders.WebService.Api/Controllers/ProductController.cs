﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace WasabiOrders.WebService.Controllers
{
	using DomainModel;
	using DomainModel.Dtos;
	using DomainModel.Services;

    /// <summary>
    /// Provide endpoints to work with products
    /// </summary>
    public class ProductController : ApiControllerBase
    {
        #region Fields and properties

        private IProductService _productService;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductController"/>
        /// </summary>
        /// <param name="productService">Instance of the <see cref="IProductService"/></param>
        public ProductController(IProductService productService) : base()
        {
            _productService = productService;
        }
		#endregion

		#region Action methods

		/// <summary>
		/// Get products
		/// </summary>
		/// <remarks>
		/// Gets a list of all products
		/// </remarks>
		/// <response code="200">OK</response>
		/// <response code="500">Internal server error</response>
		/// <returns></returns>
		[HttpGet]
		[Route("api/products")]
		[ResponseType(typeof(CollectionResult<ProductResponseDto>))]
        [EnablePagination(50, "Name ASC")]
		public async Task<IHttpActionResult> GetAll([FromUri] Guid? categoryId = null)
        {
			PaginationContext paginationContext = GetPaginationContext();

			var result = await _productService.GetAllAsync(categoryId, paginationContext);
			if (result != null)
			{
				return GetJsonResult(result);
			}

			return InternalServerError();
        }

        /// <summary>
        /// Get product
        /// </summary>
        /// <remarks>
        /// Gets detailed information about an existing product
        /// </remarks>
        /// <param name="id">Unique product identifier (Guid)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/products/{id:Guid}")]
        [ResponseType(typeof(ProductDetailedResponseDto))]
        public async Task<IHttpActionResult> Get([FromUri] Guid id)
        {
            var getResult = await _productService.GetAsync(id);
            if (getResult.Succeeded)
            {
                return GetJsonResult(getResult.Content);
            }

            return GetErrorResultFromOperationResult(getResult);
		}

        /// <summary>
        /// Create new product
        /// </summary>
        /// <remarks>
        /// Creates a new product
        /// </remarks>
        /// <response code="200">OK</response>
        /// <response code="500">Internal server error</response>
        /// <param name="productDto">Product DTO</param>
        /// <returns></returns>
        [HttpPost]
		[Route("api/products/new")]
        [ResponseType(typeof(IdentityResponseDto))]
        public async Task<IHttpActionResult> Create([FromBody] ProductDto productDto)
		{
			var createResult = await _productService.CreateAsync(User.Identity, productDto);
			if (createResult.Succeeded)
			{
				return GetJsonResult(createResult.Content);
			}

			return GetErrorResultFromOperationResult(createResult);
		}

		/// <summary>
		/// Upload product images
		/// </summary>
		/// <remarks>
		/// Uploads a list of product images
		/// </remarks>
		/// <param name="id">Unique product identifier (Guid)</param>
		/// <response code="200">OK</response>
		/// <response code="400">Request content type isn't "multipart/form-data"</response>
		/// <response code="500">Internal server error</response>
		/// <returns></returns>
		[HttpPost]
		[Route("api/products/{id}/images")]
		[ResponseType(typeof(ImagesResponseDto))]
		[ActionParameter(ActionParameterLocation.body, ActionParameterType.file, "images", IsRequired = true, Format = "image")]
        public async Task<IHttpActionResult> UploadImages([FromUri] Guid id)
        {
            if (Request.Content.IsMimeMultipartContent())
            {
				List<FileDto> images = await ReadImagesFromRequestContent(Request.Content);

				var result = await _productService.UploadImagesAsync(User.Identity, id, images);
				if (result.Succeeded)
				{
					return GetJsonResult(result.Content);
				}

				return GetErrorResultFromOperationResult(result);
            }

            return BadRequest("Invalid content type.");
        }
		#endregion

		#region Private class methods

		private async Task<List<FileDto>> ReadImagesFromRequestContent(HttpContent content)
		{
			MultipartMemoryStreamProvider provider = await content.ReadAsMultipartAsync();

			var contents = provider.Contents;

			List<FileDto> images = new List<FileDto>(contents.Count);

			foreach (HttpContent imageContent in contents)
			{
				Stream imageData = await imageContent.ReadAsStreamAsync();
				string imageName = imageContent.Headers.ContentDisposition.FileName;
				string imageType = imageContent.Headers.ContentType.MediaType;

				images.Add(new FileDto
				{
					Data = imageData,
					Name = imageName,
					Type = imageType
				});
			}

			return images;
		}
		#endregion
	}
}