﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace WasabiOrders.WebService.Controllers
{
	using DomainModel.Dtos;
	using DomainModel.Services;

	/// <summary>
	/// Provide endpoints to work with files
	/// </summary>
	public class FileController : ApiControllerBase
	{
		#region Fields and properties

		private IFileService _fileService;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of <see cref="FileController"/>
		/// </summary>
		public FileController(IFileService fileService)
		{
			if (fileService == null)
			{
				throw new ArgumentNullException(nameof(fileService), "File service can't be null.");
			}

			_fileService = fileService;
		}
        #endregion

        #region Action methods

        /// <summary>
        /// Get file
        /// </summary>
        /// <remarks>
        /// Gets an existing file with specified identifier
        /// </remarks>
        /// <param name="id">Unique file identifier (Guid)</param>
        /// <response code="200">OK</response>
        /// <response code="404">File with specified identifier does not exist</response>
        /// <response code="500">Internal server error</response>
        /// <returns></returns>
        [HttpGet]
        [Route("api/files/{id:Guid}")]
		[ResponseType(typeof(FileResponseDto))]
		public async Task<IHttpActionResult> Get([FromUri] Guid id)
		{
			var getResult = await _fileService.GetFileAsync(id);
			if (getResult.Succeeded)
			{
				return GetJsonResult(getResult.Content);
			}

			return GetErrorResultFromOperationResult(getResult);
		}

		/// <summary>
		/// Delete file
		/// </summary>
		/// <remarks>
		/// Deletes an existing file with specified identifier
		/// </remarks>
		/// <param name="id">Unique file identifier (Guid)</param>
		/// <response code="200">OK</response>
		/// <response code="404">File with specified identifier does not exist</response>
		/// <response code="500">Internal server error</response>
		/// <returns></returns>
		[HttpDelete]
		[Route("api/files/{id:Guid}")]
		public async Task<IHttpActionResult> Delete([FromUri] Guid id)
		{
			var deleteResult = await _fileService.DeleteFileAsync(id);
			if (deleteResult.Succeeded)
			{
				return Ok();
			}

			return GetErrorResultFromOperationResult(deleteResult);
		}
		#endregion
	}
}