﻿using System;

using AutoMapper;

namespace WasabiOrders.WebService.DomainModel.AutoMappings
{
	using DataModel.Entities;
	using Dtos;

	public class OrderAutoMap : AutoMap<Order, OrderDto>
	{
		#region Constructors

		public OrderAutoMap() : base() { }
		#endregion

		#region AutoMap methods overriding

		protected override OrderDto CreateSecondFromFirst(Order order, OrderDto orderDto)
		{
			throw new InvalidOperationException("Converting from Order to OrderDto is not implemented.");
		}

		protected override Order CreateFirstFromSecond(OrderDto orderDto, Order order)
		{
			if (order == null)
			{
				order = new Order();
			}

			order.Name = orderDto.Name;
			order.Status = orderDto.Status;
			order.Rating = orderDto.Rating;
			order.OrderDateTime = orderDto.OrderDateTime;
			order.ExpectedDeliveryDateTime = orderDto.ExpectedDeliveryDateTime;
			order.ActualDeliveryDateTime = orderDto.ActualDeliveryDateTime;
			order.TotalPrice = orderDto.TotalPrice;
			order.DiscountedPrice = orderDto.DiscountedPrice;

			return order;
		}
		#endregion
	}
}