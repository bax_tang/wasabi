﻿using System;
using System.Linq.Expressions;

using AutoMapper;

namespace WasabiOrders.WebService.DomainModel.AutoMappings
{
	using DataModel.Entities;
	using Dtos;

	public class CommentAutoMap : AutoMap<Comment, CommentDto>
	{
		#region Constructors

		public CommentAutoMap() { }
		#endregion

		#region AutoMap methods overriding

		protected override CommentDto CreateSecondFromFirst(Comment comment, CommentDto commentDto)
		{
			throw new InvalidOperationException("Converting from Comment to CommentDto is not implemented.");
		}

		protected override Comment CreateFirstFromSecond(CommentDto commentDto, Comment comment)
		{
			if (comment == null)
			{
				comment = new Comment();
			}

			comment.ObjectID = commentDto.ObjectId;
			comment.Title = commentDto.Title;
			comment.Text = commentDto.Text;
			comment.RepliedTo = commentDto.RepliedTo;

			return comment;
		}
		#endregion
	}
}