﻿using System;

using AutoMapper;

namespace WasabiOrders.WebService.DomainModel.AutoMappings
{
	public interface IAutoMap
	{
		void Apply(IMapperConfigurationExpression configuration);
	}
}