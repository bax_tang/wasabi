﻿using System;

using AutoMapper;

namespace WasabiOrders.WebService.DomainModel.AutoMappings
{
	public abstract class AutoMap<TFirst, TSecond> : IAutoMap
	{
		#region Fields and properties
		#endregion

		#region Constructors

		protected internal AutoMap() { }
		#endregion

		#region IAutoMap implementation

		public void Apply(IMapperConfigurationExpression configuration)
		{
			Configure(configuration.CreateMap<TFirst, TSecond>());
			ConfigureBack(configuration.CreateMap<TSecond, TFirst>());
		}
		#endregion

		#region Class methods

		protected virtual void Configure(IMappingExpression<TFirst, TSecond> expression)
		{
			expression.ConvertUsing(CreateSecondFromFirst);
		}

		protected virtual void ConfigureBack(IMappingExpression<TSecond, TFirst> expression)
		{
			expression.ConvertUsing(CreateFirstFromSecond);
		}

		protected abstract TSecond CreateSecondFromFirst(TFirst source, TSecond destination);

		protected abstract TFirst CreateFirstFromSecond(TSecond source, TFirst destination);
		#endregion
	}
}