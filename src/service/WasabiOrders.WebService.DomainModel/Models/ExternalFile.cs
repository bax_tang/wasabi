﻿using System;

namespace WasabiOrders.WebService.DomainModel.Models
{
	public class ExternalFile
	{
		#region Properties

		public string Id { get; private set; }

		public string Name { get; private set; }

		public string DownloadUrl { get; set; }
		#endregion

		#region Constructors

		internal ExternalFile(string id, string name)
		{
			Id = id;
			Name = name;
		}
		#endregion
	}
}