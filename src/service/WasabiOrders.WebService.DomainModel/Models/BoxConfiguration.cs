﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DomainModel.Models
{
	public class BoxConfiguration
	{
		#region Fields and properties

		private readonly IDictionary<string, string> _settings;

		public string BoxClientID => GetValueInternal(nameof(BoxClientID));

		public string BoxClientSecret => GetValueInternal(nameof(BoxClientSecret));

		public string BoxEnterpriseID => GetValueInternal(nameof(BoxEnterpriseID));

		public string BoxPrivateKey => GetValueInternal(nameof(BoxPrivateKey));

		public string BoxPrivateKeyPassphrase => GetValueInternal(nameof(BoxPrivateKeyPassphrase));

		public string BoxPublicKeyID => GetValueInternal(nameof(BoxPublicKeyID));

		public string BoxUserID => GetValueInternal(nameof(BoxUserID));

		public string BoxProductsFolder => GetValueInternal(nameof(BoxProductsFolder));

		public string BoxOrdersFolder => GetValueInternal(nameof(BoxOrdersFolder));
		#endregion

		#region Constructors

		internal BoxConfiguration(IDictionary<string, string> settings)
		{
			_settings = settings;
		}
		#endregion

		#region Private class methods

		private string GetValueInternal(string name)
		{
			string value = null;
			_settings.TryGetValue(name, out value);
			return value ?? string.Empty;
		}
		#endregion
	}
}