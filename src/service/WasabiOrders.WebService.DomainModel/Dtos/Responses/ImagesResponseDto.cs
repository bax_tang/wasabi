﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class ImagesResponseDto
	{
		#region Properties

		public IEnumerable<ImageResponseDto> Images { get; set; }
		#endregion

		#region Constructors

		public ImagesResponseDto() { }
		#endregion
	}
}