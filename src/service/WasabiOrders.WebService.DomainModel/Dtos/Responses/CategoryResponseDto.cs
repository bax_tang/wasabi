﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class CategoryResponseDto : IdentityResponseDto
	{
		#region Properties

		public string Name { get; set; }

		public string Description { get; set; }

		public int ProductsCount { get; set; }
		#endregion

		#region Constructors

		public CategoryResponseDto() : base() { }
		#endregion
	}
}