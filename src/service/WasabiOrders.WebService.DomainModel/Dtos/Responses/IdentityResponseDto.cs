﻿using System;

using Newtonsoft.Json;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class IdentityResponseDto
	{
		#region Properties

		[JsonProperty(Order = -100)]
		public Guid Id { get; set; }
		#endregion

		#region Constructors

		public IdentityResponseDto() { }

		public IdentityResponseDto(Guid id) : this()
		{
			Id = id;
		}
		#endregion
	}
}