﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class CommentResponseDto : IdentityResponseDto
	{
		#region Properties

		public Guid AuthorId { get; set; }

		public string AuthorName { get; set; }

		public Guid? RepliedTo { get; set; }

		public string Title { get; set; }

		public string Text { get; set; }

		public int ChildCount => ChildComments?.Count ?? 0;

		public List<CommentResponseDto> ChildComments { get; set; }

		public DateTime? CreatedAt { get; set; }

		public DateTime? UpdatedAt { get; set; }

		[JsonIgnore]
		public int Level { get; set; }
		#endregion

		#region Constructors

		public CommentResponseDto() : base() { }
		#endregion
	}
}