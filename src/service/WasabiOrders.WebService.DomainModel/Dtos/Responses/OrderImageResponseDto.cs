﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class OrderImageResponseDto
	{
		#region Properties

		public Guid Id { get; set; }

		public Guid FileId { get; set; }

		public string DownloadUrl { get; set; }
		#endregion

		#region Constructors

		public OrderImageResponseDto() { }
		#endregion
	}
}