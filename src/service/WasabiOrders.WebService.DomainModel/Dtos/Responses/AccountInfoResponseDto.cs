﻿using System;
using System.Security.Principal;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class AccountInfoResponseDto
	{
		#region Properties

		public string AccessToken { get; internal set; }

		public string TokenType { get; internal set; }

		public UserInfoResponseDto User { get; internal set; }
		#endregion

		#region Constructors

		public AccountInfoResponseDto() { }
		#endregion
	}
}