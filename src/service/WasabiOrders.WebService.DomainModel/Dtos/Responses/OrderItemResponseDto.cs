﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class OrderItemResponseDto
	{
		#region Properties

		public Guid Id { get; set; }

		public Guid ProductId { get; set; }

		public string ProductName { get; set; }

		public int Amount { get; set; }
		#endregion

		#region Constructors

		public OrderItemResponseDto() { }
		#endregion
	}
}