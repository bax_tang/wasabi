﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class ImageResponseDto
	{
		#region Properties

		public Guid Id { get; internal set; }

		public Guid BinaryId { get; internal set; }

		public string DownloadUrl { get; internal set; }
		#endregion

		#region Constructors

		public ImageResponseDto() { }
		#endregion
	}
}