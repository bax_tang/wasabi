﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	using DataModel;

	public class OrderResponseDto : IdentityResponseDto
	{
		#region Properties

		public string Name { get; set; }

		public string Author { get; set; }

		public DateTime OrderDateTime { get; set; }

		public DateTime? ExpectedDeliveryDateTime { get; set; }

		public DateTime? ActualDeliveryDateTime { get; set; }

		public OrderStatus Status { get; set; }

		public int? Rating { get; set; }

		public decimal? TotalPrice { get; set; }

		public decimal? DiscountedPrice { get; set; }
		#endregion

		#region Constructors

		public OrderResponseDto() : base() { }
		#endregion
	}
}