﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	using DataModel;

	public class OrderDetailedResponseDto : OrderResponseDto
	{
		#region Properties

		public Guid CreatedBy { get; set; }

		public List<OrderImageResponseDto> Images { get; set; }

		public List<OrderItemResponseDto> Items { get; set; }
		#endregion

		#region Constructors

		public OrderDetailedResponseDto() { }
		#endregion
	}
}