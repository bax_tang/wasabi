﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class FileResponseDto
	{
		#region Properties

		public string Name { get; set; }

		public string Type { get; set; }

		public long Size { get; set; }

		public string DownloadUrl { get; set; }
		#endregion

		#region Constructors

		public FileResponseDto() { }
		#endregion
	}
}