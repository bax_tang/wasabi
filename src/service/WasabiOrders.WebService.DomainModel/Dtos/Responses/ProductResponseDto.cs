﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
    public class ProductResponseDto : IdentityResponseDto
	{
        #region Properties

		public string Name { get; set; }

		public string Description { get; set; }

		public decimal Price { get; set; }

		public double Weight { get; set; }

		public int? Rating { get; set; }

		public Guid CategoryId { get; set; }

		public string CategoryName { get; set; }

		public Guid? ThumbnailId { get; set; }

		public string ThumbnailUrl { get; set; }
        #endregion

        #region Constructors

        public ProductResponseDto() : base() { }
        #endregion
    }
}