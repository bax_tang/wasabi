﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class UserInfoResponseDto
	{
		#region Properties

		public Guid Id { get; set; }

		public string AccountName { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }

		public double Discount { get; set; }
		#endregion

		#region Constructors

		public UserInfoResponseDto() { }
		#endregion
	}
}