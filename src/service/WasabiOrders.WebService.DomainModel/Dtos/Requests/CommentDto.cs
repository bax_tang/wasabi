﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class CommentDto
	{
		#region Properties

		public Guid ObjectId { get; set; }

		public string Title { get; set; }

		public string Text { get; set; }

		public Guid? RepliedTo { get; set; }
		#endregion

		#region Constructors

		public CommentDto() { }
		#endregion
	}
}