﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
    public class ProductDto
    {
        #region Properties

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public double Weight { get; set; }

        public int? Rating { get; set; }

        public Guid CategoryId { get; set; }
        #endregion

        #region Constructors

        public ProductDto() { }
        #endregion
    }
}