﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class AccountInfoDto
	{
		#region Properties

		public string UserName { get; set; }

		public string Password { get; set; }
		#endregion

		#region Constructors

		public AccountInfoDto() { }
		#endregion
	}
}