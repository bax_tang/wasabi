﻿using System;
using System.IO;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class FileDto
	{
		#region Properties

		public Stream Data { get; set; }

		public string Name { get; set; }

		public string Type { get; set; }
		#endregion

		#region Constructors

		public FileDto() { }
		#endregion
	}
}