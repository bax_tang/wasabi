﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public abstract class IdentityDto
	{
		#region Properties

		public Guid Id { get; set; }
		#endregion

		#region Constructors

		protected internal IdentityDto() { }
		#endregion
	}
}