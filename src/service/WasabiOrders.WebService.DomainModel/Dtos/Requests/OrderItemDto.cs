﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class OrderItemDto
	{
		#region Properties

		public Guid ProductId { get; set; }

		public int Amount { get; set; }
		#endregion

		#region Constructors

		public OrderItemDto() { }
		#endregion
	}
}