﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	public class CategoryDto
	{
		#region Properties

		public string Name { get; set; }

		public string Description { get; set; }
		#endregion

		#region Constructors

		public CategoryDto() { }
        #endregion

        #region object methods overriding

        public override string ToString() => string.Concat("Category \"", Name, "\"");
        #endregion
    }
}