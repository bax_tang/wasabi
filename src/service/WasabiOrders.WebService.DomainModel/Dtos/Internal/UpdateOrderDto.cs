﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
	internal class UpdateOrderDto
	{
		#region Properties

		public Guid AuthorID { get; private set; }

		public OrderDto Order { get; private set; }
		#endregion

		#region Constructors

		internal UpdateOrderDto(Guid authorID, OrderDto order)
		{
			AuthorID = authorID;
			Order = order;
		}
		#endregion
	}
}