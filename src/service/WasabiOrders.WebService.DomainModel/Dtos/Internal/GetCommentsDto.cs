﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
    internal class GetCommentsDto
    {
        #region Properties

        public Guid ObjectID { get; private set; }

        public Guid? RootCommentID { get; private set; }

        public int MaxLevel { get; private set; }
        #endregion

        #region Constructors

        internal GetCommentsDto(Guid objectID, Guid? rootCommentID, int maxLevel)
        {
            ObjectID = objectID;
            RootCommentID = rootCommentID;
            MaxLevel = maxLevel;
        }
        #endregion
    }
}