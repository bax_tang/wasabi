﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
    internal class UploadObjectImagesDto
    {
        #region Properties

        public Guid ObjectID { get; private set; }

        public List<FileDto> Images { get; private set; }
        #endregion

        #region Constructors

        internal UploadObjectImagesDto(Guid objectID, List<FileDto> images)
        {
            ObjectID = objectID;
            Images = images;
        }
        #endregion
    }
}