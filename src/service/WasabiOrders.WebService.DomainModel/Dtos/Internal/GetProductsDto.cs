﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
    internal class GetProductsDto
    {
        #region Properties

        public Guid? CategoryID { get; private set; }

        public PaginationContext Pagination { get; private set; }
        #endregion

        #region Constructors

        internal GetProductsDto(Guid? categoryID, PaginationContext pagination)
        {
            CategoryID = categoryID;
            Pagination = pagination;
        }
        #endregion
    }
}