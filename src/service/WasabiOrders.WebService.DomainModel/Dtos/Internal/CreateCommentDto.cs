﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
    internal class CreateCommentDto
    {
        #region Properties

        public Guid AuthorID { get; private set; }

        public CommentDto Comment { get; private set; }
        #endregion

        #region Constructors

        internal CreateCommentDto(Guid authorID, CommentDto comment)
        {
            AuthorID = authorID;
            Comment = comment;
        }
        #endregion
    }
}