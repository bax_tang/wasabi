﻿using System;

namespace WasabiOrders.WebService.DomainModel.Dtos
{
    internal class UpdateCommentDto
    {
        #region Properties

        public Guid AuthorID { get; private set; }

        public Guid CommentID { get; private set; }

        public CommentDto Comment { get; private set; }
        #endregion

        #region Constructors

        internal UpdateCommentDto(Guid authorID, Guid commentID, CommentDto comment)
        {
            AuthorID = authorID;
            CommentID = commentID;
            Comment = comment;
        }
        #endregion
    }
}