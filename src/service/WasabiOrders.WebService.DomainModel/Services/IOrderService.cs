﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services
{
	using Dtos;

	public interface IOrderService
	{
		#region Service methods

		Task<CollectionResult<OrderResponseDto>> GetAllAsync(PaginationContext paginationContext);

		Task<OperationResult<OrderDetailedResponseDto>> GetAsync(Guid orderID);

		Task<OperationResult<IdentityResponseDto>> CreateAsync(IIdentity currentUser, OrderDto orderDto);

		Task<OperationResult> UpdateAsync(IIdentity currentUser, OrderDto orderDto);

		Task<OperationResult<ImagesResponseDto>> UploadImagesAsync(IIdentity currentUser, Guid orderID, List<FileDto> imageDtos);
		#endregion
	}
}