﻿using System;
using System.Security.Principal;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services
{
	using Dtos;

	public interface IAccountService
	{
		Task<OperationResult<AccountInfoResponseDto>> LoginAsync(AccountInfoDto accountInfoDto);

		Task<OperationResult> LogoutAsync(IIdentity userIdentity);
	}
}