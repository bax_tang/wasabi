﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services.Impl
{
	using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;
	using Dtos;
	using Exceptions;
	using Providers;

	public class CommentService : DomainModelServiceBase<Guid, Comment>, ICommentService
	{
		#region Fields and properties

		private readonly ICommentRepository _commentRepository;

		private readonly IMappingProvider _mappingProvider;

		private readonly ISqlProvider _sqlProvider;
		#endregion

		#region Constructors

		public CommentService(IUnitOfWork unitOfWork,
			ICommentRepository commentRepository,
			IMappingProvider mappingProvider,
			ISqlProvider sqlProvider) : base(unitOfWork)
		{
			_commentRepository = commentRepository;
			_mappingProvider = mappingProvider;
			_sqlProvider = sqlProvider;
		}
		#endregion

		#region ICommentService implementation

		public async Task<CollectionResult<CommentResponseDto>> GetCommentsAsync(Guid objectID, Guid? rootCommentID, int maxLevel)
		{
            var result = await _unitOfWork.ReturnAsync(InternalGetCommentsAsync, new GetCommentsDto(objectID, rootCommentID, maxLevel));

            return result;
		}

		public async Task<OperationResult<IdentityResponseDto>> CreateCommentAsync(Guid authorID, CommentDto commentDto)
		{
            Comment newComment = await _unitOfWork.ExecuteAsync(InternalCreateAsync, new CreateCommentDto(authorID, commentDto));

			return OperationResult.Ok(new IdentityResponseDto(newComment.Id));
		}

		public async Task<OperationResult> UpdateCommentAsync(Guid authorID, Guid commentID, CommentDto commentDto)
		{
            await _unitOfWork.ExecuteAsync(InternalUpdateAsync, new UpdateCommentDto(authorID, commentID, commentDto));

			return OperationResult.Ok();
		}
		#endregion

		#region Private class methods

		private async Task ValidatePermissionsAsync(Guid authorID, CommentDto commentDto, Guid? commentID = null, bool forUpdate = false)
		{
			if (authorID == Guid.Empty)
			{
				throw MakeBadRequestException($"Author Id can't be equal to '{Guid.Empty}'.");
			}
			if (forUpdate)
			{
				if (commentID == Guid.Empty)
				{
					throw MakeBadRequestException($"Comment Id can't be equal to '{Guid.Empty}'.");
				}
			}
			if (commentDto == null)
			{
				throw MakeBadRequestException("Comment DTO can't be null.");
			}
			if (commentDto.ObjectId == Guid.Empty)
			{
				throw MakeBadRequestException($"Object Id can't be equal to '{Guid.Empty}'.");
			}
			if (commentDto.RepliedTo.HasValue)
			{
				bool parentCommentExists = await _commentRepository.ExistsAsync(commentDto.RepliedTo.Value);
				if (!parentCommentExists)
				{
					throw MakeBadRequestException($"Parent comment with Id '{commentDto.RepliedTo.Value}' does not exist.");
				}
			}
		}

        private async Task<CollectionResult<CommentResponseDto>> InternalGetCommentsAsync(GetCommentsDto getCommentsDto)
        {
            Guid objectID = getCommentsDto.ObjectID;
            Guid? rootCommentID = getCommentsDto.RootCommentID;
            int maxLevel = getCommentsDto.MaxLevel;

            var queryResult = await _sqlProvider.GetComments(objectID, rootCommentID, maxLevel);
            var commentsTree = BuildCommentsTree(queryResult);

            int totalCount = await _commentRepository.CountAsync(x => x.ObjectID == objectID);
            int pageSize = queryResult.Count;

            return CollectionResult.Create(commentsTree, 1, pageSize, 1, totalCount);
        }

        private async Task<Comment> InternalCreateAsync(CreateCommentDto createCommentDto)
        {
            Guid authorID = createCommentDto.AuthorID;
            CommentDto commentDto = createCommentDto.Comment;

            await ValidatePermissionsAsync(authorID, commentDto);

            Comment newComment = _mappingProvider.Map<CommentDto, Comment>(commentDto);
            newComment.AuthorID = authorID;

            _commentRepository.Add(newComment);

            return newComment;
        }

        private async Task<bool> InternalUpdateAsync(UpdateCommentDto updateCommentDto)
        {
            Guid authorID = updateCommentDto.AuthorID;
            Guid commentID = updateCommentDto.CommentID;
            CommentDto commentDto = updateCommentDto.Comment;

            await ValidatePermissionsAsync(authorID, commentDto, commentID, true);

            Comment commentToUpdate = await _commentRepository.FindAsync(commentID);
            if (commentToUpdate == null)
            {
                throw MakeNotFoundException(commentID);
            }
            if (commentToUpdate.AuthorID != authorID)
            {
                throw MakeNoPermissionsException(commentID);
            }

            _mappingProvider.Map(commentDto, commentToUpdate);
            return true;
        }

		private List<CommentResponseDto> BuildCommentsTree(List<CommentResponseDto> comments)
		{
			List<CommentResponseDto> commentsTree = new List<CommentResponseDto>(3);
			Dictionary<Guid, CommentResponseDto> commentsMap = new Dictionary<Guid, CommentResponseDto>(comments.Count);

			CommentResponseDto parentComment = null;

			foreach (CommentResponseDto comment in comments)
			{
				commentsMap[comment.Id] = comment;

				if (comment.Level == 0)
				{
					comment.ChildComments = new List<CommentResponseDto>();
					commentsTree.Add(comment);
				}
				else
				{
					Guid parentID = comment.RepliedTo.GetValueOrDefault(Guid.Empty);
					if ((parentID != Guid.Empty) && commentsMap.TryGetValue(parentID, out parentComment))
					{
						var childComments = parentComment.ChildComments;
						if (childComments == null)
						{
							childComments = new List<CommentResponseDto>();
							parentComment.ChildComments = childComments;
						}
						childComments.Add(comment);
					}
				}
			}

			return commentsTree;
		}
		#endregion
	}
}