﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services.Impl
{
	using DataModel;
    using DataModel.Entities;
    using DataModel.Repositories;
    using Dtos;
	using Exceptions;
	using Providers;

    public class ProductService : DomainModelServiceBase<Guid, Product>, IProductService
    {
		#region Constants, fields and properties

		private readonly IProductRepository _productRepository;

		private readonly IProductCategoryRepository _productCategoryRepository;

		private readonly IProductImageRepository _productImageRepository;

		private readonly IFileRepository _fileRepository;

		private readonly IActivityLogProvider _activityLogProvider;

		private readonly IFileUploadProvider _fileUploadProvider;

		private readonly IGlobalSettingProvider _globalSettingProvider;
        #endregion

        #region Constructors

        public ProductService(IUnitOfWork unitOfWork,
			IProductRepository productRepository,
            IProductCategoryRepository productCategoryRepository,
            IProductImageRepository productImageRepository,
            IFileRepository fileRepository,
			IActivityLogProvider activityLogProvider,
			IFileUploadProvider fileUploadProvider,
			IGlobalSettingProvider globalSettingProvider) : base(unitOfWork)
		{
			_productRepository = productRepository;
            _productCategoryRepository = productCategoryRepository;
			_productImageRepository = productImageRepository;
			_fileRepository = fileRepository;

			_activityLogProvider = activityLogProvider;
			_fileUploadProvider = fileUploadProvider;
			_globalSettingProvider = globalSettingProvider;
		}
        #endregion

        #region IProductsService implementation

        public async Task<OperationResult<IdentityResponseDto>> CreateAsync(IIdentity currentUser, ProductDto productDto)
        {
            Product newProduct = await _unitOfWork.ExecuteAsync(InternalCreateAsync, productDto);

			await _activityLogProvider.ProductCreatedAsync(currentUser, newProduct);

            return OperationResult.Ok(new IdentityResponseDto(newProduct.Id));
        }

		public async Task<CollectionResult<ProductResponseDto>> GetAllAsync(Guid? categoryID, PaginationContext paginationContext)
		{
            var result = await _unitOfWork.ReturnAsync(InternalGetAllAsync, new GetProductsDto(categoryID, paginationContext));

            return result;
		}
		
		public async Task<OperationResult<ProductDetailedResponseDto>> GetAsync(Guid productID)
        {
            var product = await _unitOfWork.ReturnAsync(InternalGetAsync, productID);

            return OperationResult.Ok(product);
        }

		public async Task<OperationResult<ImagesResponseDto>> UploadImagesAsync(IIdentity currentUser, Guid productID, List<FileDto> imageDtos)
		{
            var productImages = await _unitOfWork.ExecuteAsync(InternalUploadImagesAsync, new UploadObjectImagesDto(productID, imageDtos));

			foreach (var productImage in productImages)
			{
				await _activityLogProvider.ImageUploadedAsync(currentUser, productImage.ImageFile);
			}

            var result = CreateResultFromProductImages(productImages);
			return OperationResult.Ok(result);
		}
		#endregion

		#region Private class methods

        private async Task<Product> InternalCreateAsync(ProductDto productDto)
        {
            bool categoryExists = await _productCategoryRepository.ExistsAsync(productDto.CategoryId);
            if (!categoryExists)
            {
                throw MakeNotFoundException(productDto.CategoryId, default(ProductCategory));
            }
            if (string.IsNullOrWhiteSpace(productDto.Name))
            {
                throw MakeBadRequestException("Product name can't be null or empty.");
            }

            Product newProduct = _productRepository.Add(new Product
            {
                Name = productDto.Name,
                Description = productDto.Description,
                Price = productDto.Price,
                Weight = productDto.Weight,
                Rating = productDto.Rating,
                CategoryID = productDto.CategoryId
            });
            return newProduct;
        }

        private async Task<CollectionResult<ProductResponseDto>> InternalGetAllAsync(GetProductsDto getProductsDto)
        {
            Guid? categoryID = getProductsDto.CategoryID;
            PaginationContext paginationContext = getProductsDto.Pagination;

            var productsQuery = _productRepository.Query();
            var productCategoriesQuery = _productCategoryRepository.Query();
            var productImagesQuery = _productImageRepository.Query();
			var filesQuery = _fileRepository.Query();

            string orderExpression = paginationContext.GetOrderingExpression("p.", "Name ASC");

            var totalQuery = categoryID.HasValue ? productsQuery.Where(x => x.CategoryID == categoryID) : productsQuery;
			
            var resultQuery = (from p in totalQuery
                               join pi in productImagesQuery on new { p.Id, Type = ImageType.Thumbnail } equals new { Id = pi.ProductID, pi.Type } into pij
                               from pix in pij.DefaultIfEmpty()
							   join f in filesQuery on pix.FileID equals f.Id into fj
							   from fx in fj.DefaultIfEmpty()
                               join c in productCategoriesQuery on p.CategoryID equals c.Id
                               select new { p, fx, c })
                         .OrderBy(orderExpression)
                         .Skip(paginationContext.Offset)
                         .Take(paginationContext.PageSize)
                         .Select(x => new ProductResponseDto
                         {
                             Id = x.p.Id,
                             Name = x.p.Name,
                             Description = x.p.Description,
                             Price = x.p.Price,
                             Weight = x.p.Weight,
                             Rating = x.p.Rating,
                             CategoryId = x.c.Id,
                             CategoryName = x.c.Name,
							 ThumbnailId = (x.fx != null) ? x.fx.Id : (Guid?)null,
                             ThumbnailUrl = (x.fx != null) ? x.fx.ExternalFileUrl : null
                         });

            int totalCount = await totalQuery.CountAsync();
            int pagesCount = (int)Math.Ceiling(totalCount * 1.0 / paginationContext.PageSize);

            var productsList = await resultQuery.ToListAsync();

            return CollectionResult.Create(productsList, pagesCount, totalCount, paginationContext);
        }

        private async Task<ProductDetailedResponseDto> InternalGetAsync(Guid productID)
        {
            if (productID == Guid.Empty)
            {
                throw new BadRequestException($"Product ID can't be equal to '{Guid.Empty}'.");
            }

            Product product = await _productRepository.FindAsync(productID);
            if (product == null)
            {
                throw MakeNotFoundException(productID);
            }

            ProductDetailedResponseDto resultModel = new ProductDetailedResponseDto
            {
                // TODO: implement result model filling
            };
            return resultModel;
        }

        private async Task<IEnumerable<ProductImage>> InternalUploadImagesAsync(UploadObjectImagesDto uploadObjectImagesDto)
        {
            Guid productID = uploadObjectImagesDto.ObjectID;
            var imageDtos = uploadObjectImagesDto.Images;

            if (productID == Guid.Empty)
            {
                throw new BadRequestException($"Product ID can't be equal to '{Guid.Empty}'.");
            }
            if ((imageDtos == null) || (imageDtos.Count == 0))
            {
                throw new BadRequestException("Collection of product images can't be null or empty.");
            }

			Product product = await _productRepository.Query()
				.Include(x => x.Category)
				.Where(x => x.Id == productID)
				.FirstOrDefaultAsync();
			
            if (product == null)
            {
                throw MakeNotFoundException(productID);
            }

			string targetFolderID = await GetTargetFolderAsync(product);

			var images = await InternalUploadImagesAsync(targetFolderID, imageDtos);

            var productImages = CreateProductImagesFromImages(product, images);
            return productImages;
        }

		private async Task<string> GetTargetFolderAsync(Product product)
		{
			string folderID = null;

			string categoryFolderID = null;
			if (string.IsNullOrEmpty(product.Category.ExternalFolderID))
			{
				var configuration = await _globalSettingProvider.GetBoxConfigurationAsync();

				categoryFolderID = await _fileUploadProvider.CreateFolderAsync(configuration.BoxProductsFolder, product.Category.Name);
			}
			else
			{
				categoryFolderID = product.Category.ExternalFolderID;
			}

			if (string.IsNullOrEmpty(product.ExternalFolderID))
			{
				folderID = await _fileUploadProvider.CreateFolderAsync(categoryFolderID, product.Name);
				product.ExternalFolderID = folderID;
			}
			else
			{
				folderID = product.ExternalFolderID;
			}

			return folderID;
		}

		private async Task<IEnumerable<File>> InternalUploadImagesAsync(string parentFolderID, List<FileDto> imageDtos)
		{
			List<File> images = new List<File>(imageDtos.Count);

			foreach (FileDto imageDto in imageDtos)
			{
				string fileName = imageDto.Name;
				Stream fileData = imageDto.Data;
				long fileSize = fileData.Length;

				var externalFile = await _fileUploadProvider.UploadFileAsync(parentFolderID, fileName, fileData, true);
				images.Add(new File
				{
					Id = Guid.NewGuid(),
					Name = fileName,
					Size = fileSize,
					Type = imageDto.Type,
					ExternalFileID = externalFile.Id,
					ExternalFileUrl = externalFile.DownloadUrl
				});
			}

			return _fileRepository.AddRange(images);
		}

		private IEnumerable<ProductImage> CreateProductImagesFromImages(Product parentProduct, IEnumerable<File> images)
		{
			List<ProductImage> productImages = new List<ProductImage>();

			foreach (File imageFile in images)
			{
				productImages.Add(new ProductImage
				{
					Id = Guid.NewGuid(),
					ParentProduct = parentProduct,
					ImageFile = imageFile,
					Type = ImageType.Other
				});
			}

			return _productImageRepository.AddRange(productImages);
		}

		private ImagesResponseDto CreateResultFromProductImages(IEnumerable<ProductImage> productImages)
		{
			List<ImageResponseDto> createdImages = new List<ImageResponseDto>();

			foreach (ProductImage productImage in productImages)
			{
				createdImages.Add(new ImageResponseDto
				{
					Id = productImage.Id,
					BinaryId = productImage.FileID,
					DownloadUrl = productImage.ImageFile.ExternalFileUrl
				});
			}

			return new ImagesResponseDto
			{
				Images = createdImages
			};
		}
		#endregion
	}
}