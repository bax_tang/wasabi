﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services.Impl
{
    using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;
	using Dtos;

	public class FileService : DomainModelServiceBase<Guid, File>, IFileService
	{
		#region Fields and properties

		private IFileRepository _fileRepository;
		#endregion

		#region Constructors

		public FileService(IUnitOfWork unitOfWork, IFileRepository fileRepository) : base(unitOfWork)
		{
			if (fileRepository == null)
			{
				throw new ArgumentNullException(nameof(fileRepository), "Images repository can't be null.");
			}

			_fileRepository = fileRepository;
		}
		#endregion

		#region IImageService implementation

		public async Task<OperationResult> DeleteFileAsync(Guid fileID)
		{
			bool deleted = await _unitOfWork.ExecuteAsync(InternalDeleteFileAsync, fileID);
			if (deleted)
			{
				return OperationResult.Ok();
			}

			return OperationResult.Error($"Unable to delete file with ID '{fileID}'.");
		}

		public async Task<OperationResult<FileResponseDto>> GetFileAsync(Guid fileID)
		{
            FileResponseDto fileDto = await _unitOfWork.ReturnAsync(InternalGetFileAsync, fileID);
            if (fileDto != null)
            {
                return OperationResult.Ok(fileDto);
            }

            return OperationResult.NotFound<FileResponseDto>();
        }
        #endregion

        #region Private class methods

		private async Task<bool> InternalDeleteFileAsync(Guid fileID)
		{
			var existingFile = await _fileRepository.FindAsync(fileID);
			if (existingFile == null)
			{
				throw MakeNotFoundException(fileID);
			}

			_fileRepository.Remove(existingFile);
			return true;
		}

        private async Task<FileResponseDto> InternalGetFileAsync(Guid fileID)
        {
			var fileQuery = _fileRepository.Query()
				.Where(x => x.Id == fileID)
				.Select(x => new FileResponseDto
				{
					Name = x.Name,
					Type = x.Type,
					Size = x.Size,
					DownloadUrl = x.ExternalFileUrl
				});

			var file = await fileQuery.FirstOrDefaultAsync();
			return file;
        }
        #endregion
    }
}