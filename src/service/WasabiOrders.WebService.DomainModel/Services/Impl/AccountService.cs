﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading.Tasks;

using BlowfishCrypt = BCrypt.Net.BCrypt;

namespace WasabiOrders.WebService.DomainModel.Services.Impl
{
	using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;
	using Dtos;
	using Providers;

	public class AccountService : DomainModelServiceBase, IAccountService
	{
		#region Fields and properties

		private readonly IUserRepository _userRepository;

		private readonly IUserSessionRepository _userSessionRepository;

		private readonly IActivityLogProvider _activityLogProvider;

		private readonly IIdentityTokenProvider _identityTokenProvider;
		#endregion

		#region Constructors

		public AccountService(IUnitOfWork unitOfWork,
			IUserRepository userRepository,
			IUserSessionRepository userSessionRepository,
			IActivityLogProvider activityLogProvider,
			IIdentityTokenProvider identityTokenProvider) : base(unitOfWork)
		{
			_userRepository = userRepository;
			_userSessionRepository = userSessionRepository;

			_activityLogProvider = activityLogProvider;
			_identityTokenProvider = identityTokenProvider;
		}
		#endregion

		#region IAccountService implementation

		public async Task<OperationResult<AccountInfoResponseDto>> LoginAsync(AccountInfoDto accountInfoDto)
		{
			var userSession = await _unitOfWork.ExecuteAsync(InternalLoginAsync, accountInfoDto);
			if (userSession != null)
			{
				var identity = _identityTokenProvider.CreateIdentity(userSession);
				
				await _activityLogProvider.UserLoginAsync(identity);

				var accessToken = _identityTokenProvider.CreateAccessToken(identity, userSession.LoginDateTime);
				var accountInfo = CreateAccountInfo(userSession.User, userSession, accessToken);

				return OperationResult.Ok(accountInfo);
			}

			return MakeUserNotFoundResult(accountInfoDto.UserName);
		}

		public async Task<OperationResult> LogoutAsync(IIdentity userIdentity)
		{
			var logoutResult = await _unitOfWork.ExecuteAsync(InternalLogoutAsync, userIdentity);
			if (logoutResult)
			{
				await _activityLogProvider.UserLogoutAsync(userIdentity);
			}

			return OperationResult.Ok();
		}
		#endregion

		#region Private class methods

		private async Task<UserSession> InternalLoginAsync(AccountInfoDto accountInfoDto)
		{
			User user = await FindUserAsync(accountInfoDto.UserName, accountInfoDto.Password);
			if (user != null)
			{
				UserSession userSession = _userSessionRepository.Add(new UserSession
				{
					Id = Guid.NewGuid(),
					User = user,
					LoginDateTime = DateTime.UtcNow
				});

				return userSession;
			}

			return null;
		}

		private async Task<bool> InternalLogoutAsync(IIdentity userIdentity)
		{
			Guid sessionID = userIdentity.GetSessionID();

			UserSession userSession = await _userSessionRepository.FindAsync(sessionID);
			if (userSession != null)
			{
				_userSessionRepository.Remove(userSession);
			}

			return (userSession != null);
		}

		private async Task<User> FindUserAsync(string userName, string password)
		{
			var userByName = await _userRepository.Query()
				.FirstOrDefaultAsync(x => x.AccountName == userName);

			if (userByName != null && BlowfishCrypt.Verify(password, userByName.PasswordHash))
			{
				return userByName;
			}

			return null;
		}

		private AccountInfoResponseDto CreateAccountInfo(User user, UserSession session, string accessToken)
		{
			var userInfo = new UserInfoResponseDto
			{
				Id = user.Id,
				AccountName = user.AccountName,
				FirstName = user.FirstName,
				LastName = user.LastName,
				Email = user.Email,
				Discount = user.Discount
			};

			var accountInfo = new AccountInfoResponseDto
			{
				AccessToken = accessToken,
				TokenType = "bearer",
				User = userInfo
			};
			return accountInfo;
		}

		private OperationResult<AccountInfoResponseDto> MakeUserNotFoundResult(string userName)
		{
			string message = $"User with account name '{userName}' does not exist.";

			return new OperationResult<AccountInfoResponseDto>(message)
			{
				StatusCode = HttpStatusCode.BadRequest
			};
		}
		#endregion
	}
}