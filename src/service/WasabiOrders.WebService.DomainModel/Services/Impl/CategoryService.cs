﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services.Impl
{
	using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;
	using Dtos;
	using Exceptions;
	using Providers;
	using Resources;

	public class CategoryService : DomainModelServiceBase<Guid, ProductCategory>, ICategoryService
	{
		#region Fields and properties

		private IProductCategoryRepository _categoryRepository;

        private IProductRepository _productRepository;

		private ISqlProvider _sqlProvider;
		#endregion

		#region Constructors

		public CategoryService(IUnitOfWork unitOfWork,
            IProductCategoryRepository categoryRepository,
            IProductRepository productRepository,
			ISqlProvider sqlProvider) : base(unitOfWork)
		{
			_categoryRepository = categoryRepository;
            _productRepository = productRepository;
			_sqlProvider = sqlProvider;
		}
		#endregion

		#region ICategoryService implementation

        public async Task<OperationResult<IdentityResponseDto>> CreateAsync(CategoryDto categoryDto)
        {
            ProductCategory newCategory = await _unitOfWork.ExecuteAsync(InternalCreateAsync, categoryDto);

            return OperationResult.Ok(new IdentityResponseDto(newCategory.Id));
        }

        public async Task<CollectionResult<CategoryResponseDto>> GetAllAsync()
        {
            var categoriesList = await _unitOfWork.ReturnAsync(_sqlProvider.GetAllCategories);
            
            int count = categoriesList.Count;
            return CollectionResult.Create(categoriesList, 1, count, 1, count);
        }

        public async Task<OperationResult<CategoryResponseDto>> GetAsync(Guid categoryID)
        {
            // TODO: implement extracting detailed info about category

            return OperationResult.Ok(new CategoryResponseDto());
        }

        public async Task<OperationResult> DeleteAsync(Guid categoryID)
        {
            await _unitOfWork.ExecuteAsync(InternalDeleteAsync, categoryID);

            return OperationResult.Ok();
        }
        #endregion

        #region Private class methods

        private ProductCategory InternalCreateAsync(CategoryDto categoryDto)
        {
            if (categoryDto == null)
            {
                throw MakeBadRequestException(BadRequestErrors.CategoryDtoCannotBeNull);
            }
            if (string.IsNullOrWhiteSpace(categoryDto.Name))
            {
                throw MakeBadRequestException(BadRequestErrors.CategoryNameCannotBeNull);
            }

            ProductCategory newCategory = _categoryRepository.Add(new ProductCategory
            {
                Name = categoryDto.Name,
                Description = categoryDto.Description
            });

            return newCategory;
        }

        private async Task<bool> InternalDeleteAsync(Guid categoryID)
        {
            if (categoryID == Guid.Empty)
            {
                throw MakeBadRequestException($"Category ID cannot be equal to '{Guid.Empty}'.");
            }
            if (await _productRepository.Query().AnyAsync(x => x.CategoryID == categoryID))
            {
                throw MakeBusinessLogicException($"Cannot delete non-empty category.");
            }

            ProductCategory category = await _categoryRepository.FindAsync(categoryID);
            if (category == null)
            {
                throw MakeNotFoundException(categoryID, "Category");
            }

            _categoryRepository.Remove(category);
            return true;
        }
        #endregion
    }
}