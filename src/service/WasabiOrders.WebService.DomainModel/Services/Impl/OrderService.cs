﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services.Impl
{
    using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;
	using Dtos;
	using Exceptions;
	using Providers;

	public class OrderService : DomainModelServiceBase<Guid, Order>, IOrderService
	{
		#region Fields and properties

		private readonly IUserRepository _userRepository;

		private readonly IFileRepository _fileRepository;

		private readonly IOrderRepository _orderRepository;

		private readonly IOrderImageRepository _orderImageRepository;

		private readonly IOrderItemRepository _orderItemRepository;

		private readonly IProductRepository _productRepository;

		private readonly IActivityLogProvider _activityLogProvider;

		private readonly IGlobalSettingProvider _globalSettingProvider;

		private readonly IFileUploadProvider _fileUploadProvider;

		private readonly IMappingProvider _mappingProvider;
		#endregion

		#region Constructors

		public OrderService(IUnitOfWork unitOfWork,
            IUserRepository userRepository,
			IFileRepository fileRepository,
			IOrderRepository orderRepository,
			IOrderImageRepository orderImageRepository,
			IOrderItemRepository orderItemRepository,
			IProductRepository productRepository,
			IActivityLogProvider activityLogProvider,
			IGlobalSettingProvider globalSettingProvider,
			IFileUploadProvider fileUploadProvider,
			IMappingProvider mappingProvider) : base(unitOfWork)
		{
			_userRepository = userRepository;
			_fileRepository = fileRepository;
			_orderRepository = orderRepository;
			_orderImageRepository = orderImageRepository;
			_orderItemRepository = orderItemRepository;
			_productRepository = productRepository;

			_activityLogProvider = activityLogProvider;
			_globalSettingProvider = globalSettingProvider;
			_fileUploadProvider = fileUploadProvider;
			_mappingProvider = mappingProvider;
		}
		#endregion

		#region IOrdersService implementation

		public async Task<CollectionResult<OrderResponseDto>> GetAllAsync(PaginationContext paginationContext)
		{
            return await _unitOfWork.ReturnAsync(InternalGetAllAsync, paginationContext);
		}

		public async Task<OperationResult<OrderDetailedResponseDto>> GetAsync(Guid orderID)
		{
            var resultModel = await _unitOfWork.ReturnAsync(InternalGetAsync, orderID);

            return OperationResult.Ok(resultModel);
		}

		public async Task<OperationResult<IdentityResponseDto>> CreateAsync(IIdentity currentUser, OrderDto orderDto)
		{
			Guid authorID = currentUser.GetUserID();
            Order newOrder = await _unitOfWork.ExecuteAsync(InternalCreateAsync, new CreateOrderDto(authorID, orderDto));

			await _activityLogProvider.OrderCreatedAsync(currentUser, newOrder);

			return OperationResult.Ok(new IdentityResponseDto(newOrder.Id));
		}

		public async Task<OperationResult> UpdateAsync(IIdentity currentUser, OrderDto orderDto)
		{
			Guid authorID = currentUser.GetUserID();
			Order updatedOrder = await _unitOfWork.ExecuteAsync(InternalUpdateAsync, new UpdateOrderDto(authorID, orderDto));

			await _activityLogProvider.OrderUpdatedAsync(currentUser, updatedOrder);

			return OperationResult.Ok();
		}

		public async Task<OperationResult<ImagesResponseDto>> UploadImagesAsync(IIdentity currentUser, Guid orderID, List<FileDto> imageDtos)
		{
            var orderImages = await _unitOfWork.ExecuteAsync(InternalUploadImagesAsync, new UploadObjectImagesDto(orderID, imageDtos));

			foreach (var orderImage in orderImages)
			{
				await _activityLogProvider.ImageUploadedAsync(currentUser, orderImage.ImageFile);
			}

            var result = CreateResultFromOrderImages(orderImages);
			return OperationResult.Ok(result);
		}
		#endregion

		#region Private class methods

        private async Task<CollectionResult<OrderResponseDto>> InternalGetAllAsync(PaginationContext paginationContext)
        {
            string orderingExpression = paginationContext.GetOrderingExpression("", "OrderDateTime DESC");

            int totalCount = await _orderRepository.CountAsync();
            int pagesCount = (int)Math.Ceiling(totalCount * 1.0 / paginationContext.PageSize);

            var ordersQuery = _orderRepository.Query()
                .Include(x => x.Author)
                .OrderBy(orderingExpression)
                .Skip(paginationContext.Offset)
                .Take(paginationContext.PageSize)
                .Select(x => new OrderResponseDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Author = (x.Author.FirstName + " " + x.Author.LastName),
                    OrderDateTime = x.OrderDateTime,
                    ExpectedDeliveryDateTime = x.ExpectedDeliveryDateTime,
                    ActualDeliveryDateTime = x.ActualDeliveryDateTime,
                    Status = x.Status,
                    Rating = x.Rating,
                    TotalPrice = x.TotalPrice,
                    DiscountedPrice = x.DiscountedPrice
                });

            var ordersList = await ordersQuery.ToListAsync();

            return CollectionResult.Create(ordersList, pagesCount, totalCount, paginationContext);
        }

        private async Task<OrderDetailedResponseDto> InternalGetAsync(Guid orderID)
        {
			var orderQuery = _orderRepository.Query()
				.Include(x => x.Author)
				.Where(x => x.Id == orderID)
				.Select(x => new OrderDetailedResponseDto
				{
					Id = x.Id,
					Author = x.Author.FirstName + " " + x.Author.LastName,
					CreatedBy = x.AuthorID,
					Name = x.Name,
					OrderDateTime = x.OrderDateTime,
					ExpectedDeliveryDateTime = x.ExpectedDeliveryDateTime,
					ActualDeliveryDateTime = x.ActualDeliveryDateTime,
					Rating = x.Rating,
					Status = x.Status,
					TotalPrice = x.TotalPrice,
					DiscountedPrice = x.DiscountedPrice
				});

			var orderResultModel = await orderQuery.FirstOrDefaultAsync();
			if (orderResultModel == null)
            {
                throw MakeNotFoundException(orderID);
            }

			orderResultModel.Images = await GetOrderImagesAsync(orderID);
			orderResultModel.Items = await GetOrderItemsAsync(orderID);

			return orderResultModel;
        }

		private Task<List<OrderImageResponseDto>> GetOrderImagesAsync(Guid orderID)
		{
			var orderImagesQuery = _orderImageRepository.Query()
				.Include(x => x.ImageFile)
				.Where(x => x.OrderID == orderID)
				.Select(x => new OrderImageResponseDto
				{
					Id = x.Id,
					FileId = x.FileID,
					DownloadUrl = x.ImageFile.ExternalFileUrl
				});

			return orderImagesQuery.ToListAsync();
		}

		private Task<List<OrderItemResponseDto>> GetOrderItemsAsync(Guid orderID)
		{
			var orderItemsQuery = _orderItemRepository.Query()
				.Include(x => x.RelatedProduct)
				.Where(x => x.OrderID == orderID)
				.Select(x => new OrderItemResponseDto
				{
					Id = x.Id,
					ProductId = x.ProductID,
					ProductName = x.RelatedProduct.Name,
					Amount = x.Amount
				});

			return orderItemsQuery.ToListAsync();
		}

        private async Task<Order> InternalCreateAsync(CreateOrderDto createOrderDto)
        {
            Guid authorID = createOrderDto.AuthorID;
            OrderDto orderDto = createOrderDto.Order;

            if (authorID == Guid.Empty)
            {
                throw MakeBadRequestException($"Author ID can't be equal to '{Guid.Empty}'.");
            }
            if (orderDto == null)
            {
                throw MakeBadRequestException("Order DTO can't be null.");
            }

            await CheckProductsExists(orderDto.Items);

			Order newOrder = _mappingProvider.Map<OrderDto, Order>(orderDto);
			newOrder.AuthorID = authorID;

            var newOrderItems = CreateOrderItems(newOrder, orderDto.Items);
			
            return newOrder;
		}

		private async Task<Order> InternalUpdateAsync(UpdateOrderDto updateOrderDto)
		{
			Guid authorID = updateOrderDto.AuthorID;
			OrderDto orderDto = updateOrderDto.Order;

			if (authorID == Guid.Empty)
			{
				throw MakeBadRequestException($"Author ID can't be equal to '{Guid.Empty}'.");
			}
			if (orderDto == null)
			{
				throw MakeBadRequestException("Order DTO can't be null.");
			}

			Order existingOrder = await _orderRepository.FindAsync(orderDto.Id);
			if (existingOrder == null)
			{
				throw MakeNotFoundException(orderDto.Id);
			}
			if (existingOrder.AuthorID != authorID)
			{
				throw MakeNoPermissionsException(orderDto.Id);
			}

			return _mappingProvider.Map(orderDto, existingOrder);
		}


		private async Task<IEnumerable<OrderImage>> InternalUploadImagesAsync(UploadObjectImagesDto uploadObjectImagesDto)
        {
            Guid orderID = uploadObjectImagesDto.ObjectID;
            var imageDtos = uploadObjectImagesDto.Images;

            if (orderID == Guid.Empty)
            {
                throw MakeBadRequestException($"Product ID can't be equal to '{Guid.Empty}'.");
            }
            if ((imageDtos == null) || (imageDtos.Count == 0))
            {
                throw MakeBadRequestException("Collection of product images can't be null or empty.");
            }

			Order order = await _orderRepository.FindAsync(orderID);
			if (order == null)
            {
                throw MakeNotFoundException(orderID);
            }

			string targetFolderID = await GetTargetFolderAsync(order);

			var images = await InternalUploadImagesAsync(targetFolderID, imageDtos);

            var orderImages = CreateOrderImagesFromImages(order, images);
            return orderImages;
        }

		private async Task<string> GetTargetFolderAsync(Order order)
		{
			string folderID = null;

			if (string.IsNullOrEmpty(order.ExternalFolderID))
			{
				var configuration = await _globalSettingProvider.GetBoxConfigurationAsync();

				folderID = await _fileUploadProvider.CreateFolderAsync(configuration.BoxOrdersFolder, order.Name);
				order.ExternalFolderID = folderID;
			}
			else
			{
				folderID = order.ExternalFolderID;
			}

			return folderID;
		}

		private async Task<IEnumerable<File>> InternalUploadImagesAsync(string parentFolderID, List<FileDto> imageDtos)
		{
			List<File> images = new List<File>(imageDtos.Count);

			foreach (FileDto imageDto in imageDtos)
			{
				string fileName = imageDto.Name;
				Stream fileData = imageDto.Data;
				long fileSize = fileData.Length;

				var externalFile = await _fileUploadProvider.UploadFileAsync(parentFolderID, fileName, fileData, true);
				images.Add(new File
				{
					Id = Guid.NewGuid(),
					Name = fileName,
					Size = fileSize,
					Type = imageDto.Type,
					ExternalFileID = externalFile.Id,
					ExternalFileUrl = externalFile.DownloadUrl
				});
			}

			return _fileRepository.AddRange(images);
		}

        private async Task CheckProductsExists(List<OrderItemDto> orderItems)
		{
			foreach (OrderItemDto item in orderItems)
			{
				bool productExists = await _productRepository.ExistsAsync(item.ProductId);
				if (!productExists)
				{
					throw MakeNotFoundException(item.ProductId, default(Product));
				}
			}
		}
		
		private Order CreateOrder(Guid authorID, OrderDto orderDto)
		{
			Order newOrder = new Order
			{
				AuthorID = authorID,
				Name = orderDto.Name,
				Status = orderDto.Status,
				Rating = orderDto.Rating,
				OrderDateTime = orderDto.OrderDateTime,
				ExpectedDeliveryDateTime = orderDto.ExpectedDeliveryDateTime,
				ActualDeliveryDateTime = orderDto.ActualDeliveryDateTime,
				TotalPrice = orderDto.TotalPrice,
				DiscountedPrice = orderDto.DiscountedPrice
			};

			return _orderRepository.Add(newOrder);
		}

		private IEnumerable<OrderItem> CreateOrderItems(Order newOrder, List<OrderItemDto> orderItems)
		{
			List<OrderItem> newItems = new List<OrderItem>(orderItems.Count);

			foreach (OrderItemDto item in orderItems)
			{
				newItems.Add(new OrderItem
				{
					Id = Guid.NewGuid(),
					ParentOrder = newOrder,
					ProductID = item.ProductId,
					Amount = item.Amount
				});
			}

			return _orderItemRepository.AddRange(newItems);
		}

		private IEnumerable<File> CreateImagesFromDtos(List<FileDto> imageDtos)
		{
			List<File> images = new List<File>(imageDtos.Count);

			foreach (FileDto imageDto in imageDtos)
			{
				images.Add(new File
				{
					Name = imageDto.Name,
					Type = imageDto.Type
				});
			}

			return _fileRepository.AddRange(images);
		}

		private IEnumerable<OrderImage> CreateOrderImagesFromImages(Order parentOrder, IEnumerable<File> images)
		{
			List<OrderImage> orderImages = new List<OrderImage>();

			foreach (File imageData in images)
			{
				orderImages.Add(new OrderImage
				{
					Id = Guid.NewGuid(),
					ParentOrder = parentOrder,
					ImageFile = imageData
				});
			}

			return _orderImageRepository.AddRange(orderImages);
		}

		private ImagesResponseDto CreateResultFromOrderImages(IEnumerable<OrderImage> orderImages)
		{
			List<ImageResponseDto> createdImages = new List<ImageResponseDto>();

			foreach (OrderImage orderImage in orderImages)
			{
				createdImages.Add(new ImageResponseDto
				{
					Id = orderImage.Id,
					BinaryId = orderImage.FileID,
					DownloadUrl = orderImage.ImageFile.ExternalFileUrl
				});
			}

			return new ImagesResponseDto
			{
				Images = createdImages
			};
		}
		#endregion
	}
}