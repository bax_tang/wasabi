﻿using System;

namespace WasabiOrders.WebService.DomainModel.Services
{
	using DataModel;
	using DataModel.Entities;
	using Exceptions;

	public class DomainModelServiceBase<TKey, TEntity> : DomainModelServiceBase where TKey : struct where TEntity : KeyedEntity<TKey>
	{
		#region Constructors

		protected internal DomainModelServiceBase(IUnitOfWork unitOfWork) : base(unitOfWork) { }
		#endregion

		#region Protected class methods

		protected BadRequestException MakeBadRequestException(string message) => new BadRequestException(message);

		protected BusinessLogicException MakeBusinessLogicException(string message) => new BusinessLogicException(message);

		protected BusinessLogicException MakeNoPermissionsException(TKey entityId)
		{
			string identifier = entityId.ToString();
			string message = $"You do not have permissions to {typeof(TEntity).Name} with ID '{identifier}'.";

			return new BusinessLogicException(message);
		}

		protected NotFoundException MakeNotFoundException(TKey entityId, string entityName = null)
		{
			string identifier = entityId.ToString();
			string message = $"{entityName ?? typeof(TEntity).Name} with ID '{identifier}' does not exist.";

			return new NotFoundException(message);
		}
		#endregion
	}
}