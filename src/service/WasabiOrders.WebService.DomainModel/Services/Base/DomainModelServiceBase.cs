﻿using System;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services
{
    using DataModel;
	using DataModel.Entities;
	using Exceptions;

	public abstract class DomainModelServiceBase
	{
        #region Fields and properties

        protected readonly IUnitOfWork _unitOfWork;
		#endregion

		#region Constructors

        protected internal DomainModelServiceBase(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
            {
                throw new ArgumentNullException(nameof(unitOfWork), "Unit of work can't be null.");
            }

            _unitOfWork = unitOfWork;
        }
		#endregion

		#region Protected class methods

		protected NotFoundException MakeNotFoundException<TKey, TEntity>(TKey entityId, TEntity entity = default(TEntity))
			where TKey : struct
			where TEntity : KeyedEntity<TKey>
		{
			string identifier = entityId.ToString();
			string message = $"{typeof(TEntity).Name} with ID '{identifier}' does not exist.";

			return new NotFoundException(message);
		}
		#endregion
	}
}