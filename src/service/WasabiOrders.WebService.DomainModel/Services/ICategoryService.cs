﻿using System;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services
{
	using Dtos;

	public interface ICategoryService
	{

        Task<OperationResult<IdentityResponseDto>> CreateAsync(CategoryDto categoryDto);

		Task<CollectionResult<CategoryResponseDto>> GetAllAsync();

        Task<OperationResult<CategoryResponseDto>> GetAsync(Guid categoryID);

        Task<OperationResult> DeleteAsync(Guid categoryID);
	}
}