﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services
{
	using Dtos;

	public interface IProductService
    {

        Task<OperationResult<IdentityResponseDto>> CreateAsync(IIdentity currentUser, ProductDto productDto);

		Task<CollectionResult<ProductResponseDto>> GetAllAsync(Guid? categoryID, PaginationContext paginationContext);

        Task<OperationResult<ProductDetailedResponseDto>> GetAsync(Guid productID);

        Task<OperationResult<ImagesResponseDto>> UploadImagesAsync(IIdentity currentUser, Guid productID, List<FileDto> imageDtos);
	}
}