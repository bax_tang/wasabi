﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services
{
	using Dtos;

	public interface ICommentService
	{
		Task<CollectionResult<CommentResponseDto>> GetCommentsAsync(Guid objectID, Guid? rootCommentID = null, int maxLevel = 0);

		Task<OperationResult<IdentityResponseDto>> CreateCommentAsync(Guid authorID, CommentDto commentDto);

		Task<OperationResult> UpdateCommentAsync(Guid authorID, Guid commentID, CommentDto commentDto);
	}
}