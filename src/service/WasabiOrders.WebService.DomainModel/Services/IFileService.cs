﻿using System;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Services
{
	using Dtos;

	public interface IFileService
	{
		Task<OperationResult> DeleteFileAsync(Guid fileID);

		Task<OperationResult<FileResponseDto>> GetFileAsync(Guid fileID);
	}
}