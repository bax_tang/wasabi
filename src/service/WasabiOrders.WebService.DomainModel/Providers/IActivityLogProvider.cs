﻿using System;
using System.Data.Entity;
using System.Security.Principal;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Providers
{
	using DataModel.Entities;

	public interface IActivityLogProvider
	{

		Task UserLoginAsync(IIdentity currentUser);

		Task UserLogoutAsync(IIdentity currentUser);

		Task ImageUploadedAsync(IIdentity currentUser, File uploadedImage);

		Task ProductCreatedAsync(IIdentity currentUser, Product createdProduct);

		Task ProductUpdatedAsync(IIdentity currentUser, Product updatedProduct);

		Task OrderCreatedAsync(IIdentity currentUser, Order createdOrder);

		Task OrderUpdatedAsync(IIdentity currentUser, Order updatedOrder);
	}
}