﻿using System;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Providers
{
	using Models;

	public interface IGlobalSettingProvider
	{
		Task<BoxConfiguration> GetBoxConfigurationAsync();
	}
}