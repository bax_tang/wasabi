﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Providers
{
	using Models;

	public interface IFileUploadProvider
	{
		Task<string> SearchFolderAsync(string parentFolderID, string folderName);

		Task<string> CreateFolderAsync(string parentFolderID, string folderName);

		Task<bool> DeleteFolderAsync(string folderID, bool recursive = false);

		Task<ExternalFile> UploadFileAsync(string parentFolderID, string name, Stream data, bool getDownloadUrl = true);

		Task<bool> DeleteFileAsync(string fileID);
	}
}