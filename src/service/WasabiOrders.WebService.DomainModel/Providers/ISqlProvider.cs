﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Providers
{
	using Dtos;

	public interface ISqlProvider
	{
		Task<List<CategoryResponseDto>> GetAllCategories();

		Task<List<CommentResponseDto>> GetComments(Guid objectID, Guid? rootCommentID = null, int maxLevel = 0);
	}
}