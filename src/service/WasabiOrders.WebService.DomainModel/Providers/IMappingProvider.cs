﻿using System;

namespace WasabiOrders.WebService.DomainModel.Providers
{
	public interface IMappingProvider
	{
		
		TDestination Map<TSource, TDestination>(TSource source);

		TDestination Map<TSource, TDestination>(TSource source, TDestination destination);
	}
}