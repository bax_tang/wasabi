﻿using System;
using System.Security.Claims;

namespace WasabiOrders.WebService.DomainModel.Providers
{
	using DataModel.Entities;

	public interface IIdentityTokenProvider
	{
		ClaimsIdentity CreateIdentity(UserSession userSession);

		string CreateAccessToken(ClaimsIdentity userIdentity, DateTime issuedAt);

		ClaimsIdentity CreateIdentityFromToken(string accessToken);
	}
}