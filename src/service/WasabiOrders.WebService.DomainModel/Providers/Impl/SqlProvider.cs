﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Providers.Impl
{
	using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;
	using Dtos;

	public class SqlProvider : ISqlProvider
	{
		#region Fields and properties

		private readonly ISqlRepository SqlRepository;
		#endregion

		#region Constructors

		public SqlProvider(ISqlRepository sqlRepository)
		{
			SqlRepository = sqlRepository;
		}
		#endregion

		#region ISqlService implementation

		public async Task<List<CategoryResponseDto>> GetAllCategories()
		{
			var query = SqlRepository
                .StoredProcedure<CategoryResponseDto>(SqlProcedureNames.GetAllCategories)
                .Execute();

			var queryResult = await query.ToListAsync();
            return queryResult;
		}

		public async Task<List<CommentResponseDto>> GetComments(Guid objectID, Guid? rootCommentID = null, int maxLevel = 0)
		{
			var query = SqlRepository
                .StoredProcedure<CommentResponseDto>(SqlProcedureNames.GetComments)
				.AddParameter("@ObjectID", objectID)
				.AddParameter("@RootCommentID", rootCommentID)
				.AddParameter("@MaxLevel", maxLevel)
                .Execute();

			var queryResult = await query.ToListAsync();
			return queryResult;
		}
		#endregion
	}
}