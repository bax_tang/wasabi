﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Providers.Impl
{
	using System.Security.Principal;
	using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;

	public class ActivityLogProvider : IActivityLogProvider
	{
		#region Fields and properties

		private readonly ISqlRepository _sqlRepository;
		#endregion

		#region Constructors

		public ActivityLogProvider(ISqlRepository sqlRepository)
		{
			_sqlRepository = sqlRepository;
		}
		#endregion

		#region IActivityLogProvider implementation

		public Task UserLoginAsync(IIdentity currentUser)
		{
			return InternalExecuteAsync(ActivityTypeNames.UserLogin, currentUser);
		}

		public Task UserLogoutAsync(IIdentity currentUser)
		{
			return InternalExecuteAsync(ActivityTypeNames.UserLogout, currentUser);
		}

		public Task ImageUploadedAsync(IIdentity currentUser, File uploadedImage)
		{
			return InternalExecuteAsync(ActivityTypeNames.ImageUploaded, currentUser, uploadedImage.Id);
		}

		public Task ProductCreatedAsync(IIdentity currentUser, Product createdProduct)
		{
			return InternalExecuteAsync(ActivityTypeNames.ProductCreated, currentUser, createdProduct.Id);
		}

		public Task ProductUpdatedAsync(IIdentity currentUser, Product updatedProduct)
		{
			return InternalExecuteAsync(ActivityTypeNames.ProductUpdated, currentUser, updatedProduct.Id);
		}

		public Task OrderCreatedAsync(IIdentity currentUser, Order createdOrder)
		{
			return InternalExecuteAsync(ActivityTypeNames.OrderCreated, currentUser, createdOrder.Id);
		}

		public Task OrderUpdatedAsync(IIdentity currentUser, Order updatedOrder)
		{
			return InternalExecuteAsync(ActivityTypeNames.OrderUpdated, currentUser, updatedOrder.Id);
		}
		#endregion

		#region Private class methods

		private Task<int> InternalExecuteAsync(string activityType, Guid userID, Guid? objectID = null)
		{
			SqlQueryable procedure = _sqlRepository.StoredProcedure(SqlProcedureNames.CreateActivityLogRecord);

			procedure
				.AddParameter("@ActivityType", activityType)
				.AddParameter("@UserID", userID)
				.AddParameter("@ObjectID", objectID);

			return procedure.ExecuteAsync();
		}

		private Task<int> InternalExecuteAsync(string activityType, IIdentity currentUser, Guid? objectID = null)
		{
			SqlQueryable procedure = _sqlRepository.StoredProcedure(SqlProcedureNames.CreateActivityLogRecord);

			procedure
				.AddParameter("@ActivityType", activityType)
				.AddParameter("@UserID", currentUser.GetUserID())
				.AddParameter("@ObjectID", objectID);

			return procedure.ExecuteAsync();
		}
		#endregion
	}
}