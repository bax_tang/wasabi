﻿using System;

using AutoMapper;

namespace WasabiOrders.WebService.DomainModel.Providers.Impl
{
	public class AutoMapperMappingProvider : IMappingProvider
	{
		#region Fields and properties

		private readonly IMapper Mapper;
		#endregion

		#region Constructors

		public AutoMapperMappingProvider(IMapper mapper)
		{
			if (mapper == null)
			{
				throw new ArgumentNullException(nameof(mapper), "Mapper can't be null.");
			}

			Mapper = mapper;
		}
		#endregion

		#region IMappingProvider implementation

		public TDestination Map<TSource, TDestination>(TSource source)
		{
			return Mapper.Map<TSource, TDestination>(source);
		}

		public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
		{
			return Mapper.Map(source, destination);
		}
		#endregion
	}
}