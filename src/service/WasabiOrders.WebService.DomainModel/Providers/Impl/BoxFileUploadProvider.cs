﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Box.V2;
using Box.V2.Config;
using Box.V2.JWTAuth;
using Box.V2.Managers;
using Box.V2.Models;

namespace WasabiOrders.WebService.DomainModel.Providers.Impl
{
	using Models;

	public class BoxFileUploadProvider : IFileUploadProvider
	{
		#region Constants, fields and properties

		private const double MinutesBeforeTokenRefreshing = 5.0;

		private readonly IGlobalSettingProvider _globalSettingProvider;

		private DateTime _nextAuthorizationTime;

		private BoxClient _boxClient;

		private bool IsAuthorizationRequired =>
			(_boxClient == null) ||
			(_nextAuthorizationTime - DateTime.UtcNow).TotalMinutes <= MinutesBeforeTokenRefreshing;
		#endregion

		#region Constructors

		public BoxFileUploadProvider(IGlobalSettingProvider globalSettingProvider)
		{
			_globalSettingProvider = globalSettingProvider;
		}
		#endregion

		#region IFileUploadProvider implementation

		public async Task<string> SearchFolderAsync(string parentFolderID, string folderName)
		{
			if (IsAuthorizationRequired)
			{
				await AuthorizeAsync();
			}

			folderName = FixupFolderNameForBox(folderName);

			var searchResults = await _boxClient.SearchManager.SearchAsync(
				ancestorFolderIds: new[] { parentFolderID },
				keyword: folderName,
				fields: new[] { "id" },
				scope: "user_content",
				type: "folder");

			if (searchResults != null && searchResults.TotalCount > 0)
			{
				return searchResults.Entries[0].Id;
			}

			return null;
		}

		public async Task<string> CreateFolderAsync(string parentFolderID, string folderName)
		{
			if (IsAuthorizationRequired)
			{
				await AuthorizeAsync();
			}

			folderName = FixupFolderNameForBox(folderName);

			BoxFolderRequest folderRequest = new BoxFolderRequest
			{
				Name = folderName,
				Parent = new BoxRequestEntity
				{
					Id = parentFolderID,
					Type = BoxType.folder
				},
				Type = BoxType.folder
			};

			BoxFolder newFolder = await _boxClient.FoldersManager.CreateAsync(folderRequest, new[] { "id" });

			return newFolder.Id;
		}

		public async Task<bool> DeleteFolderAsync(string folderID, bool recursive = false)
		{
			if (IsAuthorizationRequired)
			{
				await AuthorizeAsync();
			}

			return await _boxClient.FoldersManager.DeleteAsync(folderID, recursive);
		}

		public async Task<ExternalFile> UploadFileAsync(string parentFolderID, string name, Stream data, bool getDownloadUrl = true)
		{
			if (IsAuthorizationRequired)
			{
				await AuthorizeAsync();
			}

			BoxFileRequest fileRequest = new BoxFileRequest
			{
				Name = name,
				Parent = new BoxRequestEntity
				{
					Id = parentFolderID,
					Type = BoxType.folder
				},
				Type = BoxType.file
			};

			BoxFile newFile = await _boxClient.FilesManager.UploadAsync(fileRequest, data, fields: new[] { "id" });
			if (getDownloadUrl)
			{
				BoxSharedLinkRequest sharedLinkRequest = new BoxSharedLinkRequest
				{
					Access = BoxSharedLinkAccessType.open,
					Permissions = new BoxPermissionsRequest
					{
						Download = true
					}
				};

				newFile = await _boxClient.FilesManager.CreateSharedLinkAsync(newFile.Id, sharedLinkRequest);
			}

			return new ExternalFile(newFile.Id, newFile.Name)
			{
				DownloadUrl = (getDownloadUrl) ? newFile.SharedLink?.DownloadUrl : null
			};
		}

		public async Task<bool> DeleteFileAsync(string fileID)
		{
			if (IsAuthorizationRequired)
			{
				await AuthorizeAsync();
			}

			return await _boxClient.FilesManager.DeleteAsync(fileID);
		}
		#endregion

		#region Private class methods

		private string FixupFolderNameForBox(string folderName)
		{
			return folderName.Replace("/", "_").Replace("\\", "_");
		}

		private async Task AuthorizeAsync()
		{
			if (_boxClient != null)
			{
				await _boxClient.Auth.RefreshAccessTokenAsync(null);
			}
			else
			{
				BoxConfiguration configuration = await _globalSettingProvider.GetBoxConfigurationAsync();

				BoxConfig boxConfig = new BoxConfig(
					configuration.BoxClientID,
					configuration.BoxClientSecret,
					configuration.BoxEnterpriseID,
					configuration.BoxPrivateKey,
					configuration.BoxPrivateKeyPassphrase,
					configuration.BoxPublicKeyID);

				BoxJWTAuth boxJwtAuth = new BoxJWTAuth(boxConfig);

				string userToken = boxJwtAuth.UserToken(configuration.BoxUserID);

				_boxClient = boxJwtAuth.UserClient(userToken, configuration.BoxUserID);
			}
			
			double expiresIn = _boxClient.Auth.Session.ExpiresIn;
			_nextAuthorizationTime = DateTime.UtcNow.AddSeconds(expiresIn);
		}
		#endregion
	}
}