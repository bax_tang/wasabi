﻿using System;

namespace WasabiOrders.WebService.DomainModel
{
	internal static class ActivityTypeNames
	{
		#region Properties

		internal static string UserLogin => "UserLogin";

		internal static string UserLogout => "UserLogout";

		internal static string ProductCreated => "ProductCreated";

		internal static string ProductUpdated => "ProductUpdated";

		internal static string ProductDeleted => "ProductDeleted";

		internal static string OrderCreated => "OrderCreated";

		internal static string OrderUpdated => "OrderUpdated";

		internal static string ImageUploaded => "ImageUploaded";

		internal static string CommentAdded => "CommentAdded";

		internal static string CommentUpdated => "CommentUpdated";

		internal static string CommentDeleted => "CommentDeleted";
		#endregion
	}
}