﻿using System;
using System.IdentityModel;
using System.IdentityModel.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;

using Microsoft.IdentityModel.Tokens;

using Newtonsoft.Json;

namespace WasabiOrders.WebService.DomainModel.Providers.Impl
{
	using DataModel.Entities;

	public class IdentityTokenProvider : IIdentityTokenProvider
	{
		#region Constants, fields and properties

		private const string IdentityProviderClaimType = "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";

		private const string SessionIdentifierClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sessionidentifier";

		private readonly byte[] _keyForHmacSha256;

		private readonly JwtSecurityTokenHandler _tokenHandler;
		#endregion

		#region Constructors

		public IdentityTokenProvider(WebServiceConfiguration configuration, JwtSecurityTokenHandler tokenHandler)
		{
			_keyForHmacSha256 = Convert.FromBase64String(configuration.SecurityKey);
			_tokenHandler = tokenHandler;
		}
		#endregion

		#region IIdentityTokenProvider implementation

		public ClaimsIdentity CreateIdentity(UserSession userSession)
		{
			string userID = userSession.UserID.ToString();
			string sessionID = userSession.Id.ToString();

			ClaimsIdentity userIdentity = new ClaimsIdentity(AuthConstants.DefaultAuthenticationType, ClaimTypes.Name, ClaimTypes.Role);

			userIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userID, ClaimValueTypes.String));
			userIdentity.AddClaim(new Claim(ClaimTypes.Name, userSession.User.AccountName, ClaimValueTypes.String));
			userIdentity.AddClaim(new Claim(SessionIdentifierClaimType, sessionID, ClaimValueTypes.String));
			userIdentity.AddClaim(new Claim(IdentityProviderClaimType, AuthConstants.DefaultIdentityProviderClaimValue, ClaimValueTypes.String));

			return userIdentity;
		}

		public string CreateAccessToken(ClaimsIdentity userIdentity, DateTime issuedAt)
		{
			var credentials = new SigningCredentials(new SymmetricSecurityKey(_keyForHmacSha256), SecurityAlgorithms.HmacSha256Signature);
			
			DateTime expires = issuedAt.AddYears(5);

			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Audience = AuthConstants.Audience,
				Issuer = AuthConstants.Issuer,
				Expires = expires,
				IssuedAt = issuedAt,
				NotBefore = issuedAt,
				Subject = userIdentity,
				SigningCredentials = credentials
			};

			var plainToken = _tokenHandler.CreateJwtSecurityToken(tokenDescriptor);

			string tokenText = _tokenHandler.WriteToken(plainToken);
			return tokenText;
		}

		public ClaimsIdentity CreateIdentityFromToken(string accessToken)
		{
			var token = _tokenHandler.ReadJwtToken(accessToken);

			ClaimsIdentity userIdentity = new ClaimsIdentity(
				token.Claims,
				AuthConstants.DefaultAuthenticationType,
				ClaimTypes.Name,
				ClaimTypes.Role);
			return userIdentity;
		}
		#endregion

		#region Private class methods
		#endregion
	}
}