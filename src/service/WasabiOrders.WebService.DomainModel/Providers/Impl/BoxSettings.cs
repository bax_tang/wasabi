﻿using System;

namespace WasabiOrders.WebService.DomainModel.Providers.Impl
{
	internal static class BoxSettings
	{
		internal static string BoxClientID => "BoxClientID";

		internal static string BoxClientSecret => "BoxClientSecret";

		internal static string BoxEnterpriseID => "BoxEnterpriseID";

		internal static string BoxPrivateKey => "BoxPrivateKey";

		internal static string BoxPrivateKeyPassphrase => "BoxPrivateKeyPassphrase";

		internal static string BoxPublicKeyID => "BoxPublicKeyID";

		internal static string BoxUserID => "BoxUserID";
	}
}