﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace WasabiOrders.WebService.DomainModel.Providers.Impl
{
	using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;
	using Models;

	public class GlobalSettingProvider : IGlobalSettingProvider
	{
		#region Fields and properties

		private readonly IGlobalSettingRepository _globalSettingRepository;
		#endregion

		#region Constructors

		public GlobalSettingProvider(IGlobalSettingRepository globalSettingRepository)
		{
			_globalSettingRepository = globalSettingRepository;
		}
		#endregion

		#region IGlobalSettingProvider implementation

		public Task<BoxConfiguration> GetBoxConfigurationAsync()
		{
			return InternalGetBoxConfiguration();
		}
		#endregion

		#region Private class methods

		private Task<string> InternalGetSetting(string settingName)
		{
			return _globalSettingRepository.Query()
				.Where(x => x.Name == settingName)
				.Select(x => x.Value)
				.FirstOrDefaultAsync();
		}

		private async Task<BoxConfiguration> InternalGetBoxConfiguration()
		{
			var boxSettingsQuery = _globalSettingRepository.Query()
				.Where(x => x.Name.StartsWith("Box"))
				.Select(x => new { x.Name, x.Value });

			var boxSettings = await boxSettingsQuery.ToListAsync();

			var settingsDictionary = new Dictionary<string, string>(boxSettings.Count, StringComparer.OrdinalIgnoreCase);
			foreach (var boxSetting in boxSettings)
			{
				settingsDictionary[boxSetting.Name] = boxSetting.Value;
			}

			BoxConfiguration configuration = new BoxConfiguration(settingsDictionary);
			return configuration;
		}
		#endregion
	}
}