﻿using System;
using System.Collections.Generic;
using System.Net;

namespace WasabiOrders.WebService.DomainModel
{
	/// <summary>
	/// Represents a result of service operation
	/// </summary>
	public class OperationResult
	{
		#region Fields and properties
		/// <summary>
		/// Gets or sets value that determine operation executing status
		/// </summary>
		public bool Succeeded { get; private set; }
		/// <summary>
		/// Gets or sets result status code
		/// </summary>
		public HttpStatusCode StatusCode { get; set; }
		/// <summary>
		/// Gets or sets collection of executing errors
		/// </summary>
		public ICollection<string> Errors { get; private set; }
		#endregion

		#region Constructors
		/// <summary>
		/// Initialize a new instance of the <see cref="OperationResult"/>
		/// </summary>
		/// <param name="succeeded">Value that determine operation executing status</param>
		public OperationResult(bool succeeded)
		{
			Succeeded = succeeded;
			StatusCode = (succeeded) ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
			Errors = new List<string>(4);
		}
		/// <summary>
		/// Initialize a new instance of the <see cref="OperationResult"/>
		/// </summary>
		/// <param name="errorMessage">Error message</param>
		public OperationResult(string errorMessage)
		{
			Succeeded = false;
			StatusCode = HttpStatusCode.BadRequest;
			Errors = new List<string>(4)
			{
				errorMessage
			};
		}
		/// <summary>
		/// Initialize a new instance of the <see cref="OperationResult"/> based on other operation result
		/// </summary>
		/// <param name="otherResult">Other instance of the <see cref="OperationResult" /></param>
		public OperationResult(OperationResult otherResult)
		{
			if (otherResult == null)
			{
				throw new ArgumentNullException(nameof(otherResult), "Initial result can't be null.");
			}

			Succeeded = otherResult.Succeeded;
			Errors = new List<string>(otherResult.Errors);
		}
		#endregion

		#region object methods overriding

		public override string ToString()
		{
			if (Succeeded)
			{
				return StatusCode.ToString();
			}

			if (Errors.Count > 0)
			{
				return $"{StatusCode}; {string.Join(";", Errors)}";
			}

			return StatusCode.ToString();
		}
		#endregion

		#region Public class methods
		/// <summary>
		/// Create failed result with status code <see cref="HttpStatusCode.NotFound"/>
		/// </summary>
		/// <typeparam name="TContent">Result content type</typeparam>
		/// <returns></returns>
		public static OperationResult<TContent> NotFound<TContent>()
		{
			return new OperationResult<TContent>(false)
			{
				StatusCode = HttpStatusCode.NotFound
			};
		}
        /// <summary>
        /// Create succeeded result
        /// </summary>
        /// <returns></returns>
        public static OperationResult Ok()
        {
            return new OperationResult(true);
        }
		/// <summary>
		/// Create succeeded result with specified model data
		/// </summary>
		/// <typeparam name="TContent">Result content type</typeparam>
		/// <param name="content">Content instance</param>
		/// <returns></returns>
		public static OperationResult<TContent> Ok<TContent>(TContent content)
		{
			return new OperationResult<TContent>(true)
			{
				Content = content
			};
		}
        /// <summary>
        /// Create error result with specified error message and status code
        /// </summary>
        /// <param name="message">Result error message</param>
        /// <param name="statusCode">Result status code</param>
        /// <returns></returns>
        public static OperationResult Error(string message, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return new OperationResult(message)
            {
                StatusCode = statusCode
            };
        }
		/// <summary>
		/// Returns object that represents result content
		/// </summary>
		/// <returns></returns>
		public virtual object GetData() => ToString();
		#endregion
	}

	/// <summary>
	/// Represents a generic result of the service operation
	/// </summary>
	/// <typeparam name="TContent">Result content type</typeparam>
	public class OperationResult<TContent> : OperationResult
	{
		#region Properties
		/// <summary>
		/// Gets or sets result content
		/// </summary>
		public TContent Content { get; internal set; }
		#endregion

		#region Constructors
		/// <summary>
		/// Initialize a new instance of the <see cref="OperationResult"/>
		/// </summary>
		/// <param name="succeeded">Value that determine operation executing status</param>
		public OperationResult(bool succeeded) : base(succeeded) { }
		/// <summary>
		/// Initialize a new instance of the <see cref="OperationResult"/>
		/// </summary>
		/// <param name="errorMessage">Error message</param>
		public OperationResult(string errorMessage) : base(errorMessage) { }
		#endregion

		#region OperationResult methods overriding
		/// <summary>
		/// Returns object that represents result content
		/// </summary>
		/// <returns></returns>
		public override object GetData() => Content;
		#endregion
	}
}