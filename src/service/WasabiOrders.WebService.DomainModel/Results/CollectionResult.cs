﻿using System;
using System.Collections.Generic;

namespace WasabiOrders.WebService.DomainModel
{
	/// <summary>
	/// Represents a collection-based result of paging-supported service operations
	/// </summary>
	/// <typeparam name="TItem">Item type</typeparam>
	public class CollectionResult<TItem>
	{
		#region Fields and properties
		/// <summary>
		/// Result items
		/// </summary>
		public IEnumerable<TItem> Items { get; internal set; }
		/// <summary>
		/// Total count
		/// </summary>
		public int Total { get; internal set; }
		/// <summary>
		/// Page number
		/// </summary>
		public int PageNumber { get; internal set; }
		/// <summary>
		/// Page size
		/// </summary>
		public int PageSize { get; internal set; }
		/// <summary>
		/// Pages count
		/// </summary>
		public int PagesCount { get; internal set; }
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="CollectionResult{TItem}"/>
		/// </summary>
		/// <param name="items">Collection of result items</param>
		internal CollectionResult(IEnumerable<TItem> items)
		{
			if (items == null)
			{
				throw new ArgumentNullException(nameof(items), "The collection of result items can't be null.");
			}

			Items = items;
		}
		#endregion
	}

	/// <summary>
	/// Provides static methods to create instances of <see cref="CollectionResult{TItem}"/>
	/// </summary>
	public static class CollectionResult
	{
		#region Public class methods
		/// <summary>
		/// Creates a new <see cref="CollectionResult{TItem}"/>
		/// </summary>
		/// <typeparam name="TItem">Item type</typeparam>
		/// <param name="items">Collection of result items</param>
		/// <param name="pageNumber">Page number</param>
		/// <param name="pageSize">Page size</param>
		/// <param name="pagesCount">Pages count</param>
		/// <param name="totalCount">Total count</param>
		/// <returns></returns>
		public static CollectionResult<TItem> Create<TItem>(IEnumerable<TItem> items, int pageNumber, int pageSize, int pagesCount, int totalCount)
		{
			CollectionResult<TItem> collectionResult = new CollectionResult<TItem>(items)
			{
				PageNumber = pageNumber,
				PageSize = pageSize,
				PagesCount = pagesCount,
				Total = totalCount
			};

			return collectionResult;
		}

        public static CollectionResult<TItem> Create<TItem>(IEnumerable<TItem> items, int pagesCount, int totalCount, PaginationContext paginationContext)
        {
            CollectionResult<TItem> collectionResult = new CollectionResult<TItem>(items)
            {
                PageNumber = paginationContext.PageNumber,
                PageSize = paginationContext.PageSize,
                PagesCount = pagesCount,
                Total = totalCount
            };

            return collectionResult;
        }
		#endregion
	}
}