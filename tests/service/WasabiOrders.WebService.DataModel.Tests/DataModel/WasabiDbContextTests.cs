﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;

namespace WasabiOrders.WebService.DataModel.Tests
{
	using Entities;
	using Mappings;

    [TestClass]
    public class WasabiDbContextTests
    {
        #region Test methods

        [TestMethod]
        public void IsConnectionCreatesSuccessfully()
        {
            Assert.IsNotNull(Connection);
            Assert.IsTrue(Connection.State == ConnectionState.Open);
        }

        [TestMethod]
        public void IsContextCreatesSuccessfully()
        {
            WasabiDbContext context = CreateContext();

            Assert.IsNotNull(context);
            Assert.IsTrue(context.Database.Exists());
        }

        [TestMethod]
        public void IsDataModelBuildsSuccessfully()
        {
            WasabiDbContext context = CreateContext();

			DbSet<ProductCategory> categoriesSet = context.Set<ProductCategory>();

			List<ProductCategory> categoriesList = categoriesSet.ToList();

			Assert.IsNotNull(categoriesList);
			Assert.IsTrue(categoriesList.Count >= 0);

			var categoriesResult = new
			{
				Categories = categoriesList
			};
			Console.WriteLine(JsonConvert.SerializeObject(categoriesResult, SerializationSettings));
        }

		[TestMethod]
		public void IsProductsListExtractsSuccessfully()
		{
			WasabiDbContext context = CreateContext();

			DbSet<Product> productsSet = context.Set<Product>();

			IQueryable<Product> query = productsSet.AsQueryable().Include(p => p.Images).Take(5);

			List<Product> productsList = query.ToList();

			Assert.IsNotNull(productsList);
			Assert.IsTrue(productsList.Count >= 0);

			var productsResult = new
			{
				Products = productsList
			};
			Console.WriteLine(JsonConvert.SerializeObject(productsResult, SerializationSettings));
		}

		[TestMethod]
		public void IsOrdersListExtractsSuccessfully()
		{
			WasabiDbContext context = CreateContext();

			DbSet<Order> ordersSet = context.Set<Order>();

			IQueryable<Order> query = ordersSet.AsQueryable().Include(p => p.Author).Include(p => p.Items);

			List<Order> ordersList = query.ToList();

			Assert.IsNotNull(ordersList);
			Assert.IsTrue(ordersList.Count >= 0);

			var ordersResult = new
			{
				Orders = ordersList
			};
			Console.WriteLine(JsonConvert.SerializeObject(ordersResult, SerializationSettings));
		}

		[TestMethod]
		public void IsUsersWithRolesExtractsSuccessfully()
		{
			WasabiDbContext context = CreateContext();

			DbSet<User> usersSet = context.Set<User>();

			IQueryable<User> query = usersSet.AsQueryable();

			List<User> usersList = query.ToList();

			Assert.IsNotNull(usersList);
			Assert.IsTrue(usersList.Count >= 0);

			var usersResult = new
			{
				Users = usersList
			};
			Console.WriteLine(JsonConvert.SerializeObject(usersResult, SerializationSettings));
		}

		[TestMethod]
		public void IsWhereQueryBasedOnHashSetExecutesSuccessfully()
		{
			WasabiDbContext context = CreateContext();

			Guid authorID = Guid.Parse("0FFDE532-AF7D-4E80-8364-8C21C47B8C39");
			HashSet<Guid> orderIds = new HashSet<Guid>
			{
				Guid.NewGuid(),
				Guid.NewGuid()
			};

			DbSet<Order> ordersSet = context.Set<Order>();
			var ordersQuery = ordersSet.AsQueryable().Where(x =>
				(x.Rating == 5 && x.AuthorID == authorID) ||
				orderIds.Contains(x.Id));

			List<Order> ordersList = ordersQuery.ToList();

			Assert.IsNotNull(ordersList);
			Assert.IsTrue(ordersList.Count >= 0);

			var ordersResult = new
			{
				Orders = ordersList
			};
			Console.WriteLine(JsonConvert.SerializeObject(ordersResult, SerializationSettings));
		}

		[TestMethod]
		public void IsWhereQueryBasedOnCreatedAtExecutesSuccessfully()
		{
			WasabiDbContext context = CreateContext();

			DbSet<ProductCategory> categoriesSet = context.Set<ProductCategory>();

			DateTime searchTime = DateTime.Now.AddHours(-1.5);
			var categoriesQuery = categoriesSet.AsQueryable().Where(x => x.CreatedAt <= searchTime);

			List<ProductCategory> categoriesList = categoriesQuery.ToList();

			Assert.IsNotNull(categoriesList);
			Assert.IsTrue(categoriesList.Count >= 0);

			var categoriesResult = new
			{
				Categories = categoriesList
			};
			Console.WriteLine(JsonConvert.SerializeObject(categoriesResult, SerializationSettings));
		}

		[TestMethod]
		public void IsNewCategoryCreatesSuccessfully()
		{
			WasabiDbContext context = CreateContext();

			DbSet<ProductCategory> categoriesSet = context.Set<ProductCategory>();

			ProductCategory newCategory = new ProductCategory
			{
				Name = "Новая крутая категория"
			};

			categoriesSet.Add(newCategory);

			int count = context.SaveChanges();

			Assert.IsTrue(newCategory.Id != Guid.Empty);

			var categoryResult = new
			{
				NewCategory = newCategory
			};
			Console.WriteLine(JsonConvert.SerializeObject(categoryResult, SerializationSettings));

			categoriesSet.Remove(newCategory);
			context.SaveChanges();
		}

		[TestMethod]
		public void IsOrderExpressionCreatesAndWorksSuccessfully()
		{
			WasabiDbContext context = CreateContext();
			DbSet<Product> productsSet = context.Set<Product>();

			var productsQuery = productsSet.OrderBy("Price DESC").Skip(0).Take(1).Select(x => new
			{
				x.Id,
				x.Name,
				x.Description,
				x.Price,
				x.Weight,
				x.Rating,
				x.CategoryID,
				ImagesCount = x.Images.Count
			});
			var productsList = productsQuery.ToList();

			var productsResult = new
			{
				Products = productsList,
				Total = productsSet.Count()
			};
			Console.WriteLine(JsonConvert.SerializeObject(productsResult, SerializationSettings));
		}
        #endregion

        #region Fields and properties

        private SqlConnection Connection;

		private JsonSerializerSettings SerializationSettings;
        #endregion

        #region Initialize / cleanup methods

        [TestInitialize]
        public void Initialize()
        {
            SqlConnection connection = new SqlConnection(WebServiceConfiguration.Instance.ConnectionString);
            connection.Open();
            Connection = connection;

			JsonSerializerSettings serializationSettings = new JsonSerializerSettings
			{
				Formatting = Formatting.Indented,
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
				MaxDepth = 1
			};
			serializationSettings.Converters.Add(new GuidConverter());
			SerializationSettings = serializationSettings;
        }

        [TestCleanup]
        public void Cleanup()
        {
            Connection?.Dispose();
        }
		#endregion

		#region Private class methods

		private WasabiDbContext CreateContext()
		{
            WasabiDbContext context = new WasabiDbContext(WebServiceConfiguration.Instance.ConnectionString);

#if DEBUG
            context.Database.Log = Console.WriteLine;
#endif

            return context;
		}
		#endregion
	}
}