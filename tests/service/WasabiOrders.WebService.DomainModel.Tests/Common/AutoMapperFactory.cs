﻿using System;
using System.Collections.Generic;

using AutoMapper;
using AutoMapper.Configuration;

namespace WasabiOrders.WebService.DomainModel
{
	using AutoMappings;

	internal static class AutoMapperFactory
	{
		#region Public class methods

		public static IMapper CreateMapper()
		{
			var configExpression = new MapperConfigurationExpression();

			new CommentAutoMap().Apply(configExpression);

			var mapperConfig = new MapperConfiguration(configExpression);

			return new Mapper(mapperConfig);
		}
		#endregion
	}
}