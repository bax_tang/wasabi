﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;

namespace WasabiOrders.WebService.DomainModel
{
	using DataModel;

	internal class DbContextScope : IDbContextScope
	{
		private readonly DbContext Context;

		public DbContextScope(DbContext context)
		{
			Context = context;
		}

		void IDisposable.Dispose()
		{
			Context?.Dispose();
		}

		public DbContext GetDbContext() => Context;
	}

	internal static class DbContextFactory
	{
		#region Public class methods

		public static IDbContextScope CreateScope()
		{
            WasabiDbContext context = new WasabiDbContext(WebServiceConfiguration.Instance.ConnectionString);

#if DEBUG
			context.Database.Log = Console.WriteLine;
#endif

			return new DbContextScope(context);
		}
		#endregion
	}
}