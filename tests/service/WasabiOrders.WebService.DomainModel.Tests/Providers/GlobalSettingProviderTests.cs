﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;

namespace WasabiOrders.WebService.DomainModel.Providers.Tests
{
	using DataModel;
	using DataModel.Repositories;
	using Impl;
	using Models;

	[TestClass]
	public class GlobalSettingProviderTests
	{
		#region Test methods

		[TestMethod]
		public async Task IsBoxConfigurationGetsSuccessfully()
		{
			BoxConfiguration configuration = await _globalSettingProvider.GetBoxConfigurationAsync();

			Assert.IsNotNull(configuration);

			Console.WriteLine(JsonConvert.SerializeObject(configuration, Formatting.Indented));
		}
		#endregion

		#region Fields and properties

		private IGlobalSettingProvider _globalSettingProvider;
		#endregion

		#region Initialize / cleanup methods

		[TestInitialize]
		public void Initialize()
		{
			IDbContextScope contextScope = DbContextFactory.CreateScope();
			IGlobalSettingRepository globalSettingRepository = new GlobalSettingRepository(contextScope);

			_globalSettingProvider = new GlobalSettingProvider(globalSettingRepository);
		}
		#endregion
	}
}