﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WasabiOrders.WebService.DomainModel.Tests
{
    using DataModel;
    using DataModel.Entities;
    using DataModel.Repositories;
    using Dtos;
    using Providers.Impl;
    using Services.Impl;

    [TestClass]
    public class CategoryServiceTests
    {
        #region Test methods

        [TestMethod]
        public async Task IsExistingCategoriesExtractsSuccessfully()
        {
            var result = await Service.GetAllAsync();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Total >= 0);

            Console.WriteLine(JsonConvert.SerializeObject(result, Formatting.Indented, SerializerSettings));
        }

        [TestMethod]
        public async Task IsNewCategoryCreatesSuccessfully()
        {
            CategoryDto categoryDto = new CategoryDto
            {
                Name = "Test category",
                Description = "Test category description"
            };

            var result = await Service.CreateAsync(categoryDto);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Succeeded);
            Assert.IsTrue(result.Content.Id != Guid.Empty);

            Console.WriteLine(JsonConvert.SerializeObject(result, Formatting.Indented, SerializerSettings));
        }

        [TestMethod]
        public async Task IsExistingCategoryDeletesSuccessfully()
        {
            Guid categoryID = new Guid("30ce5bd7-4782-e711-bdcb-74d435bbe466");

            var result = await Service.DeleteAsync(categoryID);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Succeeded);

            Console.WriteLine(JsonConvert.SerializeObject(result, Formatting.Indented, SerializerSettings));
        }
        #endregion

        #region Fields and properties

        private Guid AuthorID = new Guid("0FFDE532-AF7D-4E80-8364-8C21C47B8C39"); // kalinov_dmitry

        private static JsonSerializerSettings SerializerSettings;

        private CategoryService Service;
        #endregion

        #region Initialize / cleanup methods

        [ClassInitialize]
        public static void ClassInitialize()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
            settings.Converters.Add(new GuidConverter());
            settings.Converters.Add(new StringEnumConverter());

            SerializerSettings = settings;
        }

        [TestInitialize]
        public void Initialize()
        {
            var mapper = AutoMapperFactory.CreateMapper();
            var contextScope = DbContextFactory.CreateScope();

            var mappingProvider = new AutoMapperMappingProvider(mapper);

            var unitOfWork = new UnitOfWork(contextScope);
            var categoriesRepository = new ProductCategoryRepository(contextScope);
            var productsRepository = new ProductRepository(contextScope);
            var sqlRepository = new SqlRepository(contextScope);
            var sqlService = new SqlProvider(sqlRepository);

            Service = new CategoryService(unitOfWork, categoriesRepository, productsRepository, sqlService);
        }

        [TestCleanup]
        public void Cleanup()
        {
            Service = null;
        }
        #endregion
    }
}