﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace WasabiOrders.WebService.DomainModel.Tests
{
	using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;
	using Dtos;
	using Providers;
	using Providers.Impl;
	using Services.Impl;

	[TestClass]
	public class ProductServiceTests
	{
		#region Test methods

		[TestMethod]
		public async Task IsExistingProductsUploadsSuccessfully()
		{
			//await DeleteExistingProductsAsync();

			await UploadProductsAsync();
		}
		#endregion

		#region Fields and properties

		private IDbContextScope _contextScope;

		private IUnitOfWork _unitOfWork;

		private IGlobalSettingRepository _globalSettingRepository;

		private IProductCategoryRepository _productCategoryRepository;

		private IProductRepository _productRepository;

		private IProductImageRepository _productImageRepository;

		private IFileRepository _fileRepository;

		private ISqlRepository _sqlRepository;

		private IActivityLogProvider _activityLogProvider;

		private IFileUploadProvider _fileUploadProvider;

		private IGlobalSettingProvider _globalSettingProvider;

		private ProductService _productService;
		#endregion

		#region Initialize / cleanup methods

		[TestInitialize]
		public void Initialize()
		{
			IDbContextScope contextScope = DbContextFactory.CreateScope();

			_contextScope = contextScope;
			_unitOfWork = new UnitOfWork(contextScope);

			_globalSettingRepository = new GlobalSettingRepository(contextScope);
			_productCategoryRepository = new ProductCategoryRepository(contextScope);
			_productRepository = new ProductRepository(contextScope);
			_productImageRepository = new ProductImageRepository(contextScope);
			_fileRepository = new FileRepository(contextScope);
			_sqlRepository = new SqlRepository(contextScope);

			_activityLogProvider = new ActivityLogProvider(_sqlRepository);
			_globalSettingProvider = new GlobalSettingProvider(_globalSettingRepository);
			_fileUploadProvider = new BoxFileUploadProvider(_globalSettingProvider);

			_productService = new ProductService(
				_unitOfWork,
				_productRepository,
				_productCategoryRepository,
				_productImageRepository,
				_fileRepository,
				_activityLogProvider,
				_fileUploadProvider,
				_globalSettingProvider);
		}

		[TestCleanup]
		public void Cleanup()
		{
			_contextScope?.Dispose();
			_contextScope = null;
		}
		#endregion

		#region Private class methods

		private async Task DeleteExistingProductsAsync()
		{
			/*
			var filesQuery = _fileRepository.Query();
			var productImagesQuery = _productImageRepository.Query();

			var imageFilesQuery = from f in filesQuery
								  join pi in productImagesQuery on f.Id equals pi.FileID
								  where f.ExternalFileID != null
								  select f.ExternalFileID;

			var imageFiles = await imageFilesQuery.ToListAsync();
			if (imageFiles.Count > 0)
			{
				foreach (string fileID in imageFiles)
				{
					await _fileUploadProvider.DeleteFileAsync(fileID);
				}
			}
			*/
			
			var productFoldersQuery = _productRepository.Query()
				.Where(x => x.ExternalFolderID != null)
				.Select(x => x.ExternalFolderID)
				.Distinct();
			var productFolders = await productFoldersQuery.ToListAsync();
			if (productFolders.Count > 0)
			{
				foreach (string folderID in productFolders)
				{
					await _fileUploadProvider.DeleteFolderAsync(folderID, true);
				}
			}

			DbContext context = _contextScope.GetDbContext();

			// delete existing product images, files and products
			await context.Database.ExecuteSqlCommandAsync(@"
DECLARE @ProductImageFiles TABLE ([FileID] UNIQUEIDENTIFIER NOT NULL);

INSERT INTO @ProductImageFiles ([FileID])
SELECT [FileID] FROM [dbo].[ProductImages];

DELETE FROM [dbo].[ProductImages] WITH(ROWLOCK);

DELETE FROM [dbo].[Files] WITH(ROWLOCK)
WHERE [FileID] IN (SELECT [FileID] FROM @ProductImageFiles);

DELETE FROM [dbo].[Products] WITH(ROWLOCK);");
		}

		private async Task UploadProductsAsync()
		{
			string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			string wasabiDirectory = Path.Combine(desktopDirectory, "Wasabi");

			if (Directory.Exists(wasabiDirectory))
			{
				var productFiles = Directory.EnumerateFiles(wasabiDirectory, "*.json", SearchOption.TopDirectoryOnly);
				foreach (var productFilePath in productFiles)
				{
					string productJson = System.IO.File.ReadAllText(productFilePath, Encoding.UTF8);
					JObject jsonObject = JObject.Parse(productJson);

					string categoryName = (string)jsonObject["Category"];
					string productName = (string)jsonObject["Name"];
					string productDescription = (string)jsonObject["Description"];

					string weightText = (string)jsonObject["Weight"];
					int delimiterIndex = weightText.IndexOf("/");
					if (delimiterIndex == -1)
					{
						delimiterIndex = weightText.IndexOf(",");
					}
					string weightValue = (delimiterIndex != -1) ? weightText.Substring(0, delimiterIndex) : weightText;

                    double productWeight = 0.0D;
					double.TryParse(weightValue, out productWeight);

					decimal productPrice = (decimal)(double)jsonObject["Price"];

					JArray productImages = (JArray)jsonObject["Images"];

					ProductCategory category = await _productCategoryRepository.Query()
						.Where(x => x.Name == categoryName)
						.FirstOrDefaultAsync();

					productName = productName.Trim();

					if (await _productRepository.Query().AnyAsync(x => x.CategoryID == category.Id && x.Name == productName))
					{
						continue;
					}

					string productFolder = null;
					try
					{
						productFolder = await _fileUploadProvider.CreateFolderAsync(category.ExternalFolderID, productName);
					}
					catch (Exception exc)
					{
						if (Debugger.IsAttached)
						{
							Debugger.Break();
						}
					}

					Product newProduct = _productRepository.Add(new Product
					{
						Name = productName,
						Description = productDescription,
						Price = productPrice,
						Weight = productWeight,
						Category = category,
						ExternalFolderID = productFolder
					});
					
					for (int index = 0; index < productImages.Count; ++index)
					{
						JObject imageObject = (JObject)productImages[index];

						ImageType imageType = (ImageType)(int)imageObject["Type"];

						string imageFilePath = (string)imageObject["Path"];
						FileInfo imageFileInfo = new FileInfo(imageFilePath);

						long imageSize = imageFileInfo.Length;
						FileStream imageStream = imageFileInfo.OpenRead();

						try
						{
							var externalFile = await _fileUploadProvider.UploadFileAsync(productFolder, imageFileInfo.Name, imageStream, true);

							File newFile = _fileRepository.Add(new File
							{
								Name = imageFileInfo.Name,
								Size = imageSize,
								Type = "image/jpeg",
								ExternalFileID = externalFile.Id,
								ExternalFileUrl = externalFile.DownloadUrl
							});

							ProductImage newImage = _productImageRepository.Add(new ProductImage
							{
								ImageFile = newFile,
								ParentProduct = newProduct,
								Type = imageType
							});
						}
						catch (Exception exc)
						{
							if (Debugger.IsAttached)
							{
								Debugger.Break();
							}
						}
					}

					await _contextScope.GetDbContext().SaveChangesAsync();
				}
			}
		}
		#endregion
	}
}