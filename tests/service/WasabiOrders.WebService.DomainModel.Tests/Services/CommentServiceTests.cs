﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WasabiOrders.WebService.DomainModel.Tests
{
	using DataModel;
	using DataModel.Entities;
	using DataModel.Repositories;
	using Dtos;
	using Providers.Impl;
	using Services.Impl;

	[TestClass]
	public class CommentServiceTests
	{
		#region Test methods

		[TestMethod]
		public async Task IsNewCommentCreatesSuccessfully()
		{
			Guid objectID = new Guid("37F626E9-0963-E711-A2EF-74D435BBE466"); // Кунсей харумаки

			CommentDto commentDto = new CommentDto
			{
				ObjectId = objectID,
				RepliedTo = null,
				Title = "New test comment",
				Text = "New test comment text"
			};

			var result = await Service.CreateCommentAsync(AuthorID, commentDto);

			Assert.IsNotNull(result);
			Assert.IsTrue(result.Succeeded);

			Console.WriteLine(JsonConvert.SerializeObject(result, Formatting.Indented, SerializerSettings));
		}

		[TestMethod]
		public async Task IsExistingCommentUpdatesSuccessfully()
		{
			Guid commentID = new Guid("4D2DBE2A-5A82-E711-BDCB-74D435BBE466"); // existing comment
			Guid objectID = new Guid("37F626E9-0963-E711-A2EF-74D435BBE466"); // Кунсей харумаки

			CommentDto commentDto = new CommentDto
			{
				ObjectId = objectID,
				RepliedTo = null,
				Title = "Updated comment",
				Text = "Updated comment text"
			};

			var result = await Service.UpdateCommentAsync(AuthorID, commentID, commentDto);

			Assert.IsNotNull(result);
			Assert.IsTrue(result.Succeeded);

			Console.WriteLine(JsonConvert.SerializeObject(result, Formatting.Indented, SerializerSettings));
		}

		[TestMethod]
		public async Task IsObjectCommentsExtractsSuccessfully()
		{
			Guid objectID = new Guid("37F626E9-0963-E711-A2EF-74D435BBE466"); // Кунсей харумаки

			var result = await Service.GetCommentsAsync(objectID, null, 10);

			Assert.IsNotNull(result);
			Assert.IsTrue(result.Total >= 0);
			Assert.IsTrue(result.PageSize >= 0);

			Console.WriteLine(JsonConvert.SerializeObject(result, Formatting.Indented, SerializerSettings));
		}
		#endregion

		#region Fields and properties

		private Guid AuthorID = new Guid("0FFDE532-AF7D-4E80-8364-8C21C47B8C39"); // kalinov_dmitry

		private static JsonSerializerSettings SerializerSettings;

		private CommentService Service;
		#endregion

		#region Initialize / cleanup methods

		[TestMethod]
		public static void ClassInitialize()
		{
			JsonSerializerSettings settings = new JsonSerializerSettings
			{
				Formatting = Formatting.Indented
			};
			settings.Converters.Add(new GuidConverter());
			settings.Converters.Add(new StringEnumConverter());

			SerializerSettings = settings;
		}

		[TestInitialize]
		public void Initialize()
		{
			var mapper = AutoMapperFactory.CreateMapper();
			var contextScope = DbContextFactory.CreateScope();

			var mappingProvider = new AutoMapperMappingProvider(mapper);

            var unitOfWork = new UnitOfWork(contextScope);
			var commentRepository = new CommentRepository(contextScope);
			var sqlRepository = new SqlRepository(contextScope);
			var sqlService = new SqlProvider(sqlRepository);

			Service = new CommentService(unitOfWork, commentRepository, mappingProvider, sqlService);
		}

		[TestCleanup]
		public void Cleanup()
		{
			Service = null;
		}
		#endregion
	}
}