﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Box.V2;
using Box.V2.Managers;
using Box.V2.Models;

namespace Box.Tests
{
	[TestClass]
	public class BoxFolderTests
	{
		#region Test methods

		[TestMethod]
		public async Task IsRootFolderInformationAcquiresSuccessfully()
		{
			BoxFolder rootFolder = await _client.FoldersManager.GetInformationAsync("0", new[] { "id", "name" });

			Assert.IsNotNull(rootFolder);

			Console.WriteLine(JsonConvert.SerializeObject(rootFolder, Formatting.Indented));
		}

		[TestMethod]
		public async Task IsWasabiOrdersFolderCreatesSuccessfully()
		{
			var searchResult = await _client.SearchManager.SearchAsync(
				ancestorFolderIds: new[] { "0" },
				keyword: "WasabiOrders",
				fields: new[] { "id" },
				scope: "user_content",
				type: "folder");

			BoxFolder wasabiFolder = null;

			if (searchResult != null && searchResult.TotalCount > 0)
			{
				Console.WriteLine("Folder \"WasabiOrders\" is already exist, acquiring information...");

				string targetFolderID = searchResult.Entries[0].Id;

				wasabiFolder = await _client.FoldersManager.GetInformationAsync(targetFolderID);
			}
			else
			{
				Console.WriteLine("Folder \"WasabiOrders\" does not exist, creating...");

				BoxFolderRequest folderRequest = new BoxFolderRequest
				{
					Name = "WasabiOrders",
					Description = "Root folder for WasabiOrders files",
					Parent = new BoxRequestEntity
					{
						Id = "0",
						Type = BoxType.folder
					},
					Type = BoxType.folder
				};

				wasabiFolder = await _client.FoldersManager.CreateAsync(folderRequest);
			}

			Assert.IsNotNull(wasabiFolder);

			Console.WriteLine(JsonConvert.SerializeObject(wasabiFolder, Formatting.Indented));
		}
		#endregion

		#region Fields and properties

		private BoxClient _client;
		#endregion

		#region Initialize / cleanup methods

		[TestInitialize]
		public void Initialize()
		{
			_client = BoxClientFactory.CreateBoxClient();
		}

		[TestCleanup]
		public void Cleanup()
		{
			_client = null;
		}
		#endregion
	}
}