﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

using Box.V2;
using Box.V2.Auth;
using Box.V2.Config;
using Box.V2.Converter;
using Box.V2.Extensions;
using Box.V2.JWTAuth;
using Box.V2.Managers;
using Box.V2.Models;
using Box.V2.Request;
using Box.V2.Services;

namespace Box.Tests
{
    [TestClass]
    public class BoxAuthorizationTests
    {
        #region Test methods

        [TestMethod]
        public async Task IsAuthorizationWorksSuccessfully()
        {
            string clientID = "3mbi3klalacchxgxe0uzlbxo1r7d7imz";
            string clientSecret = "l9WgY9iuJtAQVPSk5xGozjXkJcisSZep";
            string enterpriseID = "26438637";

            string publicKeyID = "xm2xdnxm";
            string privateKeyPassphrase = "*8LnQM.6";

            string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string privateKeyPath = Path.Combine(desktopDirectory, @"Wasabi\Keys\wasabi_orders_private_key.pem");
            string privateKey = File.ReadAllText(privateKeyPath);

            string userID = "2355238573";

            var config = new BoxConfig(clientID, clientSecret, enterpriseID,
                privateKey, privateKeyPassphrase, publicKeyID);

            var boxJwt = new BoxJWTAuth(config);

			string userToken = boxJwt.UserToken(userID);

			Assert.IsFalse(string.IsNullOrEmpty(userToken));

			var boxClient = boxJwt.UserClient(userToken, userID);
			
            Assert.IsNotNull(boxClient);
			Assert.IsNotNull(boxClient.Auth.Session);

            var rootFolder = await boxClient.FoldersManager.GetInformationAsync("0");

            Assert.IsNotNull(rootFolder);

            Console.WriteLine(JsonConvert.SerializeObject(rootFolder, Formatting.Indented));
        }
        #endregion
    }
}