﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

using Box.V2;
using Box.V2.Auth;
using Box.V2.Config;
using Box.V2.Converter;
using Box.V2.Extensions;
using Box.V2.JWTAuth;
using Box.V2.Managers;
using Box.V2.Models;
using Box.V2.Request;
using Box.V2.Services;

namespace Box.Tests
{
	internal static class BoxClientFactory
	{
		#region Public class methods

		public static BoxClient CreateBoxClient()
		{
			BoxConfig boxConfig = CreateBoxConfig();

			BoxJWTAuth boxJwtAuth = new BoxJWTAuth(boxConfig);

			string userID = BoxResources.UserID;
			string userToken = boxJwtAuth.UserToken(userID);

			BoxClient boxClient = boxJwtAuth.UserClient(userToken, userID);
			return boxClient;
		}
		#endregion

		#region Private class methods

		private static BoxConfig CreateBoxConfig()
		{
			string clientID = BoxResources.ClientID;
			string clientSecret = BoxResources.ClientSecret;
			string enterpriseID = BoxResources.EnterpriseID;
			string privateKey = BoxResources.PrivateKey;
			string privateKeyPassphrase = BoxResources.PrivateKeyPassphrase;
			string publicKeyID = BoxResources.PublicKeyID;

			BoxConfig boxConfig = new BoxConfig(clientID, clientSecret, enterpriseID, privateKey, privateKeyPassphrase, publicKeyID);
			return boxConfig;
		}
		#endregion
	}
}