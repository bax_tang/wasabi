﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HttpParser.Tests
{
    [TestClass]
    public class ProductsParserTests
    {
        #region Constants and fields

        private const string InitialUrl = "http://wasabi-samara.ru";

        private static readonly Random random = new Random();

        private string DataDirectory;

        private HttpClient Client = null;

        private ConcurrentDictionary<string, ChildNodeSelector> Selectors;
        #endregion

        #region Constructors
        
        public ProductsParserTests() { }
        #endregion

        #region Test methods

        [TestMethod]
        public async Task IsItemsDataParsingExecutesSuccessfully()
        {
            HttpClient client = Client;
            
            HtmlDocument document = await client.GetHtmlDocumentAsync(InitialUrl);

            string selectionPath = "//li[@class='hovered']";
            HtmlNodeCollection typeNodes = document.DocumentNode.SelectNodes(selectionPath);
            
            for (int index = 0; index < typeNodes.Count; ++index)
            {
                HtmlNode typeNode = typeNodes[index];

                string typeUrl = typeNode.ChildNodes["a"].Attributes["href"].Value;
                string fullTypeUrl = string.Concat(InitialUrl, typeUrl);

                await Task.Delay(random.Next(3000, 5000));
                
                HtmlDocument typeDocument = await client.GetHtmlDocumentAsync(fullTypeUrl);

                HtmlNode pagesNode = typeDocument.DocumentNode.SelectSingleNode("//div[@class='pages']");
                if (pagesNode != null)
                {
                    ParseItemNodes(typeDocument);

                    foreach (HtmlNode nextPageNode in pagesNode.ChildNodes.Where(n => n.Name == "a"))
                    {
                        string nextPageUrl = nextPageNode.Attributes["href"].Value;
                        string fullPageUrl = string.Concat(InitialUrl, nextPageUrl);

                        await Task.Delay(random.Next(3000, 5000));
                        HtmlDocument pageDocument = await client.GetHtmlDocumentAsync(fullPageUrl);

                        ParseItemNodes(pageDocument);
                    }
                }
                else
                {
                    ParseItemNodes(typeDocument);
                }
            }
        }

        [TestMethod]
        public async Task IsItemsDataUpdatingExecutesSuccessfully()
        {
            HttpClient client = Client;

            IEnumerable<string> itemDataFiles = Directory.EnumerateFiles(DataDirectory, "*.json", SearchOption.AllDirectories);
            foreach (string itemDataFilePath in itemDataFiles)
            {
                JObject itemData = null;

                using (StreamReader reader = new StreamReader(itemDataFilePath, Encoding.UTF8))
                {
                    itemData = JObject.Load(new JsonTextReader(reader));
                }

                string itemUrl = (string)itemData["URL"];

                HttpResponseMessage response = await client.GetAsync(itemUrl);
                HtmlDocument itemDocument = await GetDocumentFromResponseAsync(response);

                HtmlNode priceNode = itemDocument.DocumentNode.SelectSingleNode("//span[@class='price_value']");
                string priceValue = priceNode.InnerText.Trim();
                double price;
                if (double.TryParse(priceValue, out price))
                {
                    itemData["Price"] = price;
                }
                else
                {
                    Debugger.Break();
                }
                
                HtmlNode descriptionNode = itemDocument.DocumentNode.SelectSingleNode("//div[@id='productDescr']");
                HtmlNode bodyNode = descriptionNode.ChildNodes["table"];

                if (bodyNode.ChildNodes.Count > 1 && bodyNode.ChildNodes[1].ChildNodes.Count > 4)
                {
                    itemData["Weight"] = bodyNode.ChildNodes[1].ChildNodes[3].InnerText.Trim();
                }

                if (bodyNode.ChildNodes.Count > 4 && bodyNode.ChildNodes[3].ChildNodes.Count > 4)
                {
                    itemData["Description"] = bodyNode.ChildNodes[3].ChildNodes[3].InnerText.Trim();
                }
                
                using (StreamWriter dataWriter = new StreamWriter(itemDataFilePath, false, Encoding.UTF8, 4096))
                {
                    dataWriter.Write(itemData.ToString(Formatting.Indented, new JsonConverter[0]));
                    dataWriter.Flush();
                }

                await Task.Delay(random.Next(3000, 5000));
            }
        }
        
        [TestMethod]
        public void IsDirectoriesRenamedSuccessfully()
        {
            string targetDirectory = Path.Combine(DataDirectory, "Роллы 1-2");

            string[] childDirectories = Directory.GetDirectories(targetDirectory);
            for (int index = 0; index < childDirectories.Length; ++index)
            {
                DirectoryInfo current = new DirectoryInfo(childDirectories[index]);

                string newName = "1-" + current.Name;
                string newFullName = Path.Combine(current.Parent.FullName, newName);

                Directory.Move(current.FullName, newFullName);
            }
        }
        #endregion

        #region Initialize / cleanup methods

        [TestInitialize]
        public void InitializeTest()
        {
            string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string dataDirectory = Path.Combine(desktopDirectory, "Wasabi");
            DataDirectory = Directory.CreateDirectory(dataDirectory).FullName;

            Selectors = new ConcurrentDictionary<string, ChildNodeSelector>(StringComparer.OrdinalIgnoreCase);

            Client = new HttpClient();
        }
        #endregion

        #region Private class methods

        private static async Task<HtmlDocument> GetDocumentFromResponseAsync(HttpResponseMessage response)
        {
            string encodingName = response.Content.Headers.ContentType.CharSet ?? "utf-8";
            Encoding responseEncoding = Encoding.GetEncoding(encodingName);

            Stream responseStream = await response.Content.ReadAsStreamAsync();

            HtmlDocument document = new HtmlDocument();
            document.Load(responseStream, responseEncoding);
            return document;
        }

        private void ParseItemNodes(HtmlDocument document)
        {
            HtmlNodeCollection itemNodes = document.DocumentNode.SelectNodes("//div[@class='item_div']");
            for (int index = 0; index < itemNodes.Count; ++index)
            {
                ParseSingleItemNode(itemNodes[index]);
            }
        }

        private void ParseSingleItemNode(HtmlNode itemNode)
        {
            JObject itemData = new JObject();

            ChildNodeSelector imageSelector = GetOrCreateSelector("product_img_div");
            HtmlNode imageNode = itemNode.ChildNodes.FirstOrDefault(imageSelector.IsTarget);
            HtmlNode linkNode = imageNode.ChildNodes["a"];

            string productUrl = linkNode.GetAttributeValue("href", string.Empty);
            string itemID = null;
            if (productUrl != string.Empty)
            {
                itemID = productUrl.Substring(1 + productUrl.LastIndexOf('='));
                itemData["ProductID"] = itemID;
            }

            ChildNodeSelector categorySelector = GetOrCreateSelector("product_cat_name");
            HtmlNode categoryNode = itemNode.ChildNodes.FirstOrDefault(categorySelector.IsTarget);

            itemData["Category"] = categoryNode.InnerText;

            ChildNodeSelector titleSelector = GetOrCreateSelector("product_title");
            HtmlNode titleNode = itemNode.ChildNodes.FirstOrDefault(titleSelector.IsTarget);
            string title = titleNode.ChildNodes["a"].InnerText;

            itemData["Name"] = title;

            ChildNodeSelector weightSelector = GetOrCreateSelector("product_gp");
            bool firstEnter = true;
            HtmlNode weightNode = itemNode.ChildNodes.FirstOrDefault(weightSelector.IsTarget);
            foreach (HtmlNode weightChildNode in weightNode.ChildNodes)
            {
                if (string.Equals(weightChildNode.Name, "#text", StringComparison.OrdinalIgnoreCase))
                {
                    if (firstEnter)
                    {
                        firstEnter = false;

                        itemData["Weight"] = weightChildNode.InnerText.Trim();
                        continue;
                    }

                    itemData["Content"] = weightChildNode.InnerText.Trim();
                }
            }

            string itemUrl = titleNode.ChildNodes["a"].Attributes["href"].Value;
            itemData["URL"] = string.Concat(InitialUrl, itemUrl);

            ChildNodeSelector priceSelector = GetOrCreateSelector("product_price");
            HtmlNode priceNode = itemNode.ChildNodes.FirstOrDefault(priceSelector.IsTarget);
            itemData["Price"] = priceNode.InnerText.Trim();

            if (!string.IsNullOrEmpty(itemID))
            {
                string imageUrl = linkNode.ChildNodes["img"].Attributes["src"].Value;
                int ampIndex = imageUrl.IndexOf('&');
                string prepUrl = imageUrl.Substring(0, ampIndex);

                string thumbnailUrl = string.Concat(InitialUrl, imageUrl);
                string fullImageUrl = string.Concat(InitialUrl, prepUrl, "&w=480&h=360");
                
                HttpResponseMessage thumbnailImage = Client.GetAsync(thumbnailUrl).GetAwaiter().GetResult();
                Stream thumbnailImageStream = thumbnailImage.Content.ReadAsStreamAsync().GetAwaiter().GetResult();

                string thumbnailImagePath = Path.Combine(DataDirectory, $"{itemID}_0.jpg");
                using (FileStream imageFile = new FileStream(thumbnailImagePath, FileMode.Create, FileAccess.Write))
                {
                    thumbnailImageStream.CopyTo(imageFile, 16384);
                    imageFile.Flush(true);
                }

                HttpResponseMessage fullImageResponse = Client.GetAsync(fullImageUrl).GetAwaiter().GetResult();
                Stream fullImageStream = fullImageResponse.Content.ReadAsStreamAsync().GetAwaiter().GetResult();

                string fullImagePath = Path.Combine(DataDirectory, $"{itemID}_1.jpg");
                using (FileStream imageFile = new FileStream(fullImagePath, FileMode.Create, FileAccess.Write))
                {
                    fullImageStream.CopyTo(imageFile, 16384);
                    imageFile.Flush(true);
                }

                itemData["Images"] = new JArray(new[]
                {
                    new JObject
                    {
                        ["Url"] = thumbnailUrl,
                        ["Path"] = thumbnailImagePath,
                        ["Type"] = 0
                    },
                    new JObject
                    {
                        ["Url"] = fullImageUrl,
                        ["Path"] = fullImagePath,
                        ["Type"] = 1
                    }
                });

                string itemDataFile = Path.Combine(DataDirectory, $"{itemID}.json");
                using (StreamWriter dataWriter = new StreamWriter(itemDataFile, false, Encoding.UTF8, 4096))
                {
                    dataWriter.Write(itemData.ToString(Formatting.Indented, new JsonConverter[0]));
                    dataWriter.Flush();
                }
            }
        }

        private ChildNodeSelector GetOrCreateSelector(string className)
        {
            return Selectors.GetOrAdd(className, CreateSelectorIfNotExists);
        }

        private static ChildNodeSelector CreateSelectorIfNotExists(string className)
        {
            return new ChildNodeSelector(className);
        }
        #endregion

        #region Nested types

        private class ChildNodeSelector
        {
            public string ClassName;
            public ChildNodeSelector(string className)
            {
                ClassName = className;
            }
            public bool IsTarget(HtmlNode sourceNode)
            {
                HtmlAttribute classAttribute = sourceNode.Attributes["class"];

                return (classAttribute != null) && string.Equals(ClassName, classAttribute.Value, StringComparison.OrdinalIgnoreCase);
            }

			public override string ToString()
			{
				return ClassName;
			}
		}
        #endregion
    }
}