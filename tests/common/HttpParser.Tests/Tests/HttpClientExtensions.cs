﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using HtmlAgilityPack;

namespace HttpParser.Tests
{
    public static class HttpClientExtensions
    {
        #region Public class methods

        public static Task<HtmlDocument> GetHtmlDocumentAsync(this HttpClient client, string requestUri)
        {
            Uri targetUri = null;

            if (!Uri.TryCreate(requestUri, UriKind.RelativeOrAbsolute, out targetUri))
            {
                throw new ArgumentException("Invalid request URI.", nameof(requestUri));
            }

            return GetHtmlDocumentAsync(client, targetUri);
        }

        public static async Task<HtmlDocument> GetHtmlDocumentAsync(this HttpClient client, Uri requestUri)
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client), "Http client can't be null.");
            }
            if (requestUri == null)
            {
                throw new ArgumentNullException(nameof(requestUri), "Request URI can't be null.");
            }

            HttpResponseMessage response = await client.GetAsync(requestUri);

            string encodingName = response.Content.Headers.ContentType.CharSet ?? "utf-8";
            Encoding responseEncoding = Encoding.GetEncoding(encodingName);

            Stream responseStream = await response.Content.ReadAsStreamAsync();

            HtmlDocument document = new HtmlDocument();
            document.Load(responseStream, responseEncoding);
            return document;
        }
        #endregion
    }
}