﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Database.Tests
{
    [TestClass]
    public class DatabaseUploadingTests
    {
        #region Constants and fields

        private const string ConnectionString = "Data Source=(local);Initial Catalog=WasabiOrders.Database;Integrated Security=False;" +
            "User ID=sa;Password=saionara;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private const string CreateProductCommandText = @"
DECLARE @CategoryID UNIQUEIDENTIFIER = NULL;

SELECT @CategoryID = [CategoryID]
FROM [dbo].[ProductCategories] WITH(NOLOCK)
WHERE [Name] = @CategoryName;

IF (@CategoryID IS NULL)
BEGIN
  DECLARE @Message NVARCHAR(MAX) = REPLACE('Can''t found category with name ''!%1''', '!%1', @CategoryName);
  RAISERROR (@Message, 16, 1);
  RETURN;
END;

DECLARE @NewProducts TABLE ([ProductID] UNIQUEIDENTIFIER NOT NULL);

INSERT INTO [dbo].[Products] WITH(ROWLOCK) ([CategoryID], [Name], [Description], [Price], [Weight])
OUTPUT Inserted.[ProductID] INTO @NewProducts ([ProductID])
VALUES
(
	@CategoryID,
	@Name,
	@Description,
	@Price,
	@Weight
);

SELECT TOP 1 [ProductID] FROM @NewProducts;";

        private const string CreateImageCommandText = @"
DECLARE @BinaryID UNIQUEIDENTIFIER = NULL;
DECLARE @NewBinaries TABLE ([BinaryID] UNIQUEIDENTIFIER);

INSERT INTO [dbo].[ImageBinaries] WITH(ROWLOCK) ([Type], [Name], [Data])
OUTPUT Inserted.[BinaryID] INTO @NewBinaries
VALUES
(
	@Type,
	@Name,
	@Data
);

SELECT TOP 1 @BinaryID = [BinaryID]
FROM @NewBinaries;

INSERT INTO [dbo].[ProductImages] WITH(ROWLOCK) ([ProductID], [BinaryID], [Type])
VALUES
(
	@ProductID,
	@BinaryID,
	@ImageType
);";
        #endregion

        #region Test methods

        [TestMethod]
        public void IsProductsDataUploadsSuccessfully()
        {
            string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string dataDirectory = Path.Combine(desktopDirectory, "Wasabi");
            if (Directory.Exists(dataDirectory))
            {
                SqlConnection connection = CreateConnection();

                var productDataFiles = Directory.EnumerateFiles(dataDirectory, "*.json", SearchOption.AllDirectories);
                foreach (string productDataFile in productDataFiles)
                {
                    UploadProductData(productDataFile, connection);
                }
            }
        }
        #endregion

        #region Private class methods

        private static SqlConnection CreateConnection()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            connection.Open();
            return connection;
        }
        
        private static JObject LoadData(string dataFilePath)
        {
            using (StreamReader reader = new StreamReader(dataFilePath, Encoding.UTF8, false, 8192))
            {
                return JObject.Load(new JsonTextReader(reader), new JsonLoadSettings
                {
                    LineInfoHandling = LineInfoHandling.Ignore
                });
            }
        }

        private static void UploadProductData(string productDataFile, SqlConnection dbConnection)
        {
            JObject dataSource = LoadData(productDataFile);
            Guid productID = CreateProduct(dataSource, dbConnection);
            if (productID != Guid.Empty)
            {
				JArray imagesArray = dataSource["Images"] as JArray;

                CreateProductImages(productID, imagesArray ?? new JArray(), dbConnection);
            }
        }

        private static Guid CreateProduct(JObject dataSource, SqlConnection dbConnection)
        {
            Guid productID = Guid.Empty;

            using (SqlCommand command = new SqlCommand(CreateProductCommandText, dbConnection))
            {
                command.CommandTimeout = 30;
                command.Parameters.AddWithValue("CategoryName", (string)dataSource["Category"]);
                command.Parameters.AddWithValue("Name", (string)dataSource["Name"]);
                command.Parameters.AddWithValue("Description", (string)dataSource["Content"]);

                string priceText = (string)dataSource["Price"];
				double rawPrice = 0.0;
				if (!double.TryParse(priceText, out rawPrice))
				{
					priceText = priceText.Substring(0, priceText.IndexOf('р'));
					double.TryParse(priceText, out rawPrice);
				}

                decimal priceValue = (decimal)rawPrice;
                command.Parameters.AddWithValue("Price", priceValue);

                string weight = (string)dataSource["Weight"];
                StringBuilder weightBuilder = new StringBuilder(6);
                for (int index = 0; index < weight.Length; ++index)
                {
                    char current = weight[index];
                    if (char.IsDigit(current) || current == '.')
                    {
                        weightBuilder.Append(current);
                    }
                    else break;
                }
                double weightValue = (weightBuilder.Length > 0) ? double.Parse(weightBuilder.ToString()) : 0.0;

                command.Parameters.AddWithValue("Weight", weightValue);
                
                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            productID = reader.GetGuid(0);
                        }
                    }
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc);
					if (Debugger.IsAttached)
					{
						Debugger.Break();
					}
                }
            }

            return productID;
        }
        
        private static void CreateProductImages(Guid productID, JArray imagesArray, SqlConnection dbConnection)
        {
            for (int index = 0; index < imagesArray.Count; ++index)
            {
                JObject currentImage = imagesArray[index] as JObject;

                string filePath = (string)currentImage["Path"];
				string fileName = Path.GetFileName(filePath);
                byte[] fileData = File.ReadAllBytes(filePath);
                int imageType = (int)currentImage["Type"];

                using (SqlCommand command = new SqlCommand(CreateImageCommandText, dbConnection))
                {
                    command.Parameters.AddWithValue("Data", fileData);
					command.Parameters.AddWithValue("Name", fileName);
                    command.Parameters.AddWithValue("Type", "image/jpg");
                    command.Parameters.AddWithValue("ProductID", productID);
                    command.Parameters.AddWithValue("ImageType", imageType);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc);
                        if (Debugger.IsAttached)
                        {
                            Debugger.Break();
                        }
                    }
                }
            }
        }
        #endregion
    }
}